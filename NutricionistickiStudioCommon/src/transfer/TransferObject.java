/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transfer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author maja
 */
public class TransferObject implements Serializable {

    public static final String MESSAGE = "message";
    
    private Map<String, Object> data;
    private Operation operation;

    public TransferObject() {
        data = new HashMap<>();
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Map<String, Object> getData() {
        return data;
    }
    
    public Object get(String key) {
        return data.get(key);
    }

    public void put(String key, Object value) {
        data.put(key, value);
    }
}
