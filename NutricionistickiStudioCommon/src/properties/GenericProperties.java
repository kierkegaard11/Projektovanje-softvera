/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 *
 * @author maja
 */
public abstract class GenericProperties {

    private static final String DEFAULT_VALUE = "???";
    protected Properties props;

    public GenericProperties() {
        props = new Properties();
        loadProperties();
    }
    
    public void setProperty(String key, String value) {
        props.setProperty(key, value);
    }
    
    public String getProperty(String key) {
        String value = props.getProperty(key);
        if (value == null || value.equals("")) {
            return DEFAULT_VALUE;
        }
        return value;
    }

    private void loadProperties() {
        InputStream in = null;
        try {
            in = new FileInputStream(getFilePath());
            props.load(in);
        } catch (IOException ex) {
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                }
            }
        }
    }
    
    public void storeProperties() {
        OutputStream output = null;
        try {
            output = new FileOutputStream(getFilePath());
            props.store(output, null);
        } catch (IOException io) {
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                }
            }
        }
    }
    
    protected abstract String getFilePath();
    
}
