/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Objects;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class MedicalRecord implements Persistence {

    public static final String MEDICAL_RECORD_KEY = "medicalRecord";
    public static final String COL_MEDICAL_RECORD_ID = "medical_record_id";
    public static final String COL_FIRST_NAME = "first_name";
    public static final String COL_LAST_NAME = "last_name";
    public static final String COL_DATE_OF_BIRTH = "date_of_birth";
    public static final String COL_GENDER = "gender";
    public static final String COL_MOBILE_PHONE = "mobile_phone";
    public static final String COL_EMAIL = "email";
    public static final String COL_NOTE = "note";

    private static final Integer MEDICAL_RECORD_ID_DEFAULT = 0;
    private static final String FIRST_NAME_DEFAULT = "UNKNOWN";
    private static final String LAST_NAME_DEFAULT = "UNKNOWN";
    private static final String GENDER_DEFAULT = Gender.FEMALE;
    private static final Date DATE_OF_BIRTH_DEFAULT = new Date();
    private static final String MOBILE_PHONE_DEFAULT = "000/000-0000";
    private static final String EMAIL_DEFAULT = "something@something.com";
    private static final String NOTE_DEFAULT = "UNKNOWN";

    private static final String GET_ATTR_VALUES_PATTERN
            = "%s, '%s', '%s', '%s', '%s', '%s', '%s', '%s'";

    private static final String SET_ATTR_VALUES_PATTERN
            = COL_FIRST_NAME + " = '%s', "
            + COL_LAST_NAME + " = '%s', "
            + COL_DATE_OF_BIRTH + " = '%s', "
            + COL_GENDER + " = '%s', "
            + COL_MOBILE_PHONE + "= '%s', "
            + COL_EMAIL + "= '%s', "
            + COL_NOTE + " ='%s'";

    private Integer medicalRecordId;
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String gender;
    private String mobilePhone;
    private String email;
    private String note;

    public MedicalRecord() {
        medicalRecordId = MEDICAL_RECORD_ID_DEFAULT;
        firstName = FIRST_NAME_DEFAULT;
        lastName = LAST_NAME_DEFAULT;
        gender = GENDER_DEFAULT;
        dateOfBirth = DATE_OF_BIRTH_DEFAULT;
        mobilePhone = MOBILE_PHONE_DEFAULT;
        email = EMAIL_DEFAULT;
        note = NOTE_DEFAULT;
    }

    public MedicalRecord(Integer medicalRecordId) {
        this.medicalRecordId = medicalRecordId;
    }

    public MedicalRecord(Integer medicalRecordId, String firstName, String lastName, Date dateOfBirth, String gender, String mobilePhone, String email, String note) {
        this.medicalRecordId = medicalRecordId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.mobilePhone = mobilePhone;
        this.email = email;
        this.note = note;
    }

    public Integer getMedicalRecordId() {
        return medicalRecordId;
    }

    public void setMedicalRecordId(Integer medicalRecordId) {
        this.medicalRecordId = medicalRecordId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.medicalRecordId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MedicalRecord other = (MedicalRecord) obj;
        if (!Objects.equals(this.medicalRecordId, other.medicalRecordId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return medicalRecordId + " " + firstName + " " + lastName;
    }

    @Override
    public String getAtrValue() {
        return String.format(GET_ATTR_VALUES_PATTERN,
                medicalRecordId,
                firstName,
                lastName,
                new java.sql.Date(dateOfBirth.getTime()),
                gender,
                mobilePhone,
                email,
                note);
    }

    @Override
    public String setAtrValue() {
        return String.format(SET_ATTR_VALUES_PATTERN,
                firstName,
                lastName,
                new java.sql.Date(dateOfBirth.getTime()),
                gender,
                mobilePhone,
                email,
                note);
    }

    @Override
    public String getClassName() {
        return "medical_record";
    }

    @Override
    public String getNameByColumn(int column) {
        String[] names = {COL_MEDICAL_RECORD_ID,
            COL_FIRST_NAME,
            COL_LAST_NAME,
            COL_DATE_OF_BIRTH,
            COL_GENDER,
            COL_MOBILE_PHONE,
            COL_EMAIL,
            COL_NOTE};
        return names[column];
    }

    @Override
    public Persistence getNewRecord(ResultSet rs) throws SQLException {
        return new MedicalRecord(rs.getInt(COL_MEDICAL_RECORD_ID),
                rs.getString(COL_FIRST_NAME),
                rs.getString(COL_LAST_NAME),
                rs.getDate(COL_DATE_OF_BIRTH),
                rs.getString(COL_GENDER),
                rs.getString(COL_MOBILE_PHONE), rs.getString(COL_EMAIL),
                rs.getString(COL_NOTE));
    }

    @Override
    public String getWhereCondition() {
        return "";
    }

}
