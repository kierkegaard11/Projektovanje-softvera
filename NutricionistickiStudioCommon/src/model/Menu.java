/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class Menu implements Persistence {

    public static final String COL_MENU_ID = "menu_id";
    public static final String COL_DATE_FROM = "date_from";
    public static final String COL_DATE_TO = "date_to";
    public static final String COL_MEDICAL_RECORD = "medical_record_id";
    public static final String COL_MENU_TYPE = "menu_type_id";
    public static final String MENU_KEY = "menu";

    private static final String GET_ATTR_VALUES_PATTERN
            = "%s, '%s', '%s', %s, %s";

    private static final String SET_ATTR_VALUES_PATTERN
            = COL_DATE_FROM + " = '%s', "
            + COL_DATE_TO + " = '%s', "
            + COL_MEDICAL_RECORD + " = %s, "
            + COL_MENU_TYPE + " = %s ";

    private Integer menuId;
    private Date dateFrom;
    private Date dateTo;
    private List<Meal> meals;
    private MedicalRecord medicalRecord;
    private MenuType menuType;

    public Menu() {
        menuId = 0;
        dateFrom = new Date();
        dateTo = new Date();
        meals = new ArrayList<>();
        medicalRecord = new MedicalRecord(1);
        menuType = new MenuType(1);
    }

    public Menu(Integer menuId) {
        this.menuId = menuId;
        dateFrom = new Date();
        dateTo = new Date();
        meals = new ArrayList<>();
        medicalRecord = new MedicalRecord(1);
        menuType = new MenuType(1);
    }

    public Menu(Integer menuId, Date dateFrom, Date dateTo, List<Meal> meals, MedicalRecord medicalRecord, MenuType menuType) {
        this.menuId = menuId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.meals = meals;
        this.medicalRecord = medicalRecord;
        this.menuType = menuType;
    }

    public Menu(Integer menuId, Date dateFrom, Date dateTo, MedicalRecord medicalRecord, MenuType menuType) {
        this.menuId = menuId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.meals = new ArrayList<>();
        this.medicalRecord = medicalRecord;
        this.menuType = menuType;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public MedicalRecord getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(MedicalRecord medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public MenuType getMenuType() {
        return menuType;
    }

    public void setMenuType(MenuType menuType) {
        this.menuType = menuType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.menuId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Menu other = (Menu) obj;
        if (!Objects.equals(this.menuId, other.menuId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Menu{" + "menuId=" + menuId + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + ", meals=" + meals + ", medicalRecord=" + medicalRecord + ", menuType=" + menuType + '}';
    }

    @Override
    public String getAtrValue() {
        return String.format(GET_ATTR_VALUES_PATTERN,
                menuId,
                new java.sql.Date(dateFrom.getTime()),
                new java.sql.Date(dateTo.getTime()),
                medicalRecord.getMedicalRecordId(),
                menuType.getMenuTypeId());
    }

    @Override
    public String setAtrValue() {
        return String.format(SET_ATTR_VALUES_PATTERN,
                new java.sql.Date(dateFrom.getTime()),
                new java.sql.Date(dateTo.getTime()),
                medicalRecord.getMedicalRecordId(),
                menuType.getMenuTypeId());
    }

    @Override
    public String getClassName() {
        return "menu";
    }

    @Override
    public String getNameByColumn(int column) {
        String[] names = {COL_MENU_ID,
            COL_DATE_FROM,
            COL_DATE_TO,
            COL_MEDICAL_RECORD,
            COL_MENU_TYPE};
        return names[column];
    }

    @Override
    public Persistence getNewRecord(ResultSet rs) throws SQLException {
        return new Menu(rs.getInt(COL_MENU_ID),
                rs.getDate(COL_DATE_FROM),
                rs.getDate(COL_DATE_TO),
                new MedicalRecord(rs.getInt(COL_MEDICAL_RECORD)),
                new MenuType(rs.getInt(COL_MENU_TYPE)));
    }

    @Override
    public String getWhereCondition() {
        return COL_MENU_ID + "=" + menuId;
    }

}
