/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class Dish implements Persistence {

    public static final String COL_DISH_ID = "dish_id";
    public static final String COL_NAME = "name";
    public static final String COL_PREP_GUIDE = "preparation_guide";
    public static final String COL_KCAL = "kcal";

    public static final String DISH_KEY = "dish";

    private static final String GET_ATTR_VALUES_PATTERN
            = "%s,'%s', '%s', %s";

    private static final String SET_ATTR_VALUES_PATTERN
            = COL_NAME + " = '%s', "
            + COL_PREP_GUIDE + " = '%s', "
            + COL_KCAL + " = %s ";

    private Integer dishId;
    private String name;
    private String preparationGuide;
    private Double kcal;
    private List<IngredientDishAggregate> ingredients;

    public Dish() {
        dishId = 0;
        name = "Unknown";
        preparationGuide = "Unknown";
        kcal = new Double(0);
        ingredients = new ArrayList<>();
    }

    public Dish(Integer dishId) {
        this.dishId = dishId;
        ingredients = new ArrayList<>();
    }

    public Dish(Integer dishId, String name, String preparationGuide, Double kcal, List<IngredientDishAggregate> ingredients) {
        this.dishId = dishId;
        this.name = name;
        this.preparationGuide = preparationGuide;
        this.kcal = kcal;
        this.ingredients = ingredients;
    }

    public Dish(Integer dishId, String name, String preparationGuide, Double kcal) {
        this.dishId = dishId;
        this.name = name;
        this.preparationGuide = preparationGuide;
        this.kcal = kcal;
        ingredients = new ArrayList<>();

    }

    public Integer getDishId() {
        return dishId;
    }

    public void setDishId(Integer dishId) {
        this.dishId = dishId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreparationGuide() {
        return preparationGuide;
    }

    public void setPreparationGuide(String preparationGuide) {
        this.preparationGuide = preparationGuide;
    }

    public Double getKcal() {
        return kcal;
    }

    public void setKcal(Double kcal) {
        this.kcal = kcal;
    }

    public List<IngredientDishAggregate> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientDishAggregate> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.dishId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dish other = (Dish) obj;
        if (!Objects.equals(this.dishId, other.dishId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return dishId + " " + name;
    }

    @Override
    public String getAtrValue() {
        return String.format(GET_ATTR_VALUES_PATTERN,
                dishId,
                name,
                preparationGuide,
                kcal);
    }

    @Override
    public String setAtrValue() {
        return String.format(SET_ATTR_VALUES_PATTERN,
                name,
                preparationGuide,
                kcal);
    }

    @Override
    public String getClassName() {
        return "dish";
    }

    @Override
    public String getNameByColumn(int column) {
        String[] names = {COL_DISH_ID,
            COL_NAME,
            COL_KCAL};
        return names[column];
    }

    @Override
    public Persistence getNewRecord(ResultSet rs) throws SQLException {
        return new Dish(rs.getInt(COL_DISH_ID),
                rs.getString(COL_NAME),
                rs.getString(COL_PREP_GUIDE),
                rs.getDouble(COL_KCAL));
    }

    @Override
    public String getWhereCondition() {
        return "";
    }

}
