/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class Meal implements Persistence {

    public static final String MEAL_KEY = "meal";

    public static final String COL_MEAL_ID = "meal_id";
    public static final String COL_MENU_ID = "menu_id";
    public static final String COL_MEAL_TYPE = "meal_type";
    public static final String COL_MEAL_DAY = "meal_day";
    public static final String COL_GAMOUNT = "g_amount";
    public static final String COL_DISH = "dish_id";

    private static final String GET_ATTR_VALUES_PATTERN
            = "%s, %s, '%s', '%s', %s, %s";

    private static final String SET_ATTR_VALUES_PATTERN
            = COL_MEAL_ID + " = %s, "
            + COL_MENU_ID + " = %s, "
            + COL_MEAL_TYPE + " = '%s', "
            + COL_MEAL_DAY + " = '%s', "
            + COL_GAMOUNT + " = %s, "
            + COL_DISH + " = %s ";
    
    private Integer mealId;
    private Integer menuId;
    private String mealType;
    private String mealDay;
    private Integer gAmount;
    private Dish dish;

    public Meal() {
        this.menuId = 1;
        this.dish = new Dish(1);
    }

    public Meal(Integer mealId, Integer menuId) {
        this.mealId = mealId;
        this.menuId = menuId;
        this.dish = new Dish(1);
    }

    public Meal(Integer mealId, Integer menuId, String mealType, String mealDay, Integer gAmount, Dish dish) {
        this.mealId = mealId;
        this.menuId = menuId;
        this.mealType = mealType;
        this.mealDay = mealDay;
        this.gAmount = gAmount;
        this.dish = dish;
    }

    public Integer getMealId() {
        return mealId;
    }

    public void setMealId(Integer mealId) {
        this.mealId = mealId;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    public String getMealDay() {
        return mealDay;
    }

    public void setMealDay(String mealDay) {
        this.mealDay = mealDay;
    }

    public Integer getgAmount() {
        return gAmount;
    }

    public void setgAmount(Integer gAmount) {
        this.gAmount = gAmount;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.mealId);
        hash = 79 * hash + Objects.hashCode(this.menuId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Meal other = (Meal) obj;
        if (!Objects.equals(this.mealId, other.mealId)) {
            return false;
        }
        if (!Objects.equals(this.menuId, other.menuId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Meal{" + "mealId=" + mealId + ", menuId=" + menuId + ", mealType=" + mealType + ", mealDay=" + mealDay + ", gAmount=" + gAmount + ", dish=" + dish.getDishId() + '}';
    }

    @Override
    public String getAtrValue() {
        return String.format(GET_ATTR_VALUES_PATTERN,
                mealId,
                menuId,
                mealType,
                mealDay,
                gAmount,
                dish.getDishId());
    }

    @Override
    public String setAtrValue() {
        return String.format(SET_ATTR_VALUES_PATTERN,
                mealId,
                menuId,
                mealType,
                mealDay,
                gAmount,
                dish.getDishId());
    }

    @Override
    public String getClassName() {
        return "meal";
    }

    @Override
    public String getNameByColumn(int column) {
        String[] names = {COL_MEAL_ID,
            COL_MENU_ID,
            COL_MEAL_TYPE,
            COL_MEAL_DAY,
            COL_GAMOUNT,
            COL_DISH};
        return names[column];
    }

    @Override
    public Persistence getNewRecord(ResultSet rs) throws SQLException {
        return new Meal(rs.getInt(COL_MEAL_ID),
                rs.getInt(COL_MENU_ID),
                rs.getString(COL_MEAL_TYPE),
                rs.getString(COL_MEAL_DAY),
                rs.getInt(COL_GAMOUNT),
                new Dish(rs.getInt(COL_DISH)));
    }

    @Override
    public String getWhereCondition() {
        return COL_MENU_ID + " = " + menuId + " AND " + COL_MEAL_ID + " = " + mealId;
    }

}
