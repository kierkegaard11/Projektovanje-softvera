/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author maja
 */
public class MealDay {
    
    public static final String MONDAY = "model.mealDay.monday";
    public static final String TUESDAY = "model.mealDay.tuesday";
    public static final String WEDNESDAY = "model.mealDay.wednesday";
    public static final String THURSDAY = "model.mealDay.thursday";
    public static final String FRIDAY = "model.mealDay.friday";
    public static final String SATURDAY = "model.mealDay.saturday";
    public static final String SUNDAY = "model.mealDay.sunday";
    
}
