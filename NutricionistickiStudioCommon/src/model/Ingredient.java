/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class Ingredient implements Persistence {

    public static final String COL_INGREDIENT_ID = "ingredient_id";
    public static final String COL_NAME = "name";
    public static final String COL_GCARBS = "g_carbs";
    public static final String COL_GPROTEINS = "g_proteins";
    public static final String COL_GFAT = "g_fat";
    public static final String COL_GLICEMY_INDEX = "glicemy_index";
    public static final String COL_KCAL = "kcal";
    public static final String COL_DESCRIPTION = "description";

    public static final String INGREDIENT_KEY = "ingredient";
    private static final String GET_ATTR_VALUE_PATTERN
            = "'%s', %s, %s, %s, %s, %s, '%s'";

    private static final String SET_ATTR_VALUE_PATTERN
            = COL_NAME + " = '%s', "
            + COL_GCARBS + " = %s, "
            + COL_GPROTEINS + " = %s, "
            + COL_GLICEMY_INDEX + " = %s, "
            + COL_KCAL + " = %s, "
            + COL_DESCRIPTION + " = '%s'";

    private Integer ingredientId;
    private String name;
    private Double gCarbs;
    private Double gProteins;
    private Double gFat;
    private Double glicemyIndex;
    private Double kcal;
    private String description;

    public Ingredient() {
    }

    public Ingredient(Integer ingredientId) {
        this.ingredientId = ingredientId;
    }

    public Ingredient(Integer ingredientId, String name, Double gCarbs, Double gProteins, Double gFat, Double glicemyIndex, Double kcal, String description) {
        this.ingredientId = ingredientId;
        this.name = name;
        this.gCarbs = gCarbs;
        this.gProteins = gProteins;
        this.gFat = gFat;
        this.glicemyIndex = glicemyIndex;
        this.kcal = kcal;
        this.description = description;
    }

    public Integer getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Integer ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getgCarbs() {
        return gCarbs;
    }

    public void setgCarbs(Double gCarbs) {
        this.gCarbs = gCarbs;
    }

    public Double getgProteins() {
        return gProteins;
    }

    public void setgProteins(Double gProteins) {
        this.gProteins = gProteins;
    }

    public Double getgFat() {
        return gFat;
    }

    public void setgFat(Double gFat) {
        this.gFat = gFat;
    }

    public Double getGlicemyIndex() {
        return glicemyIndex;
    }

    public void setGlicemyIndex(Double glicemyIndex) {
        this.glicemyIndex = glicemyIndex;
    }

    public Double getKcal() {
        return kcal;
    }

    public void setKcal(Double kcal) {
        this.kcal = kcal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.ingredientId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ingredient other = (Ingredient) obj;
        if (!Objects.equals(this.ingredientId, other.ingredientId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getAtrValue() {
        return String.format(GET_ATTR_VALUE_PATTERN,
                name,
                gCarbs,
                gProteins,
                gFat,
                glicemyIndex,
                kcal,
                description);
    }

    @Override
    public String setAtrValue() {
        return String.format(SET_ATTR_VALUE_PATTERN,
                name,
                gCarbs,
                gProteins,
                gFat,
                glicemyIndex,
                kcal,
                description);
    }

    @Override
    public String getClassName() {
        return "ingredient";
    }

    @Override
    public String getNameByColumn(int column) {
        String[] names = {COL_INGREDIENT_ID,
            COL_NAME, COL_GCARBS,
            COL_GPROTEINS,
            COL_GFAT,
            COL_GLICEMY_INDEX,
            COL_GLICEMY_INDEX,
            COL_KCAL,
            COL_DESCRIPTION};
        return names[column];
    }

    @Override
    public Persistence getNewRecord(ResultSet rs) throws SQLException {
        return new Ingredient(rs.getInt(COL_INGREDIENT_ID),
                rs.getString(COL_NAME),
                rs.getDouble(COL_GCARBS),
                rs.getDouble(COL_GPROTEINS),
                rs.getDouble(COL_GFAT),
                rs.getDouble(COL_GLICEMY_INDEX),
                rs.getDouble(COL_KCAL),
                rs.getString(COL_DESCRIPTION));
    }

    @Override
    public String getWhereCondition() {
        return "";
    }
}
