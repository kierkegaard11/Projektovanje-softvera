/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.persistence;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author maja
 */
public interface Persistence extends Serializable {

    String getAtrValue();

    String setAtrValue();

    String getClassName();

    String getNameByColumn(int column);

    Persistence getNewRecord(ResultSet rs) throws SQLException;

    public String getWhereCondition();
    
}
