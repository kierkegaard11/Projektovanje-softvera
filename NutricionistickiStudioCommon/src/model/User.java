/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class User implements Persistence {

    public static final String USER_KEY = "user";

    public static final String COL_USER_ID = "user_id";
    public static final String COL_FIRSTNAME = "first_name";
    public static final String COL_LASTNAME = "last_name";
    public static final String COL_USERNAME = "username";
    public static final String COL_PASSWORD = "password";

    private static final String GET_ATTR_VALUE_PATTERN
            = "%s, '%s', '%s', '%s', '%s'";

    private static final String SET_ATTR_VALUE_PATTERN
            = COL_USER_ID + " = %s, "
            + COL_FIRSTNAME + " = '%s', "
            + COL_LASTNAME + " = '%s', "
            + COL_USERNAME + " = '%s', "
            + COL_PASSWORD + " = '%s'";

    private Integer userId;
    private String firstName;
    private String lastName;
    private String username;
    private String password;

    public User() {
    }

    public User(Integer userId) {
        this.userId = userId;
    }

    public User(Integer userId, String firstName, String lastName, String username, String password) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
    }

    public User(String firstName, String lastName, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.userId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", username=" + username + ", password=" + password + '}';
    }

    @Override
    public String getAtrValue() {
        return String.format(GET_ATTR_VALUE_PATTERN,
                userId,
                firstName,
                lastName,
                username,
                password);
    }

    @Override
    public String setAtrValue() {
        return String.format(SET_ATTR_VALUE_PATTERN,
                userId,
                firstName,
                lastName,
                username,
                password);
    }

    @Override
    public String getClassName() {
        return "user";
    }

    @Override
    public String getNameByColumn(int column) {
        String[] names = {COL_USER_ID,
            COL_FIRSTNAME,
            COL_LASTNAME,
            COL_USERNAME,
            COL_PASSWORD};
        return names[column];
    }

    @Override
    public Persistence getNewRecord(ResultSet rs) throws SQLException {
        return new User(rs.getInt(COL_USER_ID),
                rs.getString(COL_FIRSTNAME),
                rs.getString(COL_LASTNAME),
                rs.getString(COL_USERNAME),
                rs.getString(COL_PASSWORD));
    }

    @Override
    public String getWhereCondition() {
        return "";
    }

}
