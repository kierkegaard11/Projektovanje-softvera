/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class IngredientDishAggregate implements Persistence {

    public static final String COL_INGREDIENT_ID = "ingredient_id";
    public static final String COL_DISH_ID = "dish_id";
    public static final String COL_AMOUNT = "amount";
    public static final String COL_MEASUREMENT_UNIT = "measurement_unit";

    private static final String GET_ATTR_VALUES_PATTERN
            = " %s, %s, %s, '%s'";

    private static final String SET_ATTR_VALUES_PATTERN
            = COL_AMOUNT + " = %s, "
            + COL_MEASUREMENT_UNIT + " = '%s' ";

    private Ingredient ingredient;
    private Dish dish;
    private Double amount;
    private String measurementUnit;

    public IngredientDishAggregate() {

    }

    public IngredientDishAggregate(Integer ingredientId, Integer dishId) {
        this.ingredient = new Ingredient(ingredientId);
        this.dish = new Dish(dishId);
    }

    public IngredientDishAggregate(Integer ingredientId, Integer dishId, Double amount, String measurementUnit) {
        this.ingredient = new Ingredient(ingredientId);
        this.dish = new Dish(dishId);
        this.amount = amount;
        this.measurementUnit = measurementUnit;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.ingredient.getIngredientId());
        hash = 97 * hash + Objects.hashCode(this.dish.getDishId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IngredientDishAggregate other = (IngredientDishAggregate) obj;
        if (!Objects.equals(this.ingredient.getIngredientId(), other.ingredient.getIngredientId())) {
            return false;
        }
        if (!Objects.equals(this.getDish().getDishId(), other.getDish().getDishId())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ingredient.toString();
    }

    @Override
    public String getAtrValue() {
        return String.format(GET_ATTR_VALUES_PATTERN,
                ingredient.getIngredientId(),
                dish.getDishId(),
                amount,
                measurementUnit);
    }

    @Override
    public String setAtrValue() {
        return String.format(SET_ATTR_VALUES_PATTERN,
                amount,
                measurementUnit);
    }

    @Override
    public String getClassName() {
        return "ingredient_dish_aggregate";
    }

    @Override
    public String getNameByColumn(int column) {
        String names[] = {COL_INGREDIENT_ID,
            COL_DISH_ID,
            COL_AMOUNT,
            COL_MEASUREMENT_UNIT};
        return names[column];
    }

    @Override
    public Persistence getNewRecord(ResultSet rs) throws SQLException {
        return new IngredientDishAggregate(rs.getInt(COL_INGREDIENT_ID),
                rs.getInt(COL_DISH_ID),
                rs.getDouble(COL_AMOUNT),
                rs.getString(COL_MEASUREMENT_UNIT));
    }

    @Override
    public String getWhereCondition() {
        return COL_DISH_ID + "=" + dish.getDishId() + " AND " + COL_INGREDIENT_ID + "=" + ingredient.getIngredientId();
    }

}
