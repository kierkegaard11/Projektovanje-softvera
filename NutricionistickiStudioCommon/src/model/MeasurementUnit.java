/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author maja
 */
public interface MeasurementUnit {
    
    String GRAM = "model.measurementUnit.gram";
    String MILLILITER = "model.measurementUnit.milliliter";
    String PIECE = "model.measurementUnit.piece";
}
