/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class MenuType implements Persistence {

    public static final String COL_MENU_TYPE_ID = "menu_type_id";
    public static final String COL_NAME = "name";
    public static final String COL_DESCRIPTION = "description";

    public static final String MENU_TYPE_KEY = "menu type";

    private static final String GET_ATTR_VALUE_PATTERN
            = "'%s', '%s'";

    private static final String SET_ATTR_VALUE_PATTERN
            = COL_NAME + " = '%s', "
            + COL_DESCRIPTION + " = '%s' ";

    private Integer menuTypeId;
    private String name;
    private String description;
    private List<Dish> dishes;

    public MenuType() {
        menuTypeId = 0;
        name = "Za mrsavljenje";
        description = "Za mrsavljenje";
        dishes = new ArrayList<>();
    }

    public MenuType(Integer menuTypeId) {
        this.menuTypeId = menuTypeId;
    }

    public MenuType(Integer menuTypeId, String name, String description, List<Dish> dishes) {
        this.menuTypeId = menuTypeId;
        this.name = name;
        this.description = description;
        this.dishes = dishes;
    }

    public MenuType(Integer menuTypeId, String name, String description) {
        this.menuTypeId = menuTypeId;
        this.name = name;
        this.description = description;
    }

    public Integer getMenuTypeId() {
        return menuTypeId;
    }

    public void setMenuTypeId(Integer menuTypeId) {
        this.menuTypeId = menuTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.menuTypeId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MenuType other = (MenuType) obj;
        if (!Objects.equals(this.menuTypeId, other.menuTypeId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getAtrValue() {
        return String.format(GET_ATTR_VALUE_PATTERN,
                name,
                description);
    }

    @Override
    public String setAtrValue() {
        return String.format(SET_ATTR_VALUE_PATTERN,
                name,
                description);
    }

    @Override
    public String getClassName() {
        return "menu_type";
    }

    @Override
    public String getNameByColumn(int column) {
        String[] names = {COL_MENU_TYPE_ID,
            COL_NAME,
            COL_DESCRIPTION};
        return names[column];
    }

    @Override
    public Persistence getNewRecord(ResultSet rs) throws SQLException {
        return new MenuType(rs.getInt(COL_MENU_TYPE_ID),
                rs.getString(COL_NAME),
                rs.getString(COL_DESCRIPTION));
    }

    @Override
    public String getWhereCondition() {
        return "";
    }

}
