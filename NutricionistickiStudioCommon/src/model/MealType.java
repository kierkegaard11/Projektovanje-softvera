/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author maja
 */
public class MealType {
    
    public static final String BREAKFAST = "model.mealType.breakfast";
    public static final String SNACK = "model.mealType.snack";
    public static final String LUNCH = "model.mealType.lunch";
    public static final String DINNER = "model.mealType.dinner";
    public static final String NIGHT_SNACK = "model.mealType.nightSnack";

}
