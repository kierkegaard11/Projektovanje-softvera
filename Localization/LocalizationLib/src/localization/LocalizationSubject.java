/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package localization;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public final class LocalizationSubject {
    
    private static LocalizationSubject instance;
    
    private List<LocalizationObserver> observers;
    
    private LocalizationSubject() {
        observers = new ArrayList<>();
    }
    
    public static LocalizationSubject getInstance() {
        if (instance == null) {
            instance = new LocalizationSubject();
        }
        return instance;
    }
    
    public void subscribe(LocalizationObserver observer) {
        observers.add(observer);
    }
    
    public void localizationChanged() {
        for (LocalizationObserver observer : observers) {
            observer.localizationChanged();
        }
    }
    
}
