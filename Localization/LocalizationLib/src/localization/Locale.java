/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package localization;

/**
 *
 * @author maja
 */
public enum Locale {
    
    SR("sr"),
    EN("en");
    
    private String locale;
    
    private Locale(String locale) {
        this.locale = locale;
    }
    
    public String getLocale() {
        return locale;
    }
}
