/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package localization;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author maja
 */
public final class LocalizationLoader {

    private static String filePath = ".";
    private static final String LOCALES_FILE_NAME_PATTERN = "%s/locales_%s.properties";
    private static final String DEFAULT_STRING = "Unknown";
    private static LocalizationLoader instance;

    private Properties properties;

    private LocalizationLoader() {
        properties = new Properties();
    }

    public static LocalizationLoader getInstance() {
        if (instance == null) {
            instance = new LocalizationLoader();
        }
        return instance;
    }

    public void loadLocalization(Locale locale) {
        InputStream in = null;
        try {
            String fileName = String.format(LOCALES_FILE_NAME_PATTERN, filePath, locale.getLocale());
            in = new FileInputStream(fileName);
            properties.load(in);
        } catch (IOException ex) {
            Logger.getLogger(LocalizationLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(LocalizationLoader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        LocalizationSubject.getInstance().localizationChanged();
    }
    
    public String getString(String key) {
        String string = properties.getProperty(key);
        if (string != null) {
            return string;
        }
        return DEFAULT_STRING;
    }

    public static void setFilePath(String fileName) {
        LocalizationLoader.filePath = fileName;
    }

}
