/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package start;

import form.FormStart;
import localization.LocalizationLoader;

/**
 *
 * @author lmiletic
 */
public class Start {
    
    public static void main(String[] args) {
        
        LocalizationLoader.setFilePath("./src/localization/locales");
        
        FormStart formStart = new FormStart();
        formStart.setLocationRelativeTo(null);
        formStart.setVisible(true);
        
    }
    
}
