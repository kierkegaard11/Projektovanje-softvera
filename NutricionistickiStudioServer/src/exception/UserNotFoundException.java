/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author maja
 */
public class UserNotFoundException extends Exception {

    public static final String DEFAULT_MESSAGE = "operation.login.error.userNotFound";

    public UserNotFoundException(String message) {
        super(message);
    }
}
