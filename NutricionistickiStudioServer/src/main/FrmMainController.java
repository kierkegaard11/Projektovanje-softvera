/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author maja
 */
public class FrmMainController {

    private FrmMain view;
    public static final String SERVER_STATUS_ACTIVE = "Server is currently active.";
    public static final String SERVER_STATUS_INACTIVE = "Server is currently inactive.";

    public FrmMainController() {
        view = new FrmMain();
        addListeners();
        prepareView();
    }

    private void addListeners() {
        view.getMenuItemDatabaseSettings().addActionListener(new MenuItemDatabaseSettingsActionListener(this));
        view.getMenuItemServerSettings().addActionListener(new MenuItemServerSettingsActionListener(this));
        view.getBtnStartServer().addActionListener(new BtnStartServerActionListener(this));
        view.getBtnStopServer().addActionListener(new BtnStopServerActionListener(this));
    }

    private void prepareView() {
        view.getBtnStopServer().setEnabled(false);
        view.getTxtServerStatus().setText(SERVER_STATUS_INACTIVE);
    }

    public void showForm() {
        view.setLocationRelativeTo(null);
        view.setVisible(true);
    }

    public FrmMain getView() {
        return view;
    }

}
