/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import db.DatabaseProperties;
import db.FrmDatabaseSettings;
import db.FrmDatabaseSettingsController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author maja
 */
public class MenuItemDatabaseSettingsActionListener implements ActionListener {

    private FrmMainController controller;

    public MenuItemDatabaseSettingsActionListener(FrmMainController controller) {
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        FrmDatabaseSettings view = new FrmDatabaseSettings(controller.getView(), true);
        DatabaseProperties model = new DatabaseProperties();
        FrmDatabaseSettingsController frmDatabaseSettingsController = new FrmDatabaseSettingsController(view, model);
        frmDatabaseSettingsController.showForm();
    }

}
