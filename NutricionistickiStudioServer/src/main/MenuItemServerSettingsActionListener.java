/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import server.FrmServerSettings;
import server.FrmServerSettingsController;
import server.ServerProperties;

/**
 *
 * @author maja
 */
public class MenuItemServerSettingsActionListener implements ActionListener {

    private FrmMainController controller;

    public MenuItemServerSettingsActionListener(FrmMainController controller) {
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        FrmServerSettings view = new FrmServerSettings(controller.getView(), true);
        ServerProperties model = new ServerProperties();
        FrmServerSettingsController frmServerSettingsController = new FrmServerSettingsController(view, model);
        frmServerSettingsController.showForm();
    }

}
