/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import global.GlobalData;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import server.ServerThread;

/**
 *
 * @author maja
 */
public class BtnStartServerActionListener implements ActionListener {

    private FrmMainController controller;

    public BtnStartServerActionListener(FrmMainController controller) {
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        adjustForm();
        startServer();
    }

    public void adjustForm() {
        FrmMain view = controller.getView();
        view.getBtnStartServer().setEnabled(false);
        view.getBtnStopServer().setEnabled(true);
        view.getTxtServerStatus().setText(FrmMainController.SERVER_STATUS_ACTIVE);
        view.getMenuSettings().setEnabled(false);

    }

    public void startServer() {
        ServerThread serverThread = new ServerThread();
        GlobalData.getInstance().put(GlobalData.SERVER_THREAD, serverThread);
        serverThread.start();
    }
}
