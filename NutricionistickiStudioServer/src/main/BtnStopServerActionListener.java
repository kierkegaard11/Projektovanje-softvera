/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import global.GlobalData;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import server.ServerThread;

/**
 *
 * @author maja
 */
public class BtnStopServerActionListener implements ActionListener {

    private FrmMainController controller;

    public BtnStopServerActionListener(FrmMainController controller) {
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        adjustForm();
        stopServer();
    }

    private void adjustForm() {
        FrmMain view = controller.getView();
        view.getBtnStartServer().setEnabled(true);
        view.getBtnStopServer().setEnabled(false);
        view.getTxtServerStatus().setText(FrmMainController.SERVER_STATUS_INACTIVE);
        view.getMenuSettings().setEnabled(true);
    }

    private void stopServer() {
        ServerThread serverThread = (ServerThread) GlobalData.getInstance().get(GlobalData.SERVER_THREAD);
        serverThread.kill();
    }

}
