/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import exception.UserAlreadyExistsException;
import exception.UserNotFoundException;
import exception.WrongPasswordException;
import java.util.logging.Level;
import java.util.logging.Logger;
import so.impl.SOCreateDish;
import so.impl.SOCreateMedicalRecord;
import so.impl.SOCreateMenu;
import so.impl.SODeleteMenu;
import so.impl.SOFindAllForUpdateMenu;
import so.impl.SOFindDish;
import so.impl.SOFindIngredient;
import so.impl.SOFindMedicalRecord;
import so.impl.SOFindMenu;
import so.impl.SOFindMenuType;
import so.impl.SOLogin;
import so.impl.SORegister;
import so.impl.SOUpdateDish;
import so.impl.SOUpdateMedicalRecord;
import so.impl.SOUpdateMenu;
import transfer.Operation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class RequestHandler {

    private static final String DEFAULT_ERROR_MESSAGE = "operation.error.unexpected";

    public TransferObject handleRequest(TransferObject request) {
        TransferObject response = new TransferObject();
        switch (request.getOperation()) {
            case LOGIN:
                handleLogin(request, response);
                break;
            case REGISTER:
                handleRegister(request, response);
                break;
            case NEW_MEDICAL_RECORD:
                handleNewMedicalRecord(response);
                break;
            case UPDATE_MEDICAL_RECORD:
                handleUpdateMedicalRecord(request, response);
                break;
            case FIND_MEDICAL_RECORD:
                handleFindMedicalRecords(request, response);
                break;
            case NEW_DISH:
                handleNewDish(response);
                break;
            case UPDATE_DISH:
                handleUpdateDish(request, response);
                break;
            case FIND_DISH:
                handleFindDish(request, response);
                break;
            case NEW_MENU:
                handleNewMenu(request, response);
                break;
            case UPDATE_MENU:
                handleUpdateMenu(request, response);
                break;
            case DELETE_MENU:
                handleDeleteMenu(request, response);
                break;
            case FIND_MENU:
                handleFindMenu(request, response);
                break;
            case FIND_INGREDIENT:
                handleFindIngredient(request, response);
                break;
            case FIND_MENU_TYPE:
                handleFindMenuType(request, response);
                break;
            case FIND_ALL_FOR_UPDATE_MENU:
                handleFindAllForUpdateMenu(request, response);
                break;
            default:
                break;
        }
        return response;
    }

    private void handleLogin(TransferObject request, TransferObject response) {
        try {
            new SOLogin().execute(request, response);
            response.setOperation(Operation.LOGIN_SUCCESSFUL);
        } catch (UserNotFoundException | WrongPasswordException ex) {
            response.setOperation(Operation.LOGIN_UNSUCCESSFUL);
            response.put(TransferObject.MESSAGE, ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.LOGIN_UNSUCCESSFUL);
            response.put(TransferObject.MESSAGE, DEFAULT_ERROR_MESSAGE);
        }
    }

    private void handleRegister(TransferObject request, TransferObject response) {
        try {
            new SORegister().execute(request, response);
            response.setOperation(Operation.REGISTRATION_SUCCESSFUL);
        } catch (UserAlreadyExistsException ex) {
            response.setOperation(Operation.REGISTRATION_UNSUCCESSFUL);
            response.put(TransferObject.MESSAGE, ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.REGISTRATION_UNSUCCESSFUL);
            response.put(TransferObject.MESSAGE, DEFAULT_ERROR_MESSAGE);
        }
    }

    private void handleNewMedicalRecord(TransferObject response) {
        try {
            new SOCreateMedicalRecord().execute(null, response);
            response.setOperation(Operation.MEDICAL_RECORD_CREATE_SUCCESS);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.MEDICAL_RECORD_CREATE_ERROR);
            response.put(TransferObject.MESSAGE, SOCreateMedicalRecord.ERROR_MESSAGE);
        }
    }

    private void handleUpdateMedicalRecord(TransferObject request, TransferObject response) {
        try {
            new SOUpdateMedicalRecord().execute(request, response);
            response.setOperation(Operation.MEDICAL_RECORD_UPDATE_SUCCESS);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.MEDICAL_RECORD_UPDATE_ERROR);
            response.put(TransferObject.MESSAGE, SOUpdateMedicalRecord.ERROR_MESSAGE);
        }
    }

    private void handleFindMedicalRecords(TransferObject request, TransferObject response) {
        try {
            new SOFindMedicalRecord().execute(request, response);
            response.setOperation(Operation.MEDICAL_RECORD_FOUND);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.MEDICAL_RECORD_NOT_FOUND);
            response.put(TransferObject.MESSAGE, SOFindMedicalRecord.ERROR_MESSAGE);
        }
    }

    private void handleNewDish(TransferObject response) {
        try {
            new SOCreateDish().execute(null, response);
            response.setOperation(Operation.DISH_CREATE_SUCCESS);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.DISH_CREATE_ERROR);
            response.put(TransferObject.MESSAGE, SOCreateDish.ERROR_MESSAGE);
        }
    }

    private void handleUpdateDish(TransferObject request, TransferObject response) {
        try {
            new SOUpdateDish().execute(request, response);
            response.setOperation(Operation.DISH_UPDATE_SUCCESS);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.DISH_UPDATE_ERROR);
            response.put(TransferObject.MESSAGE, SOUpdateDish.ERROR_MESSAGE);
        }
    }

    private void handleFindDish(TransferObject request, TransferObject response) {
        try {
            new SOFindDish().execute(request, response);
            response.setOperation(Operation.DISH_FOUND);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.DISH_NOT_FOUND);
            response.put(TransferObject.MESSAGE, SOFindDish.ERROR_MESSAGE);
        }
    }

    private void handleNewMenu(TransferObject request, TransferObject response) {
        try {
            new SOCreateMenu().execute(null, response);
            response.setOperation(Operation.MENU_CREATE_SUCCESS);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.MENU_CREATE_ERROR);
            response.put(TransferObject.MESSAGE, SOCreateMenu.ERROR_MESSAGE);
        }
    }

    private void handleUpdateMenu(TransferObject request, TransferObject response) {
        try {
            new SOUpdateMenu().execute(request, response);
            response.setOperation(Operation.UPDATE_MENU_SUCCESS);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.UPDATE_MENU_ERROR);
            response.put(TransferObject.MESSAGE, SOUpdateMenu.ERROR_MESSAGE);
        }
    }

    private void handleDeleteMenu(TransferObject request, TransferObject response) {
        try {
            new SODeleteMenu().execute(request, response);
            response.setOperation(Operation.DELETE_MENU_SUCCESS);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.DELETE_MENU_ERROR);
            response.put(TransferObject.MESSAGE, SODeleteMenu.ERROR_MESSAGE);
        }
    }

    private void handleFindMenu(TransferObject request, TransferObject response) {
        try {
            new SOFindMenu().execute(request, response);
            response.setOperation(Operation.FIND_MENU_SUCCESS);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.FIND_MENU_ERROR);
            response.put(TransferObject.MESSAGE, SOFindMenu.ERROR_MESSAGE);
        }
    }

    private void handleFindIngredient(TransferObject request, TransferObject response) {
        try {
            new SOFindIngredient().execute(request, response);
            response.setOperation(Operation.FIND_INGREDIENT_SUCCESS);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.FIND_INGREDIENT_ERROR);
            response.put(TransferObject.MESSAGE, DEFAULT_ERROR_MESSAGE);
        }
    }

    private void handleFindMenuType(TransferObject request, TransferObject response) {
        try {
            new SOFindMenuType().execute(request, response);
            response.setOperation(Operation.FIND_MENU_TYPE_SUCCESS);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.FIND_MENU_TYPE_ERROR);
            response.put(TransferObject.MESSAGE, DEFAULT_ERROR_MESSAGE);
        }
    }

    private void handleFindAllForUpdateMenu(TransferObject request, TransferObject response) {
        try {
            new SOFindAllForUpdateMenu().execute(request, response);
            response.setOperation(Operation.FIND_ALL_FOR_UPDATE_MENU_SUCCESS);
        } catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            response.setOperation(Operation.FIND_ALL_FOR_UPDATE_MENU_ERROR);
            response.put(TransferObject.MESSAGE, DEFAULT_ERROR_MESSAGE);
        }
    }

}
