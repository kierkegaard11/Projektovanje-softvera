/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public class ClientStorage {

    public static final String CLIENT_STORAGE_KEY = "Client Storage";

    private List<ClientHandlerThread> clients;

    public ClientStorage() {
        clients = new ArrayList<>();
    }

    public List<ClientHandlerThread> getAll() {
        return clients;
    }

    public void remove(ClientHandlerThread clientHandlerThread) {
        clients.remove(clientHandlerThread);
    }

    public void removeAll() {
        clients = new ArrayList<>();
    }

    public void add(ClientHandlerThread clientHandlerThread) {
        clients.add(clientHandlerThread);
    }
}
