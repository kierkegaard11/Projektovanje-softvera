/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import global.GlobalData;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class ClientHandlerThread extends Thread {

    private Socket socket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private RequestHandler requestHandler;

    public ClientHandlerThread(Socket socket) throws IOException {
        this.socket = socket;
        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socket.getInputStream());
        requestHandler = new RequestHandler();
    }

    public void kill() {
        try {
            socket.close();
        } catch (IOException ex) {
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                TransferObject request = (TransferObject) in.readObject();
                TransferObject response = requestHandler.handleRequest(request);
                send(response);
            }
        } catch (IOException ex) {
            ClientStorage clientStorage = (ClientStorage) GlobalData.getInstance().get(ClientStorage.CLIENT_STORAGE_KEY);
            clientStorage.remove(this);
            GlobalData.getInstance().put(ClientStorage.CLIENT_STORAGE_KEY, clientStorage);
            System.out.println("Client disconnected.");
        } catch (ClassNotFoundException ex) {
        }
    }

    public void send(TransferObject transferObject) throws IOException {
        out.writeObject(transferObject);
    }

}
