/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.awt.event.KeyAdapter;

/**
 *
 * @author maja
 */
public class TxtDatabaseNameKeyReleased extends KeyAdapter {

    private FrmDatabaseSettingsController controller;

    public TxtDatabaseNameKeyReleased(FrmDatabaseSettingsController controller) {
        this.controller = controller;
    }
    
    @Override
    public void keyReleased(java.awt.event.KeyEvent evt) {
        updateDatabaseUrl();
    }

    private void updateDatabaseUrl() {
        FrmDatabaseSettings view = controller.getView();
        DatabaseProperties model = controller.getModel();
        String databaseName = view.getTxtDatabaseName().getText().trim();
        String url = model.getProperty(DatabaseProperties.URL_KEY, DatabaseProperties.URL);
        view.getTxtUrl().setText(url + databaseName);
    }
    
}
