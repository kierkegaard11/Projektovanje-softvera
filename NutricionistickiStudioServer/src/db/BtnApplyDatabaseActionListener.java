/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author maja
 */
public class BtnApplyDatabaseActionListener implements ActionListener {

    private FrmDatabaseSettingsController controller;

    public BtnApplyDatabaseActionListener(FrmDatabaseSettingsController controller) {
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        saveProperties();
    }

    private void saveProperties() {
        String dbName = controller.getView().getTxtDatabaseName().getText().trim();
        String username = controller.getView().getTxtUsername().getText().trim();
        String password = new String(controller.getView().getTxtPassword().getPassword());
        DatabaseProperties model = controller.getModel();
        model.setProperties(dbName, username, password);
        JOptionPane.showMessageDialog(controller.getView(), "Changes saved!");
        controller.getView().dispose();
    }

}
