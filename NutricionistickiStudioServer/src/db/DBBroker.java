/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class DBBroker {

    private Connection connection = null;
    private String driver;
    private String url;
    private String dbusername;
    private String dbpassword;
    private String dbName;
    private static DBBroker instance;

    private DBBroker() {
        makeConnection();
    }

    public static DBBroker getInstance() {
        if (instance == null) {
            instance = new DBBroker();
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }

    public boolean makeConnection() {
        readConfigProperties();
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, dbusername, dbpassword);
            connection.setAutoCommit(false);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    void readConfigProperties() {
        DatabaseProperties props = new DatabaseProperties();
        driver = props.getProperty(DatabaseProperties.DRIVER_KEY, DatabaseProperties.DRIVER);
        dbName = props.getProperty(DatabaseProperties.DATABASE_NAME_KEY, DatabaseProperties.DATABASE_NAME);
        url = props.getProperty(DatabaseProperties.URL_KEY, DatabaseProperties.URL) + dbName;
        dbusername = props.getProperty(DatabaseProperties.USERNAME_KEY);
        dbpassword = props.getProperty(DatabaseProperties.PASSWORD_KEY);
    }

    public Integer insertRecord(Persistence persistenceObject) {
        String query = "INSERT INTO " + persistenceObject.getClassName() + " VALUES (" + persistenceObject.getAtrValue() + ")";
        System.out.println(query);
        return executeUpdateAndReturnId(query);
    }

    public boolean insertRecordAggregation(Persistence persistenceObject) {

        String query = "INSERT INTO " + persistenceObject.getClassName() + " VALUES (" + persistenceObject.getAtrValue() + ")";
        System.out.println(query);
        return executeUpdate(query);

    }

    public boolean deleteRecord(Persistence persistenceObject) {
        String query = "DELETE FROM " + persistenceObject.getClassName() + " WHERE " + persistenceObject.getWhereCondition();
        return executeUpdate(query);
    }

    public boolean updateRecord(Persistence persistenceObject, String whereCondition) {
        String query = "UPDATE " + persistenceObject.getClassName() + " SET " + persistenceObject.setAtrValue() + " WHERE " + whereCondition;
        System.out.println(query);
        return executeUpdate(query);
    }

    public boolean executeUpdate(String query) {
        Statement statement = null;
        boolean signal = false;
        try {

            statement = connection.prepareStatement(query);
            int rowcount = statement.executeUpdate(query);
            if (rowcount > 0) {
                signal = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
            signal = false;
        } finally {
            close(null, statement, null);
        }
        return signal;
    }

    public Integer executeUpdateAndReturnId(String query) {
        Statement statement = null;
        int id = -1;
        try {
            statement = connection.createStatement();
            int rowcount = statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            if (rowcount > 0) {
                ResultSet rs = statement.getGeneratedKeys();
                rs.next();
                id = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close(null, statement, null);
        }
        return id;
    }

    public Persistence findRecord(Persistence persistenceObject, String whereCondition) {
        ResultSet resultSet = null;
        Statement statement = null;
        String query = "SELECT * FROM " + persistenceObject.getClassName() + " WHERE " + whereCondition;
        System.out.println(query);
        boolean signal;
        try {
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery(query);
            signal = resultSet.next();
            if (signal == true) {
                persistenceObject = persistenceObject.getNewRecord(resultSet);
            } else {
                persistenceObject = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close(null, statement, resultSet);
        }
        return persistenceObject;
    }

    public List<Persistence> findRecords(Persistence persistenceObject) {
        return findRecords(persistenceObject, null);
    }
    
    public List<Persistence> findRecords(Persistence persistenceObject, String whereCondition) {
        String query = "SELECT * FROM " + persistenceObject.getClassName();
        if (whereCondition != null && !whereCondition.isEmpty()) {
            query += " WHERE " + whereCondition;
        }
        System.out.println(query);

        ResultSet resultSet = null;
        Statement statement = null;

        List<Persistence> persistenceObjects = new ArrayList<>();
        try {
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                persistenceObject = persistenceObject.getNewRecord(resultSet);
                persistenceObjects.add(persistenceObject);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close(null, statement, resultSet);
        }
        return persistenceObjects;
    }

    public boolean commitTransation() {
        try {
            connection.commit();
        } catch (SQLException esql) {
            return false;
        }
        return true;
    }

    public boolean rollbackTransation() {
        try {
            connection.rollback();
        } catch (SQLException esql) {
            return false;
        }

        return true;
    }

    public void closeConnection() {
        close(connection, null, null);
    }

    public void close(Connection connection, Statement statement, ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
