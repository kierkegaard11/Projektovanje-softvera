/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

/**
 *
 * @author maja
 */
public class FrmDatabaseSettingsController {

    private FrmDatabaseSettings view;
    private DatabaseProperties model;

    public FrmDatabaseSettingsController(FrmDatabaseSettings view, DatabaseProperties model) {
        this.view = view;
        this.model = model;
        initialize();
    }
    
    private void initialize() {
        addListeners();
        prepareView();
    }

    private void addListeners() {
        view.getBtnApplyDatabase().addActionListener(new BtnApplyDatabaseActionListener(this));
        view.getTxtDatabaseName().addKeyListener(new TxtDatabaseNameKeyReleased(this));
    }

    private void prepareView() {
        String databaseName = model.getProperty(DatabaseProperties.DATABASE_NAME_KEY);
        String url = model.getProperty(DatabaseProperties.URL_KEY, DatabaseProperties.URL);
        String username = model.getProperty(DatabaseProperties.USERNAME_KEY);
        String password = model.getProperty(DatabaseProperties.PASSWORD_KEY);
        
        view.getTxtDatabaseName().setText(databaseName);
        view.getTxtUrl().setText(url + databaseName);
        view.getTxtUsername().setText(username);
        view.getTxtPassword().setText(password);
    }

    public void showForm() {
        view.setLocationRelativeTo(null);
        view.setVisible(true);
    }

    public FrmDatabaseSettings getView() {
        return view;
    }

    public DatabaseProperties getModel() {
        return model;
    }
    
}
