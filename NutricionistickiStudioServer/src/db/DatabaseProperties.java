/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import global.GenericProperties;

/**
 *
 * @author maja
 */
public class DatabaseProperties extends GenericProperties {

    public static final String DRIVER_KEY = "driver";
    public static final String URL_KEY = "url";
    public static final String DATABASE_NAME_KEY = "dbName";
    public static final String USERNAME_KEY = "dbusername";
    public static final String PASSWORD_KEY = "dbpassword";
    public static final String DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_CONFIG_FILE_PATH = "db_config.properties";
    public static final String URL = "jdbc:mysql://127.0.0.1:3306/";
    public static final String DATABASE_NAME = "ps_db";

    public DatabaseProperties() {
        super();
    }

    @Override
    protected String getFilePath() {
        return DB_CONFIG_FILE_PATH;
    }

    public void setProperties(String dbName, String dbUsername, String dbPassword) {
        props.setProperty(DATABASE_NAME_KEY, dbName);
        props.setProperty(DRIVER_KEY, DRIVER);
        props.setProperty(URL_KEY, URL);
        props.setProperty(USERNAME_KEY, dbUsername);
        props.setProperty(PASSWORD_KEY, dbPassword);
        storeProperties();
    }
}
