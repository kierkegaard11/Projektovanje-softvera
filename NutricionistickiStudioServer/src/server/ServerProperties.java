/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import global.GenericProperties;

/**
 *
 * @author maja
 */

public class ServerProperties extends GenericProperties {

    public static final String SERVER_CONFIG_FILE_PATH = "server_config.properties";
    public static final String PORT_KEY = "port";

    public ServerProperties() {
        super();
    }

    @Override
    protected String getFilePath() {
        return SERVER_CONFIG_FILE_PATH;
    }

    public void setProperties(int port) {
        props.setProperty(PORT_KEY, port + "");
        storeProperties();
    }
}
