/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author maja
 */
public class BtnApplyServerActionListener implements ActionListener {

    private FrmServerSettingsController controller;

    public BtnApplyServerActionListener(FrmServerSettingsController controller) {
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        saveProperties();
    }
    
    private void saveProperties() {
        int port = Integer.parseInt(controller.getView().getTxtPort().getText());
        ServerProperties model = controller.getModel();
        model.setProperties(port);
        JOptionPane.showMessageDialog(controller.getView(), "Changes saved!");
        controller.getView().dispose();
    }

}
