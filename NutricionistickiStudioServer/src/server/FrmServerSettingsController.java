/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author maja
 */
public class FrmServerSettingsController {

    private FrmServerSettings view;
    private ServerProperties model;

    public FrmServerSettingsController(FrmServerSettings view, ServerProperties model) {
        this.view = view;
        this.model = model;
        initialize();
    }
    
    private void initialize() {
        addListeners();
        prepareView();
    }

    private void prepareView() {
        InputStream in = null;
        try {
            Properties properties = new Properties();
            in = new FileInputStream(ServerProperties.SERVER_CONFIG_FILE_PATH);
            properties.load(in);
            view.getTxtPort().setText(properties.getProperty(ServerProperties.PORT_KEY));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FrmServerSettingsController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FrmServerSettingsController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(FrmServerSettingsController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void addListeners() {
        view.getBtnApplyServer().addActionListener(new BtnApplyServerActionListener(this));
    }

    public void showForm() {
        view.setVisible(true);
        view.setLocationRelativeTo(null);
    }

    public FrmServerSettings getView() {
        return view;
    }

    public ServerProperties getModel() {
        return model;
    }
}
