/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import client.ClientHandlerThread;
import client.ClientStorage;
import global.GlobalData;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author maja
 */
public class ServerThread extends Thread {

    private ServerSocket serverSocket;
    private int port;

    public ServerThread() {
        loadProperties();
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException ex) {
        }
        ClientStorage clientStorage = new ClientStorage();
        GlobalData.getInstance().put(ClientStorage.CLIENT_STORAGE_KEY, clientStorage);
    }

    public void kill() {
        try {
            serverSocket.close();
        } catch (IOException ex) {
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                System.out.println("Waiting for clients...");
                Socket socket = serverSocket.accept();
                ClientHandlerThread ct = new ClientHandlerThread(socket);
                ClientStorage clientStorage = (ClientStorage) GlobalData.getInstance().get(ClientStorage.CLIENT_STORAGE_KEY);
                clientStorage.add(ct);
                GlobalData.getInstance().put(ClientStorage.CLIENT_STORAGE_KEY, clientStorage);
                ct.start();
            }
        } catch (IOException ex) {
            ClientStorage clientStorage = (ClientStorage) GlobalData.getInstance().get(ClientStorage.CLIENT_STORAGE_KEY);
            for (ClientHandlerThread ct : clientStorage.getAll()) {
                ct.kill();
            }
            clientStorage.removeAll();
            GlobalData.getInstance().put(ClientStorage.CLIENT_STORAGE_KEY, clientStorage);
        }
    }

    private void loadProperties() {
        Properties properties = new Properties();
        InputStream in = null;

        try {
            in = new FileInputStream(ServerProperties.SERVER_CONFIG_FILE_PATH);
            properties.load(in);
            port = Integer.parseInt(properties.getProperty(ServerProperties.PORT_KEY));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
