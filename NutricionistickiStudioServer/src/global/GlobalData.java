/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package global;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author maja
 */
public class GlobalData {

    public static final String SERVER_THREAD = "Server Thread";
    
    private static GlobalData instance;
    private Map<String, Object> data;

    private GlobalData() {
        data = new HashMap<>();
    }

    public static GlobalData getInstance() {
        if (instance == null) {
            instance = new GlobalData();
        }
        return instance;
    }

    public void put(String key, Object value) {
        data.put(key, value);
    }

    public Object get(String key) {
        return data.get(key);
    }
}
