/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import java.util.List;
import model.Dish;
import model.IngredientDishAggregate;
import model.persistence.Persistence;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOUpdateDish extends SystemOperation {

    public static final String ERROR_MESSAGE = "operation.dishUpdate.error";

    @Override
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        Dish dish = (Dish) request.get(Dish.DISH_KEY);
        String conditionDish = Dish.COL_DISH_ID + " = " + dish.getDishId();
        boolean signal = dbBroker.updateRecord(dish, conditionDish);
        if (!signal) {
            throw new Exception();
        }
        updateIngredientDishAggregates(dish);
        response.put(Dish.DISH_KEY, dish);
    }

    private void updateIngredientDishAggregates(Dish dish) throws Exception {
        for (IngredientDishAggregate aggregate : dish.getIngredients()) {
            String condition = IngredientDishAggregate.COL_DISH_ID + " = " + aggregate.getDish().getDishId() + " AND " + IngredientDishAggregate.COL_INGREDIENT_ID + "=" + aggregate.getIngredient().getIngredientId();
            Persistence existingAggregate = dbBroker.findRecord(new IngredientDishAggregate(), condition);
            if (existingAggregate != null) {
                updateExistingAggregate(aggregate, condition);
            } else {
                insertNewAggregate(aggregate);
            }
        }
        deleteMissingAggregates(dish);
    }

    private void updateExistingAggregate(IngredientDishAggregate aggregate, String condition) throws Exception {
        boolean signal = dbBroker.updateRecord(aggregate, condition);
        if (!signal) {
            throw new Exception();
        }
    }

    private void insertNewAggregate(IngredientDishAggregate aggregate) throws Exception {
        boolean signal = dbBroker.insertRecordAggregation(aggregate);
        if (!signal) {
            throw new Exception();
        }
    }

    private void deleteMissingAggregates(Dish dish) throws Exception {
        String condition = Dish.COL_DISH_ID + "=" + dish.getDishId();
        List<Persistence> existingAggregates = dbBroker.findRecords(new IngredientDishAggregate(), condition);
        for (Persistence persistence : existingAggregates) {
            if (!dish.getIngredients().contains((IngredientDishAggregate) persistence)) {
                boolean signal = dbBroker.deleteRecord(persistence);
                if (!signal) {
                    throw new Exception();
                }
            }
        }
    }

}
