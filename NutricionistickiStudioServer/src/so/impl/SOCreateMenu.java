/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import java.util.List;
import model.Dish;
import model.MedicalRecord;
import model.Menu;
import model.MenuType;
import model.persistence.Persistence;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOCreateMenu extends SystemOperation {

    public static final String ERROR_MESSAGE = "operation.menuCreate.error";
    
    @Override
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        Menu menu = new Menu();
        Integer id = dbBroker.insertRecord(menu);
        menu.setMenuId(id);
        response.put(Menu.MENU_KEY, menu);
        
        List<Persistence> menuTypes = dbBroker.findRecords(new MenuType());
        response.put(MenuType.MENU_TYPE_KEY, menuTypes);
        
        List<Persistence> medicalRecords = dbBroker.findRecords(new MedicalRecord());
        response.put(MedicalRecord.MEDICAL_RECORD_KEY, medicalRecords);
        
        List<Persistence> dishes = dbBroker.findRecords(new Dish());
        response.put(Dish.DISH_KEY, dishes);
    }
}
