/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import model.MedicalRecord;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOCreateMedicalRecord extends SystemOperation {

    public static final String ERROR_MESSAGE = "operation.medicalRecordCreate.error";

    @Override
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        MedicalRecord medicalRecord = new MedicalRecord();
        Integer id = dbBroker.insertRecord(medicalRecord);
        medicalRecord.setMedicalRecordId(id);
        response.put(MedicalRecord.MEDICAL_RECORD_KEY, medicalRecord);
    }

}
