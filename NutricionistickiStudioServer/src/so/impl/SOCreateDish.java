/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import java.util.List;
import model.Dish;
import model.Ingredient;
import model.persistence.Persistence;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOCreateDish extends SystemOperation {

    public static final String ERROR_MESSAGE = "operation.dishCreate.error";
    
    @Override
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        Dish dish = new Dish();
        Integer id = dbBroker.insertRecord(dish);
        dish.setDishId(id);
        response.put(Dish.DISH_KEY, dish);
        
        List<Persistence> ingredients = dbBroker.findRecords(new Ingredient());
        response.put(Ingredient.INGREDIENT_KEY, ingredients);
    }

}
