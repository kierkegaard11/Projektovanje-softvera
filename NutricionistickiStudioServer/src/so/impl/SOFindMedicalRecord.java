/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import model.MedicalRecord;
import model.Menu;
import model.MenuType;
import model.persistence.Persistence;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOFindMedicalRecord extends SystemOperation {

    public static final String ERROR_MESSAGE = "operation.medicalRecordFind.error";

    @Override
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        Map<String, Object> filters = request.getData();
        String whereCondition = createWhereCondition(filters);
        List<Persistence> filteredMedicalRecords = dbBroker.findRecords(new MedicalRecord(), whereCondition);
        List<Persistence> filteredMedicalRecordsWithMenuType = getMedicalRecordsWithMenuType(filteredMedicalRecords, filters);
        response.put(MedicalRecord.MEDICAL_RECORD_KEY, filteredMedicalRecordsWithMenuType);
    }

    private String createWhereCondition(Map<String, Object> filters) {
        String whereCondition = "";
        whereCondition += createWhereStringEqualsCondition(MedicalRecord.COL_FIRST_NAME, filters, false);
        whereCondition += createWhereStringEqualsCondition(MedicalRecord.COL_LAST_NAME, filters, !whereCondition.isEmpty());
        whereCondition += createWhereIntegerEqualsCondition(MedicalRecord.COL_MEDICAL_RECORD_ID, filters, !whereCondition.isEmpty());
        whereCondition += createWhereStringEqualsCondition(MedicalRecord.COL_GENDER, filters, !whereCondition.isEmpty());
        return whereCondition;
    }

    private List<Persistence> getMedicalRecordsWithMenuType(List<Persistence> filteredMedicalRecords, Map filters) {
        MenuType menuType = (MenuType) filters.get(MenuType.MENU_TYPE_KEY);
        if (menuType == null) {
            return filteredMedicalRecords;
        }
        List<Persistence> menus = findMenus(menuType);
        List<Persistence> medicalRecordsWithMenuType = new ArrayList<>();
        for (Persistence persistence : filteredMedicalRecords) {
            populateMedicalRecords((MedicalRecord) persistence, menus, medicalRecordsWithMenuType);
        }
        return medicalRecordsWithMenuType;
    }

    private List<Persistence> findMenus(MenuType menuType) {
        String whereCondition = Menu.COL_MENU_TYPE + " = " + menuType.getMenuTypeId();
        return dbBroker.findRecords(new Menu(), whereCondition);
    }

    private void populateMedicalRecords(MedicalRecord medicalRecord, List<Persistence> menus, List<Persistence> medicalRecords) {
        for (Persistence persistence : menus) {
            Menu menu = (Menu) persistence;
            if (menu.getMedicalRecord().getMedicalRecordId().equals(medicalRecord.getMedicalRecordId())) {
                medicalRecords.add(medicalRecord);
            }
        }
    }

}
