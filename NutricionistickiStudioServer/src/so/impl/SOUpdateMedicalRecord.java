/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import model.MedicalRecord;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOUpdateMedicalRecord extends SystemOperation {

    public static final String ERROR_MESSAGE = "operation.medicalRecordUpdate.error";
    
    @Override
    public void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        MedicalRecord medicalRecord = (MedicalRecord) request.get(MedicalRecord.MEDICAL_RECORD_KEY);
        String condition = MedicalRecord.COL_MEDICAL_RECORD_ID + " = " + medicalRecord.getMedicalRecordId();
        boolean signal = dbBroker.updateRecord(medicalRecord, condition);
        if (!signal) {
            throw new Exception();
        }
        response.put(MedicalRecord.MEDICAL_RECORD_KEY, medicalRecord);
    }

}
