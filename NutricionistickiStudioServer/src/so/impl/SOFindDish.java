/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import model.Dish;
import model.Ingredient;
import model.IngredientDishAggregate;
import model.persistence.Persistence;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOFindDish extends SystemOperation {

    public static final String ERROR_MESSAGE = "operation.dishFind.error";

    @Override
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        Map<String, Object> filters = request.getData();
        String whereCondition = createWhereLikeCondition(Dish.COL_NAME, filters, false);
        List<Persistence> dishesWithName = dbBroker.findRecords(new Dish(), whereCondition);
        populateIngredientDishAggregates(dishesWithName);
        List<Persistence> dishesWithNameAndIngredient = getDishesWithIngredient(dishesWithName, filters);
        response.put(Dish.DISH_KEY, dishesWithNameAndIngredient);
    }

    private void populateIngredientDishAggregates(List<Persistence> dishes) {
        for (Persistence persistence : dishes) {
            Dish dish = (Dish) persistence;
            String whereCondition = IngredientDishAggregate.COL_DISH_ID + " = " + dish.getDishId();
            List<Persistence> ingredientDishAggregates = dbBroker.findRecords(new IngredientDishAggregate(), whereCondition);
            populateIngredients(dish, ingredientDishAggregates);
        }
    }

    private List<Persistence> getDishesWithIngredient(List<Persistence> dishesWithName, Map filters) {
        Ingredient ingredient = (Ingredient) filters.get(Ingredient.INGREDIENT_KEY);
        if (ingredient == null) {
            return dishesWithName;
        }
        List<Persistence> filteredDishesWithIngredient = new ArrayList<>();
        for (Persistence persistence : dishesWithName) {
            Dish dish = (Dish) persistence;
            if (dish.getIngredients().contains(new IngredientDishAggregate(ingredient.getIngredientId(), dish.getDishId()))) {
                filteredDishesWithIngredient.add(dish);
            }
        }
        return filteredDishesWithIngredient;
    }

    private void populateIngredients(Dish dish, List<Persistence> ingredientDishAggregates) {
        for (Persistence persistence : ingredientDishAggregates) {
            IngredientDishAggregate ingredientDishAggregate = (IngredientDishAggregate) persistence;
            ingredientDishAggregate.setDish(dish);
            String whereConditionIngredient = Ingredient.COL_INGREDIENT_ID + " = " + ingredientDishAggregate.getIngredient().getIngredientId();
            Ingredient ingredient = (Ingredient) dbBroker.findRecord(new Ingredient(), whereConditionIngredient);
            ingredientDishAggregate.setIngredient(ingredient);
            dish.getIngredients().add(ingredientDishAggregate);
        }
    }

}
