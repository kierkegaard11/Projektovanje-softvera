/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import model.Dish;
import model.Ingredient;
import model.IngredientDishAggregate;
import model.Meal;
import model.MedicalRecord;
import model.Menu;
import model.MenuType;
import model.persistence.Persistence;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOFindMenu extends SystemOperation {

    public static final String ERROR_MESSAGE = "operation.menuFind.error";

    @Override
    public void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        Map<String, Object> filters = request.getData();
        String whereCondition = createMenusWhereCondition(filters);
        List<Persistence> filteredMenus = dbBroker.findRecords(new Menu(), whereCondition);
        List<Persistence> populatedFilteredMenus = getPopulatedFilteredMenus(filteredMenus, filters);
        response.put(Menu.MENU_KEY, populatedFilteredMenus);
    }

    private String createMenusWhereCondition(Map filters) {
        String whereCondition = "";
        whereCondition += createWhereIntegerEqualsCondition(MedicalRecord.COL_MEDICAL_RECORD_ID, filters, false);
        whereCondition += createWhereIntegerEqualsCondition(Menu.COL_MENU_TYPE, filters, !whereCondition.isEmpty());
        return whereCondition;
    }

    private List<Persistence> getPopulatedFilteredMenus(List<Persistence> filteredMenus, Map filters) {
        List<Persistence> medicalRecords = findMedicalRecords(filters);
        List<Persistence> populatedFilteredMenus = new ArrayList<>();
        for (Persistence persistence : filteredMenus) {
            populateMenu((Menu) persistence, medicalRecords, populatedFilteredMenus);
        }
        return populatedFilteredMenus;
    }

    private List<Persistence> findMedicalRecords(Map filters) {
        String whereCondition = "";
        whereCondition += createWhereStringEqualsCondition(MedicalRecord.COL_FIRST_NAME, filters, false);
        whereCondition += createWhereStringEqualsCondition(MedicalRecord.COL_LAST_NAME, filters, !whereCondition.isEmpty());
        return dbBroker.findRecords(new MedicalRecord(), whereCondition);
    }

    private void populateMenu(Menu menu, List<Persistence> medicalRecords, List<Persistence> populatedFilteredMenus) {
        for (Persistence persistence : medicalRecords) {
            MedicalRecord medicalRecord = (MedicalRecord) persistence;
            if (menu.getMedicalRecord().getMedicalRecordId().equals(medicalRecord.getMedicalRecordId())) {
                populateWithMeals(menu);
                populateWithMedicalRecord(menu);
                populateWithMenuType(menu);
                populatedFilteredMenus.add(menu);
            }
        }
    }

    private void populateWithMeals(Menu menu) {
        String condition = Menu.COL_MENU_ID + " = " + menu.getMenuId();
        List<Persistence> meals = dbBroker.findRecords(new Meal(), condition);
        for (Persistence persistence : meals) {
            Meal meal = (Meal) persistence;
            populateMeal(meal);
            menu.getMeals().add(meal);
        }
    }

    private void populateMeal(Meal meal) {
        String conditionDish = Dish.COL_DISH_ID + "=" + meal.getDish().getDishId();
        Dish dish = (Dish) dbBroker.findRecord(new Dish(), conditionDish);
        List<Persistence> ingredients = dbBroker.findRecords(new IngredientDishAggregate(), conditionDish);
        for (Persistence persistence : ingredients) {
            populateDish(dish, (IngredientDishAggregate) persistence);
        }
        meal.setDish(dish);
    }

    private void populateDish(Dish dish, IngredientDishAggregate ingredientDishAggregate) {
        String conditionIngredient = IngredientDishAggregate.COL_INGREDIENT_ID + " = " + ingredientDishAggregate.getIngredient().getIngredientId();
        Ingredient ingredient = (Ingredient) dbBroker.findRecord(new Ingredient(), conditionIngredient);
        ingredientDishAggregate.setDish(dish);
        ingredientDishAggregate.setIngredient(ingredient);
        dish.getIngredients().add(ingredientDishAggregate);
    }

    private void populateWithMedicalRecord(Menu menu) {
        String condition = Menu.COL_MEDICAL_RECORD + " = " + menu.getMedicalRecord().getMedicalRecordId();
        MedicalRecord medicalRecord = (MedicalRecord) dbBroker.findRecord(new MedicalRecord(), condition);
        menu.setMedicalRecord(medicalRecord);
    }

    private void populateWithMenuType(Menu menu) {
        String condition = Menu.COL_MENU_TYPE + " = " + menu.getMenuType().getMenuTypeId();
        MenuType menuType = (MenuType) dbBroker.findRecord(new MenuType(), condition);
        menu.setMenuType(menuType);
    }

}
