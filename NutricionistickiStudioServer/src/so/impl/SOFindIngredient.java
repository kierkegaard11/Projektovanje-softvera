/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import java.util.List;
import model.Ingredient;
import model.persistence.Persistence;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOFindIngredient extends SystemOperation {

    @Override
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        List<Persistence> ingredients = dbBroker.findRecords(new Ingredient());
        response.put(Ingredient.INGREDIENT_KEY, ingredients);
    }
    
}
