/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import java.util.List;
import model.Meal;
import model.Menu;
import model.persistence.Persistence;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOUpdateMenu extends SystemOperation {

    public static final String ERROR_MESSAGE = "operation.menuUpdate.error";

    @Override
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        Menu menu = (Menu) request.get(Menu.MENU_KEY);
        String conditionMenu = Menu.COL_MENU_ID + " = " + menu.getMenuId();
        boolean signal = dbBroker.updateRecord(menu, conditionMenu);
        if (!signal) {
            throw new Exception();
        }
        updateMeals(menu);
        response.put(Menu.MENU_KEY, menu);
    }

    private void updateMeals(Menu menu) throws Exception {
        for (Meal meal : menu.getMeals()) {
            String condition = Meal.COL_MEAL_ID + " = " + meal.getMealId();
            Meal existingMeal = (Meal) dbBroker.findRecord(new Meal(), condition);
            if (existingMeal != null) {
                updateExistingMeal(meal, condition);
            } else {
                insertNewMeal(meal);
            }
        }
        deleteMissingMeals(menu);
    }

    private void updateExistingMeal(Meal meal, String condition) throws Exception {
        boolean signal = dbBroker.updateRecord(meal, condition);
        if (!signal) {
            throw new Exception();
        }
    }

    private void insertNewMeal(Meal meal) throws Exception {
        Integer index = dbBroker.insertRecord(meal);
        if (index == -1) {
            throw new Exception();
        }
    }

    private void deleteMissingMeals(Menu menu) throws Exception {
        String condition = Meal.COL_MENU_ID + " = " + menu.getMenuId();
        List<Persistence> meals = dbBroker.findRecords(new Meal(), condition);
        for (Persistence persistence : meals) {
            if (!menu.getMeals().contains((Meal) persistence)) {
                boolean signal = dbBroker.deleteRecord(persistence);
                if (!signal) {
                    throw new Exception();
                }
            }
        }
    }

}
