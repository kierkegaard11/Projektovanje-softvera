/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import exception.UserNotFoundException;
import exception.WrongPasswordException;
import model.User;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOLogin extends SystemOperation {

    private static final String WHERE_CONDITION_PATTERN = User.COL_USERNAME + " = '%s'";

    @Override
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        String username = (String) request.get(User.COL_USERNAME);
        String password = (String) request.get(User.COL_PASSWORD);
        User user = new User(username, password);
        User returnedUser = findUser(user);
        if (returnedUser == null) {
            throw new UserNotFoundException(UserNotFoundException.DEFAULT_MESSAGE);
        }
        if (!returnedUser.getPassword().equals(user.getPassword())) {
            throw new WrongPasswordException(WrongPasswordException.DEFAULT_MESSAGE);
        }
        response.put(User.USER_KEY, returnedUser);
    }

    private User findUser(User user) {
        String whereCondition = String.format(WHERE_CONDITION_PATTERN, user.getUsername());
        return (User) dbBroker.findRecord(user, whereCondition);
    }
}
