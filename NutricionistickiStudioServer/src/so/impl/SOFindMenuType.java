/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import java.util.List;
import model.MenuType;
import model.persistence.Persistence;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SOFindMenuType extends SystemOperation {

    @Override
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        List<Persistence> menuTypes = dbBroker.findRecords(new MenuType());
        response.put(MenuType.MENU_TYPE_KEY, menuTypes);
    }
    
}
