/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.impl;

import model.Meal;
import model.Menu;
import so.SystemOperation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class SODeleteMenu extends SystemOperation {
    
    public static final String ERROR_MESSAGE = "operation.menuDelete.error";
    
    @Override
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
        Menu menu = (Menu) request.get(Menu.MENU_KEY);
        boolean signal = dbBroker.deleteRecord(menu);
        if (!signal) {
            throw new Exception();
        }
        deleteMeals(menu);
        response.put(Menu.MENU_KEY, menu);
    }
    
    private void deleteMeals(Menu menu) {
        if (menu.getMeals() != null) {
            for (Meal m : menu.getMeals()) {
                dbBroker.deleteRecord(m);
            }
        }
    }
}
