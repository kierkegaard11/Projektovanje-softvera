/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so;

import db.DBBroker;
import java.util.Map;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public abstract class SystemOperation {

    protected DBBroker dbBroker;

    public SystemOperation() {
        dbBroker = DBBroker.getInstance();
    }

    public void execute(TransferObject request, TransferObject response) throws Exception {
        try {
            dbBroker.getConnection().setAutoCommit(false);
            abstractExecute(request, response);
            System.out.println("Commit transaction");
            commitTransaction();
        } catch (Exception ex) {
            System.out.println("Exception caught");
            rollbackTransaction();
            throw ex;
        }
    }
    
    protected void abstractExecute(TransferObject request, TransferObject response) throws Exception {
    }

    private void commitTransaction() throws Exception {
        System.out.println("commiting");
        try {
            dbBroker.commitTransation();
        } catch (Exception ex) {
            throw new Exception("Commit transaction error.");
        }
    }

    private void rollbackTransaction() throws Exception {
        System.out.println("rollback");
        try {
            dbBroker.rollbackTransation();
        } catch (Exception ex) {
            throw new Exception("Rollback transaction error.");
        }
    }
    
    protected String createWhereLikeCondition(String colName, Map filters, boolean and) {
        String colValue = (String) filters.get(colName);
        String andString = and ? " AND " : "";
        if (colValue != null && !colValue.isEmpty()) {
            return andString + colName + " LIKE '%" + colValue + "%'";
        }
        return "";
    }
    
    protected String createWhereStringEqualsCondition(String colName, Map filters, boolean and) {
        String colValue = (String) filters.get(colName);
        String andString = and ? " AND " : "";
        if (colValue != null && !colValue.isEmpty()) {
            return andString + colName + " = '" + colValue + "'";
        }
        return "";
    }
    
    protected String createWhereIntegerEqualsCondition(String colName, Map filters, boolean and) {
        Integer colValue = (Integer) filters.get(colName);
        String andString = and ? " AND " : "";
        if (colValue != null) {
            return andString + colName + " = " + colValue;
        }
        return "";
    }

}
