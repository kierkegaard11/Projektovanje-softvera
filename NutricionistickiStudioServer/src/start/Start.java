/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package start;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.FrmMainController;
import model.Dish;
import model.Meal;
import model.MealDay;
import model.MealType;
import model.MedicalRecord;
import model.Menu;
import model.MenuType;
import so.SystemOperation;
import so.impl.SOUpdateMenu;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class Start {

    public static void main(String[] args) {
        FrmMainController frmMainController = new FrmMainController();
        frmMainController.showForm();

//        ASDASD();

    }
    
    private static void ASDASD() {
        SystemOperation so = new SOUpdateMenu();
        TransferObject request = new TransferObject();
//        //request.put(MedicalRecord.COL_FIRST_NAME, "Maja");
//        //request.put(MedicalRecord.COL_LAST_NAME, "Miljanic");
//        //request.put(MedicalRecord.COL_MEDICAL_RECORD_ID, 8);
//        //request.put(MedicalRecord.COL_FIRST_NAME, "Maja");
//        //request.put(MedicalRecord.COL_MEDICAL_RECORD_ID, 8);
//        //request.put(MedicalRecord.COL_LAST_NAME, "Milenkovic");
//        //request.put(Menu.COL_MENU_TYPE, new MenuType(1));
//        //request.put(MenuType.MENU_TYPE_KEY, new MenuType(1));
//        //request.put(Dish.COL_NAME, "Boranija");
//        //request.put(Ingredient.INGREDIENT_KEY, new Ingredient(6));
        Meal meal1 = new Meal(1, 1, MealType.LUNCH, MealDay.SATURDAY, 400, new Dish(6));
        Meal meal2 = new Meal(2, 1, MealType.DINNER, MealDay.SATURDAY, 400, new Dish(7));
        List<Meal> meals = new ArrayList<>();
        meals.add(meal2);
        meals.add(meal1);
        Menu menu = new Menu(1, new Date(), new Date(), meals, new MedicalRecord(0), new MenuType(1));
        request.put(Menu.MENU_KEY, menu);
//
        TransferObject response = new TransferObject();
        try {
            so.execute(request, response);
//            List<Menu> menus = (List<Menu>) response.get(Menu.MENU_KEY);
//            for (Menu m : menus) {
//                System.out.println(m.toString());
//
//            }
        } catch (Exception ex) {
            Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
