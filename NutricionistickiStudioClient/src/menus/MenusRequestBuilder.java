/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus;

import builder.RequestBuilder;
import model.MedicalRecord;
import model.Menu;
import model.MenuType;
import transfer.Operation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class MenusRequestBuilder extends RequestBuilder {

    public TransferObject buildNewMenuRequest() {
        return createRequest(Operation.NEW_MENU)
                .build();
    }

    TransferObject buildFindMenusRequest() {
        MenusPanel panel = (MenusPanel) controller.getPanel();
        return createRequest(Operation.FIND_MENU)
                .put(MedicalRecord.COL_FIRST_NAME, getString(panel.getTxtFirstName()))
                .put(MedicalRecord.COL_LAST_NAME, getString(panel.getTxtLastName()))
                .put(Menu.COL_MENU_TYPE, getMenuTypeId(panel))
                .put(MedicalRecord.COL_MEDICAL_RECORD_ID, getInteger(panel.getTxtMedicalRecordId()))
                .build();
    }
    
    private Integer getMenuTypeId(MenusPanel panel) {
        MenuType menuType = (MenuType) getPersistence(panel.getCmbMenuType());
        return menuType != null ? menuType.getMenuTypeId() : null;
    }

    public TransferObject buildDeleteMenuRequest(Menu selectedMenu) {
        return createRequest(Operation.DELETE_MENU)
                .put(Menu.MENU_KEY, selectedMenu)
                .build();
    }

    public TransferObject buildFindAllForUpdateMenuRequest() {
        return createRequest(Operation.FIND_ALL_FOR_UPDATE_MENU)
                .build();
    }

}
