/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus.menu;

import builder.Localizer;
import information.InfoController;
import shared.Util;

/**
 *
 * @author maja
 */
public class MenuLocalizer extends Localizer {

    public static final String BREADCRUMB = "panel.menu.breadcrumb";
    public static final String VALIDATION_START_DATE_INVALID = "panel.validation.startDate.invalid";
    public static final String VALIDATION_FINISH_DATE_INVALID = "panel.validation.finishDate.invalid";
    public static final String VALIDATION_AMOUNT_IS_NUMBER = "panel.validation.amount.isNumber";
    public static final String VALIDATION_START_DATE_NOT_BEFORE_FINISH_DATE = "panel.menu.startDate.notBeforeFinishDate";
    public static final String VALIDATION_FINISH_DATE_NOT_AFTER_START_DATE = "panel.menu.finishDate.notAfterStartDate";
    public static final String MEAL_NOT_SELECTED = "panel.menu.mealNotSelected";
    public static final String MENU_UPDATE_SUCCESS_MESSAGE = "operation.menuUpdate.success";
    private static final String MENU = "panel.menu.menu";
    private static final String MENU_ID = "panel.menu.menuId";
    private static final String START_DATE = "panel.menu.startDate";
    private static final String FINISH_DATE = "panel.menu.finishDate";
    private static final String DATE_FORMAT = "panel.menu.dateFormat";
    private static final String MENU_TYPE = "panel.menu.menuType";
    private static final String MEDICAL_RECORD = "panel.menu.medicalRecord";
    private static final String MEAL = "panel.menu.meal";
    private static final String DISH = "panel.menu.dish";
    private static final String MEAL_TYPE = "panel.menu.mealType";
    private static final String MEAL_DAY = "panel.menu.mealDay";
    private static final String AMOUNT = "panel.menu.amount";
    private static final String AMOUNT_MEASUREMENT_UNIT = "panel.menu.amountMeasurementUnit";
    private static final String ADD = "panel.menu.add";
    private static final String MEALS = "panel.menu.meals";
    private static final String DELETE = "panel.menu.delete";
    private static final String SAVE = "panel.menu.save";

    private MealsTableModel mealsTableModel;
    private String[] mealsTableColumnNames;

    @Override
    public void localizationChanged() {
        MenuPanel panel = (MenuPanel) super.panel;
        initializeMealsTableColumnNames();
        localizeCmbMealType();
        localizeCmbMealDay();
        localizeLabelValidationWithMessageKey(panel.getLblStartDateValidation(), VALIDATION_START_DATE_INVALID);
        localizeLabelValidationWithMessageKey(panel.getLblFinishDateValidation(), VALIDATION_FINISH_DATE_INVALID);
        localizeLabelValidationWithMessageKey(panel.getLblAmountValidation(), VALIDATION_AMOUNT_IS_NUMBER);
        localizeLabelMessageKey(panel.getLblMenu(), MENU);
        localizeLabelMessageKey(panel.getLblMenuId(), MENU_ID);
        localizeLabelMessageKey(panel.getLblStartDate(), START_DATE);
        localizeLabelMessageKey(panel.getLblFinishDate(), FINISH_DATE);
        localizeLabelMessageKey(panel.getLblStartDateFormat(), DATE_FORMAT);
        localizeLabelMessageKey(panel.getLblFinishDateFormat(), DATE_FORMAT);
        localizeLabelMessageKey(panel.getLblMenuType(), MENU_TYPE);
        localizeButtonMessageKey(panel.getBtnMenuTypeInformation(), InfoController.I);
        localizeLabelMessageKey(panel.getLblMedicalRecord(), MEDICAL_RECORD);
        localizeButtonMessageKey(panel.getBtnMedicalRecordInformation(), InfoController.I);
        localizeLabelMessageKey(panel.getLblMeal(), MEAL);
        localizeLabelMessageKey(panel.getLblDish(), DISH);
        localizeButtonMessageKey(panel.getBtnDishInformation(), InfoController.I);
        localizeLabelMessageKey(panel.getLblMealType(), MEAL_TYPE);
        localizeLabelMessageKey(panel.getLblMealDay(), MEAL_DAY);
        localizeLabelMessageKey(panel.getLblAmount(), AMOUNT);
        localizeLabelMessageKey(panel.getLblAmountMeasurementUnit(), AMOUNT_MEASUREMENT_UNIT);
        localizeButtonMessageKey(panel.getBtnAdd(), ADD);
        localizeLabelMessageKey(panel.getLblMeals(), MEALS);
        localizeButtonMessageKey(panel.getBtnDelete(), DELETE);
        localizeButtonMessageKey(panel.getBtnSave(), SAVE);
    }
    
    public void setMealsTableModel(MealsTableModel mealsTableModel) {
        this.mealsTableModel = mealsTableModel;
    }

    private void initializeMealsTableColumnNames() {
        mealsTableColumnNames = new String[] {
            localizationLoader.getString(DISH),
            localizationLoader.getString(MEAL_TYPE),
            localizationLoader.getString(MEAL_DAY),
            localizationLoader.getString(AMOUNT)
        };
        if (mealsTableModel != null) {
            mealsTableModel.setColumnNames(mealsTableColumnNames);
        }
    }

    private void localizeCmbMealType() {
        MenuPanel panel = (MenuPanel) super.panel;
        Util.localizeCmb(panel.getCmbMealType());
    }

    private void localizeCmbMealDay() {
        MenuPanel panel = (MenuPanel) super.panel;
        Util.localizeCmb(panel.getCmbMealDay());
    }

    public String[] getMealsTableColumnNames() {
        return mealsTableColumnNames;
    }

    @Override
    public void initialize() {
        initializeMealsTableColumnNames();
        localizeCmbMealDay();
        localizeCmbMealType();
    }
    
}
