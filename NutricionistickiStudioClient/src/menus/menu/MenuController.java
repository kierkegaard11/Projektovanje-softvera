/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus.menu;

import builder.Controller;
import information.dish.DishInfoController;
import information.medicalRecord.MedicalRecordInfoController;
import information.menuType.MenuTypeInfoController;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.List;
import javax.swing.JTable;
import menus.MenusController;
import model.Dish;
import model.Meal;
import model.MealDay;
import model.MealType;
import model.MedicalRecord;
import model.Menu;
import model.MenuType;
import model.persistence.Persistence;
import shared.LocalizedCmbOption;
import shared.Util;

/**
 *
 * @author maja
 */
public class MenuController extends Controller {

    private Menu menu;
    private List<Persistence> initialMenuTypes;
    private List<Persistence> initialMedicalRecords;
    private List<Persistence> initialDishes;
    private MenusController menusController;

    public MenuController(List<Persistence> menuTypes, List<Persistence> medicalRecords, List<Persistence> dishes, Menu menu, MenusController menusController) {
        this.menu = menu;
        initialMenuTypes = menuTypes;
        initialMedicalRecords = medicalRecords;
        initialDishes = dishes;
        this.menusController = menusController;
    }

    @Override
    public String getBreadcrumbTextKey() {
        return MenuLocalizer.BREADCRUMB;
    }

    @Override
    protected void clearValidationMessages() {
        MenuPanel panel = (MenuPanel) super.panel;
        clearValidationMessage(panel.getLblStartDateValidation());
        clearValidationMessage(panel.getLblFinishDateValidation());
        clearValidationMessage(panel.getLblAmountValidation());
    }

    @Override
    protected void setupImages() {
    }

    @Override
    protected void abstractInitialize() {
        initializeCmbMealType();
        initializeCmbMealDay();
        initializeMenuTypes();
        initializeMedicalRecords();
        initializeDishes();
        initializeTableMeals();
        populateMenuPanel();
        addBtnMenuTypeInformationListener();
        addBtnDishInformationListener();
        addBtnMedicalRecordInformationListener();
        addBtnAddListener();
        addBtnDeleteListener();
        addBtnSaveListener();
    }

    private void initializeCmbMealType() {
        Util.initializeLocalizedCmb(((MenuPanel) panel).getCmbMealType(),
                MealType.BREAKFAST,
                MealType.SNACK,
                MealType.LUNCH,
                MealType.NIGHT_SNACK,
                MealType.DINNER);
    }

    private void initializeCmbMealDay() {
        Util.initializeLocalizedCmb(((MenuPanel) panel).getCmbMealDay(),
                MealDay.MONDAY,
                MealDay.TUESDAY,
                MealDay.WEDNESDAY,
                MealDay.THURSDAY,
                MealDay.FRIDAY,
                MealDay.SATURDAY,
                MealDay.SUNDAY);
    }

    private void initializeTableMeals() {
        MenuLocalizer localizer = (MenuLocalizer) super.localizer;
        String[] columnNames = localizer.getMealsTableColumnNames();
        MealsTableModel mealsTableModel = new MealsTableModel(columnNames);
        localizer.setMealsTableModel(mealsTableModel);
        JTable tableMeals = ((MenuPanel) panel).getTableMeals();
        tableMeals.getTableHeader().setReorderingAllowed(false);
        tableMeals.setModel(mealsTableModel);
    }

    private void initializeMenuTypes() {
        Util.initializeCmb(((MenuPanel) panel).getCmbMenuType(), initialMenuTypes);
    }

    private void initializeMedicalRecords() {
        Util.initializeCmb(((MenuPanel) panel).getCmbMedicalRecord(), initialMedicalRecords);
    }

    private void initializeDishes() {
        Util.initializeCmb(((MenuPanel) panel).getCmbDish(), initialDishes);
    }

    private void populateMenuPanel() {
        MenuPanel panel = (MenuPanel) super.panel;
        panel.getTxtMenuId().setText(Integer.toString(menu.getMenuId()));
        panel.getTxtStartDate().setText(Util.getDateString(menu.getDateFrom()));
        panel.getTxtFinishDate().setText(Util.getDateString(menu.getDateTo()));
        panel.getCmbMenuType().setSelectedItem(menu.getMenuType());
        panel.getCmbMedicalRecord().setSelectedItem(menu.getMedicalRecord());
        MealsTableModel mealsTableModel = (MealsTableModel) panel.getTableMeals().getModel();
        mealsTableModel.setMeals(menu.getMeals());
    }

    private void addBtnMenuTypeInformationListener() {
        MenuPanel panel = (MenuPanel) super.panel;
        panel.getBtnMenuTypeInformation().addActionListener((ActionEvent e) -> {
            MenuType selectedMenuType = (MenuType) panel.getCmbMenuType().getSelectedItem();
            new MenuTypeInfoController(selectedMenuType).showInfoDialog();
        });
    }

    private void addBtnDishInformationListener() {
        MenuPanel panel = (MenuPanel) super.panel;
        panel.getBtnDishInformation().addActionListener((ActionEvent e) -> {
            Dish selectedDish = (Dish) panel.getCmbDish().getSelectedItem();
            new DishInfoController(selectedDish).showInfoDialog();
        });
    }

    private void addBtnMedicalRecordInformationListener() {
        MenuPanel panel = (MenuPanel) super.panel;
        panel.getBtnMedicalRecordInformation().addActionListener((ActionEvent e) -> {
            MedicalRecord selectedMedicalRecord = (MedicalRecord) panel.getCmbMedicalRecord().getSelectedItem();
            new MedicalRecordInfoController(selectedMedicalRecord).showInfoDialog();
        });
    }

    private void addBtnAddListener() {
        ((MenuPanel) panel).getBtnAdd().addActionListener((ActionEvent ae) -> {
            addMealToTable();
        });
    }

    private void addMealToTable() {
        MenuPanel panel = (MenuPanel) super.panel;
        if (((MenuValidator) validator).validateAmount()) {
            Meal meal = getMealFromForm();
            ((MealsTableModel) panel.getTableMeals().getModel()).addMeal(meal);
        }
    }

    private Meal getMealFromForm() {
        MenuPanel panel = (MenuPanel) super.panel;
        Meal meal = new Meal();
        meal.setMealId(((MealsTableModel) panel.getTableMeals().getModel()).getRowCount() + 1);
        meal.setDish((Dish) panel.getCmbDish().getSelectedItem());
        meal.setMealDay(((LocalizedCmbOption) panel.getCmbMealDay().getSelectedItem()).getKey());
        meal.setMealType(((LocalizedCmbOption) panel.getCmbMealType().getSelectedItem()).getKey());
        meal.setgAmount(Integer.parseInt(panel.getTxtAmount().getText().trim()));
        meal.setMenuId(menu.getMenuId());
        return meal;
    }

    private void addBtnDeleteListener() {
        ((MenuPanel) panel).getBtnDelete().addActionListener((ActionEvent ae) -> {
            deleteMealFromTable();
        });
    }

    private void deleteMealFromTable() {
        JTable tableMeals = ((MenuPanel) panel).getTableMeals();
        int selectedIndex = tableMeals.getSelectedRow();
        if (selectedIndex != -1) {
            ((MealsTableModel) tableMeals.getModel()).removeMealAtIndex(selectedIndex);
        } else {
            Util.showMessageDialog(MenuLocalizer.MEAL_NOT_SELECTED);
        }
    }

    private void addBtnSaveListener() {
        MenuPanel panel = (MenuPanel) super.panel;
        panel.getBtnSave().addActionListener((ae) -> {
            if (validateForm()) {
                populateMenuFromForm();
                MenuRequestBuilder requestBuilder = (MenuRequestBuilder) super.requestBuilder;
                sendRequestAndHandleResponse(requestBuilder.buildUpdateMenuRequest());
            }
        });
    }

    private boolean validateForm() {
        MenuValidator validator = (MenuValidator) super.validator;
        boolean startDateValid = validator.validateStartDate();
        boolean finishDateValid = validator.validateFinishDate();
        boolean startDateBeforeFinishDate = validator.validateStartDateBeforeFinishDate();
        return startDateValid && finishDateValid && startDateBeforeFinishDate;
    }

    private void populateMenuFromForm() {
        MenuPanel panel = (MenuPanel) super.panel;
        Integer menuId = Integer.parseInt(panel.getTxtMenuId().getText().trim());
        Date startDate = Util.getDate(panel.getTxtStartDate().getText().trim());
        Date finishDate = Util.getDate(panel.getTxtFinishDate().getText().trim());
        List<Meal> meals = ((MealsTableModel) panel.getTableMeals().getModel()).getAllMeals();
        MedicalRecord medicalRecord = (MedicalRecord) panel.getCmbMedicalRecord().getSelectedItem();
        MenuType menuType = (MenuType) panel.getCmbMenuType().getSelectedItem();
        menu = new Menu(menuId, startDate, finishDate, meals, medicalRecord, menuType);
    }

    public Menu getMenu() {
        return menu;
    }

    public void updateMenusTable(Menu updatedMenu) {
        menusController.updateMenusTable(updatedMenu);
    }

}
