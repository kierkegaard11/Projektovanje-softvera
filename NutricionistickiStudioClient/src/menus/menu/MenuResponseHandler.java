/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus.menu;

import builder.ResponseHandler;
import model.Menu;
import shared.Util;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class MenuResponseHandler extends ResponseHandler {

    @Override
    public void handleResponse(TransferObject response) {
        switch (response.getOperation()) {
            case UPDATE_MENU_SUCCESS:
                handleUpdateMenuSuccess(response);
                break;
            case UPDATE_MENU_ERROR:
                showErrorMessage(response);
                break;
        }
    }

    private void handleUpdateMenuSuccess(TransferObject response) {
        Menu updatedMenu = (Menu) response.get(Menu.MENU_KEY);
        ((MenuController) controller).updateMenusTable(updatedMenu);
        Util.showMessageDialog(MenuLocalizer.MENU_UPDATE_SUCCESS_MESSAGE);
    }

}
