/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus.menu;

import builder.Controller;
import builder.ControllerBuilder;
import builder.Localizer;
import builder.RequestBuilder;
import builder.ResponseHandler;
import builder.Validator;
import java.util.List;
import javax.swing.JPanel;
import menus.MenusController;
import model.Menu;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class MenuControllerBuilder extends ControllerBuilder {

    private List<Persistence> menuTypes;
    private List<Persistence> medicalRecords;
    private List<Persistence> dishes;
    private Menu menu;
    private MenusController menusController;

    public MenuControllerBuilder(List<Persistence> menuTypes, List<Persistence> medicalRecords, List<Persistence> dishes, Menu menu, MenusController menusController) {
        this.menuTypes = menuTypes;
        this.medicalRecords = medicalRecords;
        this.dishes = dishes;
        this.menu = menu;
        this.menusController = menusController;
    }
    
    @Override
    protected JPanel createPanel() {
        return new MenuPanel();
    }

    @Override
    protected Localizer createLocalizer() {
        return new MenuLocalizer();
    }

    @Override
    protected Validator createValidator() {
        return new MenuValidator();
    }

    @Override
    protected ResponseHandler createResponseHandler() {
        return new MenuResponseHandler();
    }

    @Override
    protected RequestBuilder createRequestBuilder() {
        return new MenuRequestBuilder();
    }

    @Override
    protected Controller createController() {
        return new MenuController(menuTypes, medicalRecords, dishes, menu, menusController);
    }
    
}
