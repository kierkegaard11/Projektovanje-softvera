/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus.menu;

import builder.RequestBuilder;
import model.Menu;
import transfer.Operation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class MenuRequestBuilder extends RequestBuilder {

    public TransferObject buildUpdateMenuRequest() {
        return createRequest(Operation.UPDATE_MENU)
                .put(Menu.MENU_KEY, ((MenuController) controller).getMenu())
                .build();
    }
    
}
