/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus.menu;

import builder.Validator;
import java.util.Date;
import javax.swing.JLabel;
import shared.Util;

/**
 *
 * @author maja
 */
public class MenuValidator extends Validator {
    
    public boolean validateStartDate() {
        MenuPanel panel = (MenuPanel) super.panel;
        String text = panel.getTxtStartDate().getText().trim();
        JLabel lblValidation = panel.getLblStartDateValidation();
        String message = localizationLoader.getString(MenuLocalizer.VALIDATION_START_DATE_INVALID);
        return validateTextIsValidDate(text, lblValidation, message);
    }
    
    public boolean validateFinishDate() {
        MenuPanel panel = (MenuPanel) super.panel;
        String text = panel.getTxtFinishDate().getText().trim();
        JLabel lblValidation = panel.getLblFinishDateValidation();
        String message = localizationLoader.getString(MenuLocalizer.VALIDATION_FINISH_DATE_INVALID);
        return validateTextIsValidDate(text, lblValidation, message);
    }
    
    public boolean validateAmount() {
        MenuPanel panel = (MenuPanel) super.panel;
        String text = panel.getTxtAmount().getText().trim();
        JLabel lblValidation = panel.getLblAmountValidation();
        String message = localizationLoader.getString(MenuLocalizer.VALIDATION_AMOUNT_IS_NUMBER);
        return validateTextIsInteger(text, lblValidation, message);
    }

    public boolean validateStartDateBeforeFinishDate() {
        MenuPanel panel = (MenuPanel) super.panel;
        Date startDate = Util.getDate(panel.getTxtStartDate().getText().trim());
        Date finishDate = Util.getDate(panel.getTxtFinishDate().getText().trim());
        boolean valid = startDate.before(finishDate);
        if (valid) {
            clearLblValidation(panel.getLblStartDateValidation());
            clearLblValidation(panel.getLblFinishDateValidation());
        } else {
            fillLblValidation(panel.getLblStartDateValidation(), localizationLoader.getString(MenuLocalizer.VALIDATION_START_DATE_NOT_BEFORE_FINISH_DATE));
            fillLblValidation(panel.getLblFinishDateValidation(), localizationLoader.getString(MenuLocalizer.VALIDATION_FINISH_DATE_NOT_AFTER_START_DATE));
        }
        return valid;
    }
    
}
