/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus.menu;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import localization.LocalizationLoader;
import model.Meal;

/**
 *
 * @author maja
 */
public class MealsTableModel extends AbstractTableModel {

    private List<Meal> meals;
    private String[] columnNames;
    private LocalizationLoader localizationLoader;
    
    public MealsTableModel(String[] columnNames) {
        meals = new ArrayList<>();
        this.columnNames = columnNames;
        localizationLoader = LocalizationLoader.getInstance();
    }
    
    @Override
    public int getRowCount() {
        return meals.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        Meal meal = meals.get(row);
        switch (col) {
            case 0:
                return meal.getDish().getName();
            case 1:
                return localizationLoader.getString(meal.getMealType());
            case 2:
                return localizationLoader.getString(meal.getMealDay());
            case 3:
                return meal.getgAmount();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
        fireTableStructureChanged();
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
        fireTableDataChanged();
    }

    public void addMeal(Meal meal) {
        meals.add(meal);
        fireTableDataChanged();
    }

    public void removeMealAtIndex(int index) {
        meals.remove(index);
        refreshMealIds();
        fireTableDataChanged();
    }

    private void refreshMealIds() {
        for (int i = 0; i < meals.size(); i++) {
            meals.get(i).setMealId(i + 1);
        }
    }

    public List<Meal> getAllMeals() {
        return meals;
    }
}
