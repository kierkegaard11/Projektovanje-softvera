/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus;

import builder.Localizer;
import information.InfoController;

/**
 *
 * @author maja
 */
public class MenusLocalizer extends Localizer {

    public static final String BREADCRUMB = "panel.menus.breadcrumb";
    public static final String MENU_TYPE_NOT_SELECTED = "panel.menus.menuTypeNotSelected";
    public static final String MENU_NOT_SELECTED = "panel.menus.menuNotSelected";
    public static final String DELETE_MENU_QUESTION = "panel.menus.deleteMenuQuestion";
    public static final String MENU_CREATE_SUCCESS_MESSAGE = "operation.menuCreate.success";
    public static final String FIND_MENU_SUCCESS_MESSAGE = "operation.menuFind.success";
    public static final String FIND_MENU_ERROR_MESSAGE = "operation.menuFind.error";
    public static final String DELETE_MENU_SUCCESS_MESSAGE = "operation.menuDelete.success";
    private static final String SEARCH_RESULTS = "panel.menus.searchResults";
    private static final String NEW = "panel.menus.new";
    private static final String UPDATE = "panel.menus.update";
    private static final String DELETE = "panel.menus.delete";
    private static final String SEARCH_CRITERIA = "panel.menus.searchCriteria";
    private static final String MEDICAL_RECORD_ID = "panel.menus.medicalRecordId";
    private static final String FIRST_NAME = "panel.menus.firstName";
    private static final String LAST_NAME = "panel.menus.lastName";
    private static final String MENU_TYPE = "panel.menus.menuType";
    private static final String SEARCH = "panel.menus.search";

    private MenusTableModel menusTableModel;
    private String[] menusTableColumnNames;

    @Override
    public void localizationChanged() {
        MenusPanel panel = (MenusPanel) super.panel;
        initializeMenusTableColumnNames();
        localizeLabelMessageKey(panel.getLblSearchResults(), SEARCH_RESULTS);
        localizeButtonMessageKey(panel.getBtnNew(), NEW);
        localizeButtonMessageKey(panel.getBtnUpdate(), UPDATE);
        localizeButtonMessageKey(panel.getBtnDelete(), DELETE);
        localizeLabelMessageKey(panel.getLblSearchCriteria(), SEARCH_CRITERIA);
        localizeLabelMessageKey(panel.getLblMedicalRecordId(), MEDICAL_RECORD_ID);
        localizeLabelMessageKey(panel.getLblFirstName(), FIRST_NAME);
        localizeLabelMessageKey(panel.getLblLastName(), LAST_NAME);
        localizeLabelMessageKey(panel.getLblMenuType(), MENU_TYPE);
        localizeButtonMessageKey(panel.getBtnMenuTypeInformation(), InfoController.I);
        localizeButtonMessageKey(panel.getBtnSearch(), SEARCH);
    }

    public void setMenusTableModel(MenusTableModel menusTableModel) {
        this.menusTableModel = menusTableModel;
    }

    private void initializeMenusTableColumnNames() {
        menusTableColumnNames = new String[]{
            localizationLoader.getString(MEDICAL_RECORD_ID),
            localizationLoader.getString(MENU_TYPE),
            localizationLoader.getString(FIRST_NAME),
            localizationLoader.getString(LAST_NAME)
        };
        if (menusTableModel != null) {
            menusTableModel.setColumnNames(menusTableColumnNames);
        }
    }

    public String[] getMenusTableColumnNames() {
        return menusTableColumnNames;
    }

    @Override
    public void initialize() {
        initializeMenusTableColumnNames();
    }

}
