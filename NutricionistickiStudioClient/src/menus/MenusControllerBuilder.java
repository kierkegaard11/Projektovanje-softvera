/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus;

import builder.Controller;
import builder.ControllerBuilder;
import builder.Localizer;
import builder.RequestBuilder;
import builder.ResponseHandler;
import builder.Validator;
import java.util.List;
import javax.swing.JPanel;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class MenusControllerBuilder extends ControllerBuilder {

    private List<Persistence> menuTypes;

    public MenusControllerBuilder(List<Persistence> menuTypes) {
        this.menuTypes = menuTypes;
    }
    
    @Override
    protected JPanel createPanel() {
        return new MenusPanel();
    }

    @Override
    protected Localizer createLocalizer() {
        return new MenusLocalizer();
    }

    @Override
    protected Validator createValidator() {
        return new MenusValidator();
    }

    @Override
    protected ResponseHandler createResponseHandler() {
        return new MenusResponseHandler();
    }

    @Override
    protected RequestBuilder createRequestBuilder() {
        return new MenusRequestBuilder();
    }

    @Override
    protected Controller createController() {
        return new MenusController(menuTypes);
    }
    
}
