/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.MedicalRecord;
import model.Menu;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class MenusTableModel extends AbstractTableModel {

    private List<Persistence> menus;
    private String[] columnNames;

    public MenusTableModel(String[] columnNames) {
        menus = new ArrayList<>();
        this.columnNames = columnNames;
    }

    @Override
    public int getRowCount() {
        return menus.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        Menu menu = (Menu) menus.get(row);
        MedicalRecord medicalRecord = menu.getMedicalRecord();
        switch (col) {
            case 0:
                return medicalRecord.getMedicalRecordId();
            case 1:
                return menu.getMenuType().getName();
            case 2:
                return medicalRecord.getFirstName();
            case 3:
                return medicalRecord.getLastName();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
        fireTableStructureChanged();
    }

    public void setMenus(List<Persistence> menus) {
        this.menus = menus;
        fireTableDataChanged();
    }

    public Menu getMenuAtIndex(int index) {
        return (Menu) menus.get(index);
    }

    public void deleteMenu(Menu deletedMenu) {
        menus.remove(deletedMenu);
        fireTableDataChanged();
    }

    public void updateMenu(Menu updatedMenu) {
        for (int i = 0; i < menus.size(); i++) {
            if (menus.get(i).equals(updatedMenu)) {
                menus.set(i, updatedMenu);
                fireTableDataChanged();
                break;
            }
        }
    }

}
