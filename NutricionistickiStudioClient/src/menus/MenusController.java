/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus;

import builder.Controller;
import information.menuType.MenuTypeInfoController;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JTable;
import model.Menu;
import model.MenuType;
import model.persistence.Persistence;
import shared.ImageController;
import shared.Util;

/**
 *
 * @author maja
 */
public class MenusController extends Controller {

    private List<Persistence> initialMenuTypes;

    public MenusController(List<Persistence> menuTypes) {
        initialMenuTypes = menuTypes;
    }

    @Override
    public String getBreadcrumbTextKey() {
        return MenusLocalizer.BREADCRUMB;
    }

    @Override
    protected void clearValidationMessages() {
    }

    @Override
    protected void setupImages() {
        imageController.setupImage(((MenusPanel) panel).getLblMenusImage(), ImageController.MENUS_IMAGE, ImageController.MENUS_HOVER_IMAGE);
    }

    @Override
    protected void abstractInitialize() {
        initializeMenuTypes();
        initializeTableMenus();
        addBtnNewActionListener();
        addBtnMenuTypeInformationListener();
        addBtnSearchActionListener();
        addBtnDeleteListener();
        addBtnUpdateListener();
    }

    private void initializeTableMenus() {
        MenusLocalizer localizer = (MenusLocalizer) super.localizer;
        String[] columnNames = localizer.getMenusTableColumnNames();
        MenusTableModel menusTableModel = new MenusTableModel(columnNames);
        localizer.setMenusTableModel(menusTableModel);
        JTable tableMenus = ((MenusPanel) panel).getTableMenus();
        tableMenus.getTableHeader().setReorderingAllowed(false);
        tableMenus.setModel(menusTableModel);
    }

    private void initializeMenuTypes() {
        JComboBox cmbMenuType = ((MenusPanel) panel).getCmbMenuType();
        cmbMenuType.addItem(null);
        Util.initializeCmb(cmbMenuType, initialMenuTypes);
    }

    private void addBtnNewActionListener() {
        ((MenusPanel) panel).getBtnNew().addActionListener((ActionEvent ae) -> {
            MenusRequestBuilder requestBuilder = (MenusRequestBuilder) super.requestBuilder;
            sendRequestAndHandleResponse(requestBuilder.buildNewMenuRequest());
        });
    }

    private void addBtnMenuTypeInformationListener() {
        MenusPanel panel = (MenusPanel) super.panel;
        panel.getBtnMenuTypeInformation().addActionListener((ActionEvent e) -> {
            MenuType selectedMenuType = (MenuType) panel.getCmbMenuType().getSelectedItem();
            if (selectedMenuType != null) {
                new MenuTypeInfoController(selectedMenuType).showInfoDialog();
            } else {
                Util.showMessageDialog(MenusLocalizer.MENU_TYPE_NOT_SELECTED);
            }
        });
    }

    private void addBtnSearchActionListener() {
        ((MenusPanel) panel).getBtnSearch().addActionListener((e) -> {
            MenusRequestBuilder requestBuilder = (MenusRequestBuilder) super.requestBuilder;
            sendRequestAndHandleResponse(requestBuilder.buildFindMenusRequest());
        });
    }

    public void setMenus(List<Persistence> menus) {
        MenusPanel panel = (MenusPanel) super.panel;
        MenusTableModel tableModel = (MenusTableModel) panel.getTableMenus().getModel();
        tableModel.setMenus(menus);
    }

    private void addBtnDeleteListener() {
        ((MenusPanel) panel).getBtnDelete().addActionListener((ActionEvent ae) -> {
            deleteMenu();
        });
    }

    private void deleteMenu() {
        JTable tableMenus = ((MenusPanel) panel).getTableMenus();
        int selectedIndex = tableMenus.getSelectedRow();
        if (selectedIndex != -1) {
            int input = Util.showConfirmDialog(MenusLocalizer.DELETE_MENU_QUESTION);
            if (input == Util.CONFIRM_DIALOG_YES) {
                Menu selectedMenu = ((MenusTableModel) tableMenus.getModel()).getMenuAtIndex(selectedIndex);
                MenusRequestBuilder requestBuilder = (MenusRequestBuilder) super.requestBuilder;
                sendRequestAndHandleResponse(requestBuilder.buildDeleteMenuRequest(selectedMenu));
            }
        } else {
            Util.showMessageDialog(MenusLocalizer.MENU_NOT_SELECTED);
        }
    }

    private void addBtnUpdateListener() {
        MenusPanel panel = (MenusPanel) super.panel;
        panel.getBtnUpdate().addActionListener((ActionEvent ae) -> {
            goToMenuPanel();
        });
    }

    private void goToMenuPanel() {
        JTable menusTable = ((MenusPanel) panel).getTableMenus();
        int selectedIndex = menusTable.getSelectedRow();
        if (selectedIndex != -1) {
            MenusRequestBuilder requestBuilder = (MenusRequestBuilder) super.requestBuilder;
            sendRequestAndHandleResponse(requestBuilder.buildFindAllForUpdateMenuRequest());
        } else {
            Util.showMessageDialog(MenusLocalizer.MENU_NOT_SELECTED);
        }
    }

    public void deleteMenuFromTable(Menu deletedMenu) {
        MenusPanel panel = (MenusPanel) super.panel;
        MenusTableModel tableModel = (MenusTableModel) panel.getTableMenus().getModel();
        tableModel.deleteMenu(deletedMenu);
    }

    public void updateMenusTable(Menu updatedMenu) {
        MenusPanel panel = (MenusPanel) super.panel;
        MenusTableModel tableModel = (MenusTableModel) panel.getTableMenus().getModel();
        tableModel.updateMenu(updatedMenu);
    }
}
