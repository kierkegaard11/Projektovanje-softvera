/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus;

import builder.Controller;
import builder.ControllerBuilder;
import builder.ResponseHandler;
import java.util.List;
import javax.swing.JTable;
import menus.menu.MenuControllerBuilder;
import model.Dish;
import model.MedicalRecord;
import model.Menu;
import model.MenuType;
import model.persistence.Persistence;
import shared.Util;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class MenusResponseHandler extends ResponseHandler {
    
    @Override
    public void handleResponse(TransferObject response) {
        switch (response.getOperation()) {
            case MENU_CREATE_SUCCESS:
                menuCreateSuccess(response);
                break;
            case MENU_CREATE_ERROR:
                showErrorMessage(response);
                break;
            case FIND_MENU_SUCCESS:
                handleFindMenuSuccess(response);
                break;
            case FIND_MENU_ERROR:
                showErrorMessage(response);
                break;
            case DELETE_MENU_SUCCESS:
                handleDeleteMenuSuccess(response);
                break;
            case DELETE_MENU_ERROR:
                showErrorMessage(response);
                break;
            case FIND_ALL_FOR_UPDATE_MENU_SUCCESS:
                handleFindAllForUpdateMenuSuccess(response);
                break;
            case FIND_ALL_FOR_UPDATE_MENU_ERROR:
                showErrorMessage(response);
                break;
        }
    }

    private void menuCreateSuccess(TransferObject response) {
        List<Persistence> menuTypes = (List<Persistence>) response.get(MenuType.MENU_TYPE_KEY);
        List<Persistence> medicalRecords = (List<Persistence>) response.get(MedicalRecord.MEDICAL_RECORD_KEY);
        List<Persistence> dishes = (List<Persistence>) response.get(Dish.DISH_KEY);
        Menu menu = (Menu) response.get(Menu.MENU_KEY);
        ControllerBuilder menuControllerBuilder = new MenuControllerBuilder(menuTypes, medicalRecords, dishes, menu, (MenusController) controller);
        Controller menuController = menuControllerBuilder.build();
        controller.goToPanel(menuController);
        Util.showMessageDialog(MenusLocalizer.MENU_CREATE_SUCCESS_MESSAGE);
    }

    private void handleFindMenuSuccess(TransferObject response) {
        List<Persistence> menus = (List<Persistence>) response.get(Menu.MENU_KEY);
        if (menus.isEmpty()) {
            Util.showMessageDialog(MenusLocalizer.FIND_MENU_ERROR_MESSAGE);
        } else {
            ((MenusController) controller).setMenus(menus);
            Util.showMessageDialog(MenusLocalizer.FIND_MENU_SUCCESS_MESSAGE);
        }
    }

    private void handleDeleteMenuSuccess(TransferObject response) {
        Menu deletedMenu = (Menu) response.get(Menu.MENU_KEY);
        ((MenusController) controller).deleteMenuFromTable(deletedMenu);
        Util.showMessageDialog(MenusLocalizer.DELETE_MENU_SUCCESS_MESSAGE);
    }

    private void handleFindAllForUpdateMenuSuccess(TransferObject response) {
        List<Persistence> menuTypes = (List<Persistence>) response.get(MenuType.MENU_TYPE_KEY);
        List<Persistence> medicalRecords = (List<Persistence>) response.get(MedicalRecord.MEDICAL_RECORD_KEY);
        List<Persistence> dishes = (List<Persistence>) response.get(Dish.DISH_KEY);
        JTable tableDishes = ((MenusPanel) controller.getPanel()).getTableMenus();
        Menu selectedMenu = ((MenusTableModel) tableDishes.getModel()).getMenuAtIndex(tableDishes.getSelectedRow());
        ControllerBuilder menuControllerBuilder = new MenuControllerBuilder(menuTypes, medicalRecords, dishes, selectedMenu, (MenusController) controller);
        Controller menuController = menuControllerBuilder.build();
        controller.goToPanel(menuController);
    }
}
