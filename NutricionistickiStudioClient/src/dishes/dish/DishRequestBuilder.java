/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes.dish;

import builder.RequestBuilder;
import model.Dish;
import transfer.Operation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class DishRequestBuilder extends RequestBuilder {

    public TransferObject buildUpdateDishRequest() {
        return createRequest(Operation.UPDATE_DISH)
                .put(Dish.DISH_KEY, ((DishController) controller).getDish())
                .build();
    }

}
