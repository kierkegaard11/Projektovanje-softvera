/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes.dish;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import localization.LocalizationLoader;
import localization.LocalizationObserver;
import model.Dish;
import model.IngredientDishAggregate;
import shared.Util;

/**
 *
 * @author maja
 */
public class IngredientsTableModel extends AbstractTableModel implements LocalizationObserver {

    private List<IngredientDishAggregate> ingredients;
    private String[] columnNames;
    private LocalizationLoader localizationLoader;

    public IngredientsTableModel(String[] columnNames) {
        ingredients = new ArrayList<>();
        this.columnNames = columnNames;
        localizationLoader = LocalizationLoader.getInstance();
    }

    @Override
    public int getRowCount() {
        return ingredients.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        IngredientDishAggregate ingredient = ingredients.get(row);
        switch (col) {
            case 0:
                return ingredient.getIngredient().getName();
            case 1:
                return ingredient.getAmount();
            case 2:
                return localizationLoader.getString(ingredient.getMeasurementUnit());
            case 3:
                String measurementUnit = ingredient.getMeasurementUnit();
                Double amount = ingredient.getAmount();
                Double kcal = ingredient.getIngredient().getKcal();
                return Util.calculateKcal(measurementUnit, amount, kcal);
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
        fireTableStructureChanged();
    }

    public void setIngredients(List<IngredientDishAggregate> ingredients) {
        this.ingredients = ingredients;
        fireTableDataChanged();
    }

    public boolean ingredientExists(IngredientDishAggregate ingredientDishAggregate) {
        return ingredients.contains(ingredientDishAggregate);
    }

    public void addIngredientDishAggregate(IngredientDishAggregate ingredientDishAggregate, Dish dish) {
        ingredients.add(ingredientDishAggregate);
        fireTableDataChanged();
        calculateDishKcal(dish);
    }

    public void removeIngredientAtIndex(int index, Dish dish) {
        ingredients.remove(index);
        fireTableDataChanged();
        calculateDishKcal(dish);
    }

    private void calculateDishKcal(Dish dish) {
        double totalKcalSum = 0;
        for (int i = 0; i < ingredients.size(); i++) {
            totalKcalSum += (Double) getValueAt(i, 3);
        }
        dish.setKcal(Util.roundDouble(totalKcalSum));
    }

    public List<IngredientDishAggregate> getAllIngredients() {
        return ingredients;
    }

    @Override
    public void localizationChanged() {
        fireTableDataChanged();
    }
}
