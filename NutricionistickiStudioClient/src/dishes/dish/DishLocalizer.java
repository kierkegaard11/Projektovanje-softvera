/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes.dish;

import builder.Localizer;
import information.InfoController;
import shared.Util;

/**
 *
 * @author maja
 */
public class DishLocalizer extends Localizer {

    public static final String BREADCRUMB = "panel.dish.breadcrumb";
    public static final String VALIDATION_NAME_IS_EMPTY = "panel.dish.validation.name.isEmpty";
    public static final String VALIDATION_AMOUNT_IS_NUMBER = "panel.dish.validation.amount.isNumber";
    public static final String PREPARATION_GUIDE = "panel.dish.preparationGuide";
    public static final String INGREDIENT_INFO = "panel.dish.ingredientInfo";
    public static final String NAME = "panel.dish.name";
    public static final String KCAL = "panel.dish.kcal";
    public static final String SAVE = "panel.dish.save";
    public static final String INGREDIENT_ALREADY_ADDED = "panel.dish.ingredientAlreadyAdded";
    public static final String INGREDIENT_NOT_SELECTED = "panel.dish.ingredientNotSelected";
    public static final String DISH_UPDATE_SUCCESS_MESSAGE = "operation.dishUpdate.success";
    private static final String DISH = "panel.dish.dish";
    private static final String DISH_ID = "panel.dish.dishId";
    private static final String ADD_INGREDIENT = "panel.dish.addIngredient";
    private static final String INGREDIENT = "panel.dish.ingredient";
    private static final String MEASUREMENT_UNIT = "panel.dish.measurementUnit";
    private static final String AMOUNT = "panel.dish.amount";
    private static final String ADD = "panel.dish.add";
    private static final String INGREDIENTS = "panel.dish.ingredients";
    private static final String DELETE = "panel.dish.delete";
    
    private IngredientsTableModel ingredientsTableModel;
    private String[] ingredientsTableColumnNames;

    @Override
    public void localizationChanged() {
        DishPanel panel = (DishPanel) super.panel;
        initializeIngredientsTableColumnNames();
        localizeCmbMeasurementUnit();
        localizeLabelMessageKey(panel.getLblDish(), DISH);
        localizeLabelMessageKey(panel.getLblDishId(), DISH_ID);
        localizeLabelMessageKey(panel.getLblKcal(), KCAL);
        localizeLabelMessageKey(panel.getLblName(), NAME);
        localizeButtonMessageKey(panel.getBtnPreparationGuide(), PREPARATION_GUIDE);
        localizeLabelMessageKey(panel.getLblAddIngredient(), ADD_INGREDIENT);
        localizeLabelMessageKey(panel.getLblIngredient(), INGREDIENT);
        localizeButtonMessageKey(panel.getBtnIngredientInfo(), InfoController.I);
        localizeLabelMessageKey(panel.getLblMeasurementUnit(), MEASUREMENT_UNIT);
        localizeLabelMessageKey(panel.getLblAmount(), AMOUNT);
        localizeButtonMessageKey(panel.getBtnAdd(), ADD);
        localizeLabelMessageKey(panel.getLblIngredients(), INGREDIENTS);
        localizeButtonMessageKey(panel.getBtnDelete(), DELETE);
        localizeButtonMessageKey(panel.getBtnSave(), SAVE);
    }
    
    public void setIngredientsTableModel(IngredientsTableModel ingredientsTableModel) {
        this.ingredientsTableModel = ingredientsTableModel;
    }

    private void initializeIngredientsTableColumnNames() {
        ingredientsTableColumnNames = new String[] {
            localizationLoader.getString(NAME),
            localizationLoader.getString(AMOUNT),
            localizationLoader.getString(MEASUREMENT_UNIT),
            localizationLoader.getString(KCAL)
        };
        if (ingredientsTableModel != null) {
            ingredientsTableModel.setColumnNames(ingredientsTableColumnNames);
        }
    }
    
    private void localizeCmbMeasurementUnit() {
        Util.localizeCmb(((DishPanel) panel).getCmbMeasurementUnit());
    }

    public String[] getIngredientsTableColumnNames() {
        return ingredientsTableColumnNames;
    }

    @Override
    public void initialize() {
        initializeIngredientsTableColumnNames();
        localizeCmbMeasurementUnit();
    }
    
}
