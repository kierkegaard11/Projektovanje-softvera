/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes.dish;

import java.awt.event.ActionEvent;
import localization.LocalizationLoader;
import model.Dish;

/**
 *
 * @author maja
 */
public class PreparationGuideController {
    
    private Dish dish;
    private PreparationGuideDIalog preparationGuideDIalog;
    private LocalizationLoader localizationLoader;
    
    public PreparationGuideController(Dish dish) {
        this.dish = dish;
        this.localizationLoader = LocalizationLoader.getInstance();
        initializePreparationGuideDialog();
    }

    private void initializePreparationGuideDialog() {
        preparationGuideDIalog = new PreparationGuideDIalog(null, true);
        addBtnSavePreparationGuideListener();
    }
    
    private void addBtnSavePreparationGuideListener() {
        preparationGuideDIalog.getBtnSave().addActionListener((ActionEvent ae) -> {
            String preparationGuide = preparationGuideDIalog.getTxtArea().getText().trim();
            dish.setPreparationGuide(preparationGuide);
            preparationGuideDIalog.setVisible(false);
        });
    }

    public void showPreparationGuideDialog() {
        preparationGuideDIalog.setTitle(localizationLoader.getString(DishLocalizer.PREPARATION_GUIDE));
        preparationGuideDIalog.getLblPreparationGuide().setText(localizationLoader.getString(DishLocalizer.PREPARATION_GUIDE));
        preparationGuideDIalog.getBtnSave().setText(localizationLoader.getString(DishLocalizer.SAVE));
        preparationGuideDIalog.getTxtArea().setText(dish.getPreparationGuide());
        preparationGuideDIalog.setLocationRelativeTo(null);
        preparationGuideDIalog.setVisible(true);
    }
    
}
