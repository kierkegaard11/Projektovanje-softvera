/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes.dish;

import builder.Controller;
import builder.ControllerBuilder;
import builder.Localizer;
import builder.RequestBuilder;
import builder.ResponseHandler;
import builder.Validator;
import dishes.DishesController;
import java.util.List;
import javax.swing.JPanel;
import model.Dish;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class DishControllerBuilder extends ControllerBuilder {

    private List<Persistence> ingredients;
    private Dish dish;
    private DishesController dishesController;

    public DishControllerBuilder(List<Persistence> ingredients, Dish dish, DishesController dishesController) {
        this.ingredients = ingredients;
        this.dish = dish;
        this.dishesController = dishesController;
    }
    
    @Override
    protected JPanel createPanel() {
        return new DishPanel();
    }

    @Override
    protected Localizer createLocalizer() {
        return new DishLocalizer();
    }

    @Override
    protected Validator createValidator() {
        return new DishValidator();
    }

    @Override
    protected ResponseHandler createResponseHandler() {
        return new DishResponseHandler();
    }

    @Override
    protected RequestBuilder createRequestBuilder() {
        return new DishRequestBuilder();
    }

    @Override
    protected Controller createController() {
        return new DishController(ingredients, dish, dishesController);
    }
    
}
