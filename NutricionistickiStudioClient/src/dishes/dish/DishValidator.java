/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes.dish;

import builder.Validator;
import javax.swing.JLabel;

/**
 *
 * @author maja
 */
public class DishValidator extends Validator {
    
    public boolean validateName() {
        DishPanel panel = (DishPanel) super.panel;
        String text = panel.getTxtName().getText().trim();
        JLabel lblValidation = panel.getLblNameValidation();
        String message = localizationLoader.getString(DishLocalizer.VALIDATION_NAME_IS_EMPTY);
        return validateTextIsEmpty(text, lblValidation, message);
    }
    
    public boolean validateAmount() {
        DishPanel panel = (DishPanel) super.panel;
        String text = panel.getTxtAmount().getText().trim();
        JLabel lblValidation = panel.getLblAmountValidation();
        String message = localizationLoader.getString(DishLocalizer.VALIDATION_AMOUNT_IS_NUMBER);
        return validateTextIsDouble(text, lblValidation, message);
    }
    
}
