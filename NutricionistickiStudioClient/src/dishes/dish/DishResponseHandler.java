/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes.dish;

import builder.ResponseHandler;
import model.Dish;
import shared.Util;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class DishResponseHandler extends ResponseHandler {
    
    @Override
    public void handleResponse(TransferObject response) {
        switch (response.getOperation()) {
            case DISH_UPDATE_SUCCESS:
                handleDishUpdateSuccess(response);
                break;
            case DISH_UPDATE_ERROR:
                showErrorMessage(response);
                break;
        }
    }

    private void handleDishUpdateSuccess(TransferObject response) {
        Dish updatedDish = (Dish) response.get(Dish.DISH_KEY);
        ((DishController) controller).updateDishesTable(updatedDish);
        Util.showMessageDialog(DishLocalizer.DISH_UPDATE_SUCCESS_MESSAGE);
    }

}
