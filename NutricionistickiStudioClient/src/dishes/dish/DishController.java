/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes.dish;

import builder.Controller;
import dishes.DishesController;
import information.ingredient.IngredientInfoController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JTable;
import model.Dish;
import model.Ingredient;
import model.IngredientDishAggregate;
import model.MeasurementUnit;
import model.persistence.Persistence;
import shared.LocalizedCmbOption;
import shared.Util;

/**
 *
 * @author maja
 */
public class DishController extends Controller {

    private Dish dish;
    private PreparationGuideController preparationGuideController;
    private List<Persistence> initialIngredients;
    private DishesController dishesController;

    public DishController(List<Persistence> ingredients, Dish dish, DishesController dishesController) {
        this.dish = dish;
        this.preparationGuideController = new PreparationGuideController(dish);
        this.initialIngredients = ingredients;
        this.dishesController = dishesController;
    }

    @Override
    public String getBreadcrumbTextKey() {
        return DishLocalizer.BREADCRUMB;
    }

    @Override
    protected void clearValidationMessages() {
        DishPanel panel = (DishPanel) super.panel;
        clearValidationMessage(panel.getLblNameValidation());
        clearValidationMessage(panel.getLblAmountValidation());
    }

    @Override
    protected void setupImages() {
    }

    @Override
    protected void abstractInitialize() {
        initializeCmbMeasurementUnit();
        initializeIngredients();
        initializeTableIngredients();
        populateDishPanel();
        addBtnPreparationGuideListener();
        addBtnIngredientInfoListener();
        addBtnAddListener();
        addBtnDeleteListener();
        addBtnSaveListener();
    }

    private void populateDishPanel() {
        DishPanel panel = (DishPanel) super.panel;
        panel.getTxtxDishId().setText(Integer.toString(dish.getDishId()));
        panel.getTxtName().setText(dish.getName());
        panel.getTxtKcal().setText(Double.toString(Util.roundDouble(dish.getKcal())));
        IngredientsTableModel ingredientsTableModel = (IngredientsTableModel) panel.getTableIngredients().getModel();
        ingredientsTableModel.setIngredients(dish.getIngredients());
    }

    private void initializeCmbMeasurementUnit() {
        Util.initializeLocalizedCmb(((DishPanel) panel).getCmbMeasurementUnit(),
                MeasurementUnit.GRAM,
                MeasurementUnit.MILLILITER,
                MeasurementUnit.PIECE);
    }

    private void initializeTableIngredients() {
        DishLocalizer localizer = (DishLocalizer) super.localizer;
        String[] columnNames = localizer.getIngredientsTableColumnNames();
        IngredientsTableModel ingredientsTableModel = new IngredientsTableModel(columnNames);
        localizer.setIngredientsTableModel(ingredientsTableModel);
        JTable tableIngredients = ((DishPanel) panel).getTableIngredients();
        tableIngredients.getTableHeader().setReorderingAllowed(false);
        tableIngredients.setModel(ingredientsTableModel);
    }

    private void initializeIngredients() {
        Util.initializeCmb(((DishPanel) panel).getCmbIngredient(), initialIngredients);
    }

    private void addBtnPreparationGuideListener() {
        ((DishPanel) panel).getBtnPreparationGuide().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                preparationGuideController.showPreparationGuideDialog();
            }
        });
    }

    private void addBtnIngredientInfoListener() {
        DishPanel panel = (DishPanel) super.panel;
        panel.getBtnIngredientInfo().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Ingredient selectedIngredient = (Ingredient) panel.getCmbIngredient().getSelectedItem();
                new IngredientInfoController(selectedIngredient).showInfoDialog();
            }
        });
    }

    private void addBtnAddListener() {
        ((DishPanel) panel).getBtnAdd().addActionListener((ActionEvent ae) -> {
            addIngredientDishAggregateToTable();
        });
    }

    private void addIngredientDishAggregateToTable() {
        DishPanel panel = (DishPanel) super.panel;
        if (((DishValidator) validator).validateAmount()) {
            IngredientDishAggregate ingredientDishAggregate = getIngredientDishAggregateFromForm();
            if (!isIngredientAlreadyAdded(ingredientDishAggregate)) {
                IngredientsTableModel tableModel = (IngredientsTableModel) panel.getTableIngredients().getModel();
                tableModel.addIngredientDishAggregate(ingredientDishAggregate, dish);
                refreshDishKcal();
            } else {
                Util.showMessageDialog(DishLocalizer.INGREDIENT_ALREADY_ADDED);
            }
        }
    }

    private boolean isIngredientAlreadyAdded(IngredientDishAggregate ingredientDishAggregate) {
        DishPanel panel = (DishPanel) super.panel;
        IngredientsTableModel tableModel = (IngredientsTableModel) panel.getTableIngredients().getModel();
        return tableModel.ingredientExists(ingredientDishAggregate);
    }

    private IngredientDishAggregate getIngredientDishAggregateFromForm() {
        DishPanel panel = (DishPanel) super.panel;
        IngredientDishAggregate ingredientDishAggregate = new IngredientDishAggregate();
        ingredientDishAggregate.setIngredient((Ingredient) panel.getCmbIngredient().getSelectedItem());
        ingredientDishAggregate.setDish(dish);
        ingredientDishAggregate.setAmount(Double.parseDouble(panel.getTxtAmount().getText().trim()));
        LocalizedCmbOption measurementUnit = (LocalizedCmbOption) panel.getCmbMeasurementUnit().getSelectedItem();
        ingredientDishAggregate.setMeasurementUnit(measurementUnit.getKey());
        return ingredientDishAggregate;
    }

    private void refreshDishKcal() {
        ((DishPanel) panel).getTxtKcal().setText(Double.toString(dish.getKcal()));
    }

    private void addBtnDeleteListener() {
        ((DishPanel) panel).getBtnDelete().addActionListener((ActionEvent ae) -> {
            deleteIngredientFromTable();
        });
    }

    private void deleteIngredientFromTable() {
        JTable tableIngredients = ((DishPanel) panel).getTableIngredients();
        int selectedIndex = tableIngredients.getSelectedRow();
        if (selectedIndex != -1) {
            ((IngredientsTableModel) tableIngredients.getModel()).removeIngredientAtIndex(selectedIndex, dish);
            refreshDishKcal();
        } else {
            Util.showMessageDialog(DishLocalizer.INGREDIENT_NOT_SELECTED);
        }
    }

    private void addBtnSaveListener() {
        DishPanel panel = (DishPanel) super.panel;
        panel.getBtnSave().addActionListener((ae) -> {
            if (((DishValidator) validator).validateName()) {
                populateDishFromForm();
                DishRequestBuilder requestBuilder = (DishRequestBuilder) super.requestBuilder;
                sendRequestAndHandleResponse(requestBuilder.buildUpdateDishRequest());
            }
        });
    }

    private void populateDishFromForm() {
        DishPanel panel = (DishPanel) super.panel;
        Integer dishId = Integer.parseInt(panel.getTxtxDishId().getText().trim());
        Double kcal = Double.parseDouble(panel.getTxtKcal().getText().trim());
        String name = panel.getTxtName().getText().trim();
        IngredientsTableModel model = (IngredientsTableModel) panel.getTableIngredients().getModel();
        List<IngredientDishAggregate> ingredients = model.getAllIngredients();
        dish = new Dish(dishId, name, dish.getPreparationGuide(), kcal, ingredients);
    }

    public Dish getDish() {
        return dish;
    }

    public void updateDishesTable(Dish updatedDish) {
        dishesController.updateDishesTable(updatedDish);
    }

}
