/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes;

import builder.Controller;
import builder.ControllerBuilder;
import builder.ResponseHandler;
import dishes.dish.DishControllerBuilder;
import java.util.List;
import javax.swing.JTable;
import model.Dish;
import model.Ingredient;
import model.persistence.Persistence;
import shared.Util;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class DishesResponseHandler extends ResponseHandler {
    
    @Override
    public void handleResponse(TransferObject response) {
        switch (response.getOperation()) {
            case DISH_FOUND:
                handleDishFound(response);
                break;
            case DISH_NOT_FOUND:
                showErrorMessage(response);
                break;
            case DISH_CREATE_SUCCESS:
                handleDishCreateSuccess(response);
                break;
            case DISH_CREATE_ERROR:
                showErrorMessage(response);
                break;
            case FIND_INGREDIENT_SUCCESS:
                handleFindIngredientSuccess(response);
                break;
            case FIND_INGREDIENT_ERROR:
                showErrorMessage(response);
                break;
        }
    }

    private void handleDishFound(TransferObject response) {
        List<Persistence> dishes = (List<Persistence>) response.get(Dish.DISH_KEY);
        if (dishes.isEmpty()) {
            Util.showMessageDialog(DishesLocalizer.DISH_NOT_FOUND_MESSAGE);
        } else {
            ((DishesController) controller).setDishes(dishes);
            Util.showMessageDialog(DishesLocalizer.DISH_FOUND_MESSAGE);
        }
    }

    private void handleDishCreateSuccess(TransferObject response) {
        List<Persistence> ingredients = (List<Persistence>) response.get(Ingredient.INGREDIENT_KEY);
        Dish dish = (Dish) response.get(Dish.DISH_KEY);
        ControllerBuilder dishControllerBuilder = new DishControllerBuilder(ingredients, dish, (DishesController) controller);
        Controller dishController = dishControllerBuilder.build();
        controller.goToPanel(dishController);
        Util.showMessageDialog(DishesLocalizer.DISH_CREATE_SUCCESS_MESSAGE);
    }

    private void handleFindIngredientSuccess(TransferObject response) {
        List<Persistence> ingredients = (List<Persistence>) response.get(Ingredient.INGREDIENT_KEY);
        JTable tableDishes = ((DishesPanel) controller.getPanel()).getTableDishes();
        Dish selectedDish = ((DishesTableModel) tableDishes.getModel()).getDishAtIndex(tableDishes.getSelectedRow());
        ControllerBuilder dishControllerBuilder = new DishControllerBuilder(ingredients, selectedDish, (DishesController) controller);
        Controller medicalRecordController = dishControllerBuilder.build();
        controller.goToPanel(medicalRecordController);
    }

}
