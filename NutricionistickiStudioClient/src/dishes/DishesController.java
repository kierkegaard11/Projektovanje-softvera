/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes;

import builder.Controller;
import information.ingredient.IngredientInfoController;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JTable;
import model.Dish;
import model.Ingredient;
import model.persistence.Persistence;
import shared.ImageController;
import shared.Util;

/**
 *
 * @author maja
 */
public class DishesController extends Controller {

    private List<Persistence> initialIngredients;

    public DishesController(List<Persistence> ingredients) {
        this.initialIngredients = ingredients;
    }

    @Override
    public String getBreadcrumbTextKey() {
        return DishesLocalizer.BREADCRUMB;
    }

    @Override
    protected void clearValidationMessages() {
    }

    @Override
    protected void setupImages() {
        imageController.setupImage(((DishesPanel) panel).getLblDishesImage(), ImageController.DISHES_IMAGE, ImageController.DISHES_HOVER_IMAGE);
    }

    @Override
    protected void abstractInitialize() {
        initializeIngredients();
        initializeTableDishes();
        addBtnNewActionListener();
        addBtnIngredientInfoListener();
        addBtnSearchListener();
        addBtnUpdateListener();
    }

    private void initializeTableDishes() {
        DishesLocalizer localizer = (DishesLocalizer) super.localizer;
        String[] columnNames = localizer.getDishesTableColumnNames();
        DishesTableModel dishesTableModel = new DishesTableModel(columnNames);
        localizer.setDishesTableModel(dishesTableModel);
        JTable tableDishes = ((DishesPanel) panel).getTableDishes();
        tableDishes.getTableHeader().setReorderingAllowed(false);
        tableDishes.setModel(dishesTableModel);
    }

    private void initializeIngredients() {
        JComboBox cmbIngredient = ((DishesPanel) panel).getCmbIngredient();
        cmbIngredient.addItem(null);
        Util.initializeCmb(cmbIngredient, initialIngredients);
    }

    private void addBtnNewActionListener() {
        ((DishesPanel) panel).getBtnNew().addActionListener((ActionEvent ae) -> {
            DishesRequestBuilder requestBuilder = (DishesRequestBuilder) super.requestBuilder;
            sendRequestAndHandleResponse(requestBuilder.buildNewDishRequest());
        });
    }

    private void addBtnIngredientInfoListener() {
        DishesPanel panel = (DishesPanel) super.panel;
        panel.getBtnIngredientInfo().addActionListener((ActionEvent e) -> {
            Ingredient selectedIngredient = (Ingredient) panel.getCmbIngredient().getSelectedItem();
            if (selectedIngredient != null) {
                new IngredientInfoController(selectedIngredient).showInfoDialog();
            } else {
                Util.showMessageDialog(DishesLocalizer.INGREDIENT_NOT_SELECTED);
            }
        });
    }

    private void addBtnSearchListener() {
        ((DishesPanel) panel).getBtnSearch().addActionListener((ActionEvent e) -> {
            DishesRequestBuilder requestBuilder = (DishesRequestBuilder) super.requestBuilder;
            sendRequestAndHandleResponse(requestBuilder.buildFindDishRequest());
        });
    }

    public void setDishes(List<Persistence> dishes) {
        DishesPanel panel = (DishesPanel) super.panel;
        DishesTableModel tableModel = (DishesTableModel) panel.getTableDishes().getModel();
        tableModel.setDishes(dishes);
    }

    private void addBtnUpdateListener() {
        ((DishesPanel) panel).getBtnUpdate().addActionListener((ActionEvent ae) -> {
            goToDishPanel();
        });
    }

    private void goToDishPanel() {
        JTable dishesTable = ((DishesPanel) panel).getTableDishes();
        int selectedIndex = dishesTable.getSelectedRow();
        if (selectedIndex != -1) {
            DishesRequestBuilder requestBuilder = (DishesRequestBuilder) super.requestBuilder;
            sendRequestAndHandleResponse(requestBuilder.buildFindIngredientRequest());
        } else {
            Util.showMessageDialog(DishesLocalizer.DISH_NOT_SELECTED);
        }
    }

    public void updateDishesTable(Dish updatedDish) {
        DishesPanel panel = (DishesPanel) super.panel;
        DishesTableModel tableModel = (DishesTableModel) panel.getTableDishes().getModel();
        tableModel.updateDish(updatedDish);
    }
}
