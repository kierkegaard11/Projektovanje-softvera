/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes;

import builder.Localizer;
import information.InfoController;

/**
 *
 * @author maja
 */
public class DishesLocalizer extends Localizer {

    public static final String BREADCRUMB = "panel.dishes.breadcrumb";
    public static final String DISH_NOT_SELECTED = "panel.dishes.dishNotSelected";
    public static final String INGREDIENT_NOT_SELECTED = "panel.dishes.ingredientNotSelected";
    public static final String DISH_CREATE_SUCCESS_MESSAGE = "operation.dishCreate.success";
    public static final String DISH_FOUND_MESSAGE = "operation.dishFind.success";
    public static final String DISH_NOT_FOUND_MESSAGE = "operation.dishFind.error";
    private static final String SEARCH_CRITERIA = "panel.dishes.searchCriteria";
    private static final String NAME = "panel.dishes.name";
    private static final String INGREDIENT = "panel.dishes.ingredient";
    private static final String SEARCH = "panel.dishes.search";
    private static final String SEARCH_RESULTS = "panel.dishes.searchResults";
    private static final String KCAL = "panel.dishes.kcal";
    private static final String NEW = "panel.dishes.new";
    private static final String UPDATE = "panel.dishes.update";
    
    private DishesTableModel dishesTableModel;
    private String[] dishesTableColumnNames;

    @Override
    public void localizationChanged() {
        DishesPanel panel = (DishesPanel) super.panel;
        initializeDishesTableColumnNames();
        localizeLabelMessageKey(panel.getLblSearchCriteria(), SEARCH_CRITERIA);
        localizeLabelMessageKey(panel.getLblName(), NAME);
        localizeLabelMessageKey(panel.getLblIngredient(), INGREDIENT);
        localizeButtonMessageKey(panel.getBtnIngredientInfo(), InfoController.I);
        localizeButtonMessageKey(panel.getBtnSearch(), SEARCH);
        localizeLabelMessageKey(panel.getLblSearchResults(), SEARCH_RESULTS);
        localizeButtonMessageKey(panel.getBtnNew(), NEW);
        localizeButtonMessageKey(panel.getBtnUpdate(), UPDATE);
    }

    public void setDishesTableModel(DishesTableModel dishesTableModel) {
        this.dishesTableModel = dishesTableModel;
    }
    
    private void initializeDishesTableColumnNames() {
        dishesTableColumnNames = new String[] {
            localizationLoader.getString(NAME),
            localizationLoader.getString(KCAL)
        };
        if (dishesTableModel != null) {
            dishesTableModel.setColumnNames(dishesTableColumnNames);
        }
    }

    public String[] getDishesTableColumnNames() {
        return dishesTableColumnNames;
    }

    @Override
    public void initialize() {
        initializeDishesTableColumnNames();
    }
    
}
