/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.Dish;
import model.persistence.Persistence;
import shared.Util;

/**
 *
 * @author maja
 */
public class DishesTableModel extends AbstractTableModel {
    
    private List<Persistence> dishes;
    private String[] columnNames;
    
    public DishesTableModel(String[] columnNames) {
        dishes = new ArrayList<>();
        this.columnNames = columnNames;
    }
    
    @Override
    public int getRowCount() {
        return dishes.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        Dish dish = (Dish) dishes.get(row);
        switch (col) {
            case 0:
                return dish.getName();
            case 1:
                return Util.roundDouble(dish.getKcal());
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
        fireTableStructureChanged();
    }

    public void setDishes(List<Persistence> dishes) {
        this.dishes = dishes;
        fireTableDataChanged();
    }

    public Dish getDishAtIndex(int index) {
        return (Dish) dishes.get(index);
    }

    public void updateDish(Dish updatedDish) {
        for (int i = 0; i < dishes.size(); i++) {
            if (dishes.get(i).equals(updatedDish)) {
                dishes.set(i, updatedDish);
                fireTableDataChanged();
                break;
            }
        }
    }
    
}
