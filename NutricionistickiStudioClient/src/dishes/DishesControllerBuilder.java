/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes;

import builder.Controller;
import builder.ControllerBuilder;
import builder.Localizer;
import builder.RequestBuilder;
import builder.ResponseHandler;
import builder.Validator;
import java.util.List;
import javax.swing.JPanel;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class DishesControllerBuilder extends ControllerBuilder {

    private List<Persistence> ingredients;

    public DishesControllerBuilder(List<Persistence> ingredients) {
        this.ingredients = ingredients;
    }
    
    @Override
    protected JPanel createPanel() {
        return new DishesPanel();
    }

    @Override
    protected Localizer createLocalizer() {
        return new DishesLocalizer();
    }

    @Override
    protected Validator createValidator() {
        return new DishesValidator();
    }

    @Override
    protected ResponseHandler createResponseHandler() {
        return new DishesResponseHandler();
    }

    @Override
    protected RequestBuilder createRequestBuilder() {
        return new DishesRequestBuilder();
    }

    @Override
    protected Controller createController() {
        return new DishesController(ingredients);
    }
    
}
