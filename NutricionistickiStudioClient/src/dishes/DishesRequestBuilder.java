/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishes;

import builder.RequestBuilder;
import model.Dish;
import model.Ingredient;
import transfer.Operation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class DishesRequestBuilder extends RequestBuilder {
    
    public TransferObject buildFindDishRequest() {
        DishesPanel panel = (DishesPanel) controller.getPanel();
        return createRequest(Operation.FIND_DISH)
                .put(Dish.COL_NAME, getString(panel.getTxtName()))
                .put(Ingredient.INGREDIENT_KEY, getPersistence(panel.getCmbIngredient()))
                .build();
    }
    
    public TransferObject buildNewDishRequest() {
        return createRequest(Operation.NEW_DISH)
                .build();
    }

    public TransferObject buildFindIngredientRequest() {
        return createRequest(Operation.FIND_INGREDIENT)
                .build();
    }
    
}
