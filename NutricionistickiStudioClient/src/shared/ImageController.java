/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author maja
 */
public final class ImageController {
    
    private static ImageController instance;
    
    public static final String DISHES_IMAGE = "/images/dishes.png";
    public static final String DISHES_HOVER_IMAGE = "/images/dishesHovered.png";
    public static final String MENUS_IMAGE = "/images/menus.png";
    public static final String MENUS_HOVER_IMAGE = "/images/menusHovered.png";
    public static final String MEDICAL_RECORDS_IMAGE = "/images/medicalRecords.png";
    public static final String MEDICAL_RECORDS_HOVER_IMAGE = "/images/medicalRecordsHovered.png";
    
    private ImageController() {
    }

    public static ImageController getInstance() {
        if (instance == null) {
            instance = new ImageController();
        }
        return instance;
    }
    
    public void setupImage(JLabel lblImage, String imagePath, String imageHoveredPath) {
        setLblIcon(lblImage, imagePath);
        addImageMouseListener(lblImage, imagePath, imageHoveredPath);
    }
    
    private void addImageMouseListener(JLabel lblImage, String imagePath, String hoverImagePath) {
        lblImage.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                setLblIcon(lblImage, hoverImagePath);
            }
            @Override
            public void mouseExited(MouseEvent e) {
                setLblIcon(lblImage, imagePath);
            }
            @Override
            public void mouseClicked(MouseEvent e) {
                mouseExited(e);
            }
        });
    }    
    
    private void setLblIcon(JLabel lblImage, String imagePath) {
        lblImage.setIcon(new ImageIcon(getClass().getResource(imagePath)));
    }
}
