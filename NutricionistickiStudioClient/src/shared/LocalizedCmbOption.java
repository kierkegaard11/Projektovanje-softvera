/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared;

import java.util.Objects;
import localization.LocalizationLoader;

/**
 *
 * @author maja
 */
public class LocalizedCmbOption {

    protected String key;
    protected String value;
    private LocalizationLoader localizationLoader;

    public LocalizedCmbOption(String key) {
        this.key = key;
        localizationLoader = LocalizationLoader.getInstance();
        localize();
    }

    public void localize() {
        value = localizationLoader.getString(key);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.key);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LocalizedCmbOption other = (LocalizedCmbOption) obj;
        if (!Objects.equals(this.key, other.key)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getKey() {
        return key;
    }

}
