/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import localization.LocalizationLoader;
import model.MeasurementUnit;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public final class Util {

    public static final String DATE_FORMAT = "dd.MM.yyyy";
    public static final String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
    public static final int CONFIRM_DIALOG_YES = 0;
    private static final String TITLE = "message.dialog.title";

    public static String getDateString(Date date) {
        return new SimpleDateFormat(DATE_FORMAT).format(date);
    }

    public static Date getDate(String dateString) {
        try {
            return new SimpleDateFormat(DATE_FORMAT).parse(dateString);
        } catch (ParseException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static boolean isDateValid(String dateString) {
        DateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        sdf.setLenient(false);
        try {
            sdf.parse(dateString);
        } catch (ParseException ex) {
            return false;
        }
        return true;
    }

    public static boolean isEmailValid(String email) {
        return email.matches(EMAIL_REGEX);
    }

    public static void initializeCmb(JComboBox cmb, List<Persistence> list) {
        for (Persistence persistence : list) {
            cmb.addItem(persistence);
        }
    }

    public static void initializeLocalizedCmb(JComboBox localizedCmb, String... localizedKeys) {
        localizedCmb.removeAllItems();
        for (String key : localizedKeys) {
            if (key != null) {
                localizedCmb.addItem(new LocalizedCmbOption(key));
            } else {
                localizedCmb.addItem(null);
            }
        }
    }

    public static void localizeCmb(JComboBox localizedCmb) {
        for (int i = 0; i < localizedCmb.getItemCount(); i++) {
            LocalizedCmbOption localizedCmbOption = (LocalizedCmbOption) localizedCmb.getItemAt(i);
            if (localizedCmbOption != null) {
                ((LocalizedCmbOption) localizedCmb.getItemAt(i)).localize();
            }
        }
    }

    public static void showMessageDialog(String messageKey) {
        LocalizationLoader localizationLoader = LocalizationLoader.getInstance();
        String message = localizationLoader.getString(messageKey);
        String title = localizationLoader.getString(TITLE);
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static int showConfirmDialog(String messageKey) {
        LocalizationLoader localizationLoader = LocalizationLoader.getInstance();
        String message = localizationLoader.getString(messageKey);
        String title = localizationLoader.getString(TITLE);
        return JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
    }

    public static Double calculateKcal(String measurementUnit, Double amount, Double kcal) {
        Double calculatedKcal = 0.0;
        switch (measurementUnit) {
            case MeasurementUnit.GRAM:
            case MeasurementUnit.MILLILITER:
                calculatedKcal = kcal * amount / 100;
                break;
            case MeasurementUnit.PIECE:
                calculatedKcal = kcal * amount;
                break;
        }
        return roundDouble(calculatedKcal);
    }

    public static Double roundDouble(Double value) {
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
