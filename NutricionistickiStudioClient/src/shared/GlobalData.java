/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author maja
 */
public final class GlobalData {
    
    public static final String USER = "user";
    
    private static GlobalData instance;
    private Map<String, Object> data;
    
    private GlobalData() {
        data = new HashMap<>();
    }

    public static GlobalData getInstance() {
        if (instance == null) {
            instance = new GlobalData();
        }
        return instance;
    }
    
    public Object get(String key) {
        return data.get(key);
    }

    public void put(String key, Object value) {
        data.put(key, value);
    }
    
}
