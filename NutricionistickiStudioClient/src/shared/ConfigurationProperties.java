/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared;

import properties.GenericProperties;

/**
 *
 * @author maja
 */
public class ConfigurationProperties extends GenericProperties {

    public static final String SERVER_HOST = "server.host";
    public static final String SERVER_PORT = "server.port";
    
    private static final String CONFIGURATION_PROPERTIES_PATH = "./src/configuration/configuration.properties";
    private static ConfigurationProperties instance;
    
    private ConfigurationProperties() {
        super();
    }
    
    public static ConfigurationProperties getInstance() {
        if (instance == null) {
            instance = new ConfigurationProperties();
        }
        return instance;
    }
    
    @Override
    protected String getFilePath() {
        return CONFIGURATION_PROPERTIES_PATH;
    }
    
}
