/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package information.medicalRecord;

import information.InfoController;
import information.InfoDialog;
import model.MedicalRecord;
import model.persistence.Persistence;
import shared.Util;

/**
 *
 * @author maja
 */
public class MedicalRecordInfoController extends InfoController {

    private static final String MEDICAL_RECORD_INFORMATION = "dialog.medicalRecordInfo.medicalRecordInformation";
    private static final String FIRST_NAME = "dialog.medicalRecordInfo.firstName";
    private static final String LAST_NAME = "dialog.medicalRecordInfo.lastName";
    private static final String DATE_OF_BIRTH = "dialog.medicalRecordInfo.dateOfBirth";
    private static final String GENDER = "dialog.medicalRecordInfo.gender";
    private static final String MOBILE_PHONE = "dialog.medicalRecordInfo.mobilePhone";
    private static final String EMAIL = "dialog.medicalRecordInfo.email";
    private static final String NOTES = "dialog.medicalRecordInfo.notes";
    
    public MedicalRecordInfoController(Persistence data) {
        super(data);
    }

    @Override
    protected InfoDialog getInfoDialog() {
        return new MedicalRecordInfoDialog();
    }

    @Override
    protected String getTitleKey() {
        return MEDICAL_RECORD_INFORMATION;
    }

    @Override
    protected void populateComponents() {
        MedicalRecordInfoDialog infoDialog = (MedicalRecordInfoDialog) super.infoDialog;
        MedicalRecord data = (MedicalRecord) super.data;
        
        infoDialog.getTxtFirstName().setText(data.getFirstName());
        infoDialog.getTxtLastName().setText(data.getLastName());
        infoDialog.getTxtDateOfBirth().setText(Util.getDateString(data.getDateOfBirth()));
        infoDialog.getTxtGender().setText(localizationLoader.getString(data.getGender()));
        infoDialog.getTxtMobilePhone().setText(data.getMobilePhone());
        infoDialog.getTxtEmail().setText(data.getEmail());
        infoDialog.getTxtNotes().setText(data.getNote());
    }

    @Override
    protected void localizeComponents() {
        MedicalRecordInfoDialog infoDialog = (MedicalRecordInfoDialog) super.infoDialog;
        
        localizeLabel(infoDialog.getLblMedicalRecordInformation(), MEDICAL_RECORD_INFORMATION);
        localizeLabel(infoDialog.getLblFirstName(), FIRST_NAME);
        localizeLabel(infoDialog.getLblLastName(), LAST_NAME);
        localizeLabel(infoDialog.getLblDateOfBirth(), DATE_OF_BIRTH);
        localizeLabel(infoDialog.getLblGender(), GENDER);
        localizeLabel(infoDialog.getLblMobilePhone(), MOBILE_PHONE);
        localizeLabel(infoDialog.getLblEmail(), EMAIL);
        localizeLabel(infoDialog.getLblNotes(), NOTES);
    }
    
}
