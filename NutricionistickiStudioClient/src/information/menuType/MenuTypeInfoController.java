/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package information.menuType;

import information.InfoController;
import information.InfoDialog;
import model.MenuType;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class MenuTypeInfoController extends InfoController {
    
    private static final String MENU_TYPE_INFORMATION = "dialog.menuType.menuTypeInformation";
    private static final String NAME = "dialog.menuType.name";
    private static final String DESCRIPTION = "dialog.menuType.description";

    public MenuTypeInfoController(Persistence data) {
        super(data);
    }

    @Override
    protected InfoDialog getInfoDialog() {
        return new MenuTypeInfoDialog();
    }

    @Override
    protected String getTitleKey() {
        return MENU_TYPE_INFORMATION;
    }

    @Override
    protected void populateComponents() {
        MenuTypeInfoDialog infoDialog = (MenuTypeInfoDialog) super.infoDialog;
        MenuType data = (MenuType) super.data;
        
        infoDialog.getTxtName().setText(data.getName());
        infoDialog.getTxtDescription().setText(data.getDescription());
    }

    @Override
    protected void localizeComponents() {
        MenuTypeInfoDialog infoDialog = (MenuTypeInfoDialog) super.infoDialog;
        
        localizeLabel(infoDialog.getLblMenuTypeInformation(), MENU_TYPE_INFORMATION);
        localizeLabel(infoDialog.getLblName(), NAME);
        localizeLabel(infoDialog.getLblDescription(), DESCRIPTION);
    }
    
    
}
