/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package information;

import java.awt.Frame;
import javax.swing.JButton;
import javax.swing.JDialog;

/**
 *
 * @author maja
 */
public abstract class InfoDialog extends JDialog {
    
    public InfoDialog() {
        super(new Frame(), true);
    }
    
    protected abstract JButton getBtnOk();
}
