/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package information;

import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import localization.LocalizationLoader;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public abstract class InfoController {
    
    public static final String I = "dialog.info.i";
    public static final String OK = "dialog.info.ok";
    
    protected Persistence data;
    protected InfoDialog infoDialog;
    protected LocalizationLoader localizationLoader;

    public InfoController(Persistence data) {
        this.data = data;
        infoDialog = getInfoDialog();
        localizationLoader = LocalizationLoader.getInstance();
    }
    
    public void showInfoDialog() {
        infoDialog.setTitle(localizationLoader.getString(getTitleKey()));
        localizeButton(infoDialog.getBtnOk(), OK);
        populateComponents();
        localizeComponents();
        addBtnOkListener();
        infoDialog.setLocationRelativeTo(null);
        infoDialog.setVisible(true);
    }
    
    protected void localizeLabel(JLabel label, String messageKey) {
        label.setText(localizationLoader.getString(messageKey));
    }
    
    protected void localizeButton(JButton button, String messageKey) {
        button.setText(localizationLoader.getString(messageKey));
    }
    
    private void addBtnOkListener() {
        infoDialog.getBtnOk().addActionListener((ActionEvent e) -> {
            infoDialog.dispose();
        });
    }
    
    protected abstract InfoDialog getInfoDialog();
    
    protected abstract String getTitleKey();
    
    protected abstract void populateComponents();
    
    protected abstract void localizeComponents();
    
}
