/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package information.dish;

import information.InfoController;
import information.InfoDialog;
import information.ingredientDishAggregate.IngredientDishAggregateInfoController;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import model.Dish;
import model.IngredientDishAggregate;
import model.persistence.Persistence;
import shared.Util;

/**
 *
 * @author maja
 */
public class DishInfoController extends InfoController {

    private static final String DISH_INFORMATION = "dialog.dishInfo.dishInformation";
    private static final String NAME = "dialog.dishInfo.name";
    private static final String INGREDIENTS = "dialog.dishInfo.ingredients";
    private static final String PREPARATION_GUIDE = "dialog.dishInfo.preparationGuide";

    public DishInfoController(Persistence data) {
        super(data);
        addBtnIngredientInfoListener();
    }
    
    @Override
    protected InfoDialog getInfoDialog() {
        return new DishInfoDialog();
    }

    @Override
    protected String getTitleKey() {
        return DISH_INFORMATION;
    }

    @Override
    protected void populateComponents() {
        DishInfoDialog infoDialog = (DishInfoDialog) super.infoDialog;
        Dish data = (Dish) super.data;
        
        infoDialog.getTxtName().setText(data.getName());
        infoDialog.getTxtKcal().setText(Double.toString(Util.roundDouble(data.getKcal())));
        infoDialog.getCmbIngredientDishAggregate().setModel(new DefaultComboBoxModel(data.getIngredients().toArray()));
        infoDialog.getTxtPreparationGuide().setText(data.getPreparationGuide());
    }

    @Override
    protected void localizeComponents() {
        DishInfoDialog infoDialog = (DishInfoDialog) super.infoDialog;
        
        localizeLabel(infoDialog.getLblDishInformation(), DISH_INFORMATION);
        localizeLabel(infoDialog.getLblName(), NAME);
        localizeLabel(infoDialog.getLblIngredientDishAggregates(), INGREDIENTS);
        localizeLabel(infoDialog.getLblPreparationGuide(), PREPARATION_GUIDE);
        localizeButton(infoDialog.getBtnIngredientInfo(), InfoController.I);
    }

    private void addBtnIngredientInfoListener() {
        DishInfoDialog dishInfoDialog = (DishInfoDialog) infoDialog;
        dishInfoDialog.getBtnIngredientInfo().addActionListener((ActionEvent e) -> {
            IngredientDishAggregate selectedIngredientDishAggregate = (IngredientDishAggregate) dishInfoDialog.getCmbIngredientDishAggregate().getSelectedItem();
            new IngredientDishAggregateInfoController(selectedIngredientDishAggregate).showInfoDialog();
        });
    }
    
}
