/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package information.dish;

import information.InfoDialog;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import model.IngredientDishAggregate;

/**
 *
 * @author maja
 */
public class DishInfoDialog extends InfoDialog {

    /**
     * Creates new form DishInfoDialog
     */
    public DishInfoDialog() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelBackground = new javax.swing.JPanel();
        lblDishInformation = new javax.swing.JLabel();
        lblPreparationGuide = new javax.swing.JLabel();
        txtKcal = new javax.swing.JTextField();
        lblName = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        lblKcal = new javax.swing.JLabel();
        cmbIngredientDishAggregate = new javax.swing.JComboBox<>();
        btnIngredientInfo = new shared.Button();
        lblIngredientDishAggregates = new javax.swing.JLabel();
        scrollPaneDescription = new javax.swing.JScrollPane();
        txtPreparationGuide = new javax.swing.JTextArea();
        btnOk = new shared.Button();
        lblBackgroundImage = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        panelBackground.setLayout(null);

        lblDishInformation.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblDishInformation.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDishInformation.setText("Dish information");
        panelBackground.add(lblDishInformation);
        lblDishInformation.setBounds(0, 0, 540, 40);

        lblPreparationGuide.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblPreparationGuide.setText("Preparation guide");
        panelBackground.add(lblPreparationGuide);
        lblPreparationGuide.setBounds(20, 210, 220, 40);

        txtKcal.setEditable(false);
        txtKcal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        panelBackground.add(txtKcal);
        txtKcal.setBounds(250, 110, 280, 40);

        lblName.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblName.setText("Name");
        panelBackground.add(lblName);
        lblName.setBounds(20, 60, 220, 40);

        txtName.setEditable(false);
        txtName.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        panelBackground.add(txtName);
        txtName.setBounds(250, 60, 280, 40);

        lblKcal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblKcal.setText("Kcal");
        panelBackground.add(lblKcal);
        lblKcal.setBounds(20, 110, 220, 40);

        cmbIngredientDishAggregate.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        panelBackground.add(cmbIngredientDishAggregate);
        cmbIngredientDishAggregate.setBounds(250, 160, 230, 40);
        panelBackground.add(btnIngredientInfo);
        btnIngredientInfo.setBounds(490, 160, 40, 40);

        lblIngredientDishAggregates.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblIngredientDishAggregates.setText("Ingredients");
        panelBackground.add(lblIngredientDishAggregates);
        lblIngredientDishAggregates.setBounds(20, 160, 220, 40);

        txtPreparationGuide.setEditable(false);
        txtPreparationGuide.setColumns(20);
        txtPreparationGuide.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        txtPreparationGuide.setRows(5);
        scrollPaneDescription.setViewportView(txtPreparationGuide);

        panelBackground.add(scrollPaneDescription);
        scrollPaneDescription.setBounds(20, 260, 510, 270);
        panelBackground.add(btnOk);
        btnOk.setBounds(20, 540, 130, 50);

        lblBackgroundImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/background.jpg"))); // NOI18N
        panelBackground.add(lblBackgroundImage);
        lblBackgroundImage.setBounds(0, 0, 540, 600);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBackground, javax.swing.GroupLayout.DEFAULT_SIZE, 540, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBackground, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private shared.Button btnIngredientInfo;
    private shared.Button btnOk;
    private javax.swing.JComboBox<model.IngredientDishAggregate> cmbIngredientDishAggregate;
    private javax.swing.JLabel lblBackgroundImage;
    private javax.swing.JLabel lblDishInformation;
    private javax.swing.JLabel lblIngredientDishAggregates;
    private javax.swing.JLabel lblKcal;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPreparationGuide;
    private javax.swing.JPanel panelBackground;
    private javax.swing.JScrollPane scrollPaneDescription;
    private javax.swing.JTextField txtKcal;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextArea txtPreparationGuide;
    // End of variables declaration//GEN-END:variables

    @Override
    protected JButton getBtnOk() {
        return btnOk.getButton();
    }

    public JButton getBtnIngredientInfo() {
        return btnIngredientInfo.getButton();
    }

    public JComboBox<IngredientDishAggregate> getCmbIngredientDishAggregate() {
        return cmbIngredientDishAggregate;
    }

    public JLabel getLblPreparationGuide() {
        return lblPreparationGuide;
    }

    public JLabel getLblDishInformation() {
        return lblDishInformation;
    }

    public JLabel getLblIngredientDishAggregates() {
        return lblIngredientDishAggregates;
    }

    public JLabel getLblKcal() {
        return lblKcal;
    }

    public JLabel getLblName() {
        return lblName;
    }

    public JTextArea getTxtPreparationGuide() {
        return txtPreparationGuide;
    }

    public JTextField getTxtKcal() {
        return txtKcal;
    }

    public JTextField getTxtName() {
        return txtName;
    }
    
}
