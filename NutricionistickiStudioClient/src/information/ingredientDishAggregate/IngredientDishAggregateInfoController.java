/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package information.ingredientDishAggregate;

import information.InfoController;
import information.InfoDialog;
import information.ingredient.IngredientInfoController;
import java.awt.event.ActionEvent;
import model.IngredientDishAggregate;
import model.persistence.Persistence;
import shared.Util;

/**
 *
 * @author maja
 */
public class IngredientDishAggregateInfoController extends InfoController {

    private static final String INGREDIENT_INFORMATION = "dialog.ingredientDishAggregateInfo.ingredientInformation";
    private static final String AMOUNT = "dialog.ingredientDishAggregateInfo.amount";
    private static final String INGREDIENT = "dialog.ingredientDishAggregateInfo.ingredient";
    private static final String MEASUREMENT_UNIT = "dialog.ingredientDishAggregateInfo.measurementUnit";

    public IngredientDishAggregateInfoController(Persistence data) {
        super(data);
        addBtnIngredientInfoListener();
    }

    @Override
    protected InfoDialog getInfoDialog() {
        return new IngredientDishAggregateInfoDialog();
    }

    @Override
    protected String getTitleKey() {
        return INGREDIENT_INFORMATION;
    }

    @Override
    protected void populateComponents() {
        IngredientDishAggregateInfoDialog infoDialog = (IngredientDishAggregateInfoDialog) super.infoDialog;
        IngredientDishAggregate data = (IngredientDishAggregate) super.data;

        infoDialog.getTxtIngredient().setText(data.getIngredient().getName());
        infoDialog.getTxtAmount().setText(Double.toString(Util.roundDouble(data.getAmount())));
        infoDialog.getTxtMeasurementUnit().setText(localizationLoader.getString(data.getMeasurementUnit()));
    }

    @Override
    protected void localizeComponents() {
        IngredientDishAggregateInfoDialog infoDialog = (IngredientDishAggregateInfoDialog) super.infoDialog;

        localizeLabel(infoDialog.getLblIngredientInformation(), INGREDIENT_INFORMATION);
        localizeLabel(infoDialog.getLblIngredient(), INGREDIENT);
        localizeLabel(infoDialog.getLblAmount(), AMOUNT);
        localizeLabel(infoDialog.getLblMeasurementUnit(), MEASUREMENT_UNIT);
        localizeButton(infoDialog.getBtnIngredientInfo(), InfoController.I);
    }

    private void addBtnIngredientInfoListener() {
        IngredientDishAggregateInfoDialog infoDialog = (IngredientDishAggregateInfoDialog) super.infoDialog;
        infoDialog.getBtnIngredientInfo().addActionListener((ActionEvent e) -> {
            IngredientDishAggregate data = (IngredientDishAggregate) super.data;
            new IngredientInfoController(data.getIngredient()).showInfoDialog();
        });
    }

}
