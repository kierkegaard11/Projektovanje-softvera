/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package information.ingredient;

import information.InfoController;
import information.InfoDialog;
import model.Ingredient;
import model.persistence.Persistence;
import shared.Util;

/**
 *
 * @author maja
 */
public final class IngredientInfoController extends InfoController {   

    private static final String INGREDIENT_INFORMATION = "dialog.ingredientInfo.ingredientInformation";
    private static final String NAME = "dialog.ingredientInfo.name";
    private static final String CARBS = "dialog.ingredientInfo.carbs";
    private static final String PROTEINS = "dialog.ingredientInfo.proteins";
    private static final String FAT = "dialog.ingredientInfo.fat";
    private static final String GLICEMY_INDEX = "dialog.ingredientInfo.glicemyIndex";
    private static final String KCAL = "dialog.ingredientInfo.kcal";
    private static final String DESCRIPTION = "dialog.ingredientInfo.description";
    private static final String GRAM = "dialog.ingredientInfo.gram";
    private static final String OK = "dialog.ingredientInfo.ok";

    public IngredientInfoController(Persistence data) {
        super(data);
    }

    @Override
    protected InfoDialog getInfoDialog() {
        return new IngredientInfoDialog();
    }

    @Override
    protected String getTitleKey() {
        return INGREDIENT_INFORMATION;
    }

    @Override
    protected void populateComponents() {
        IngredientInfoDialog infoDialog = (IngredientInfoDialog) super.infoDialog;
        Ingredient data = (Ingredient) super.data;
        
        infoDialog.getTxtName().setText(data.getName());
        infoDialog.getTxtCarbs().setText(Double.toString(Util.roundDouble(data.getgCarbs())));
        infoDialog.getTxtProteins().setText(Double.toString(Util.roundDouble(data.getgProteins())));
        infoDialog.getTxtFat().setText(Double.toString(Util.roundDouble(data.getgFat())));
        infoDialog.getTxtGlicemyIndex().setText(Double.toString(Util.roundDouble(data.getGlicemyIndex())));
        infoDialog.getTxtKcal().setText(Double.toString(Util.roundDouble(data.getKcal())));
        infoDialog.getTxtDescription().setText(data.getDescription());
    }

    @Override
    protected void localizeComponents() {
        IngredientInfoDialog infoDialog = (IngredientInfoDialog) super.infoDialog;

        localizeLabel(infoDialog.getLblIngredientInformation(), INGREDIENT_INFORMATION);
        localizeLabel(infoDialog.getLblName(), NAME);
        localizeLabel(infoDialog.getLblCarbs(), CARBS);
        localizeLabel(infoDialog.getLblCarbsG(), GRAM);
        localizeLabel(infoDialog.getLblProteins(), PROTEINS);
        localizeLabel(infoDialog.getLblProteinsG(), GRAM);
        localizeLabel(infoDialog.getLblFat(), FAT);
        localizeLabel(infoDialog.getLblFatG(), GRAM);
        localizeLabel(infoDialog.getLblGlicemyIndex(), GLICEMY_INDEX);
        localizeLabel(infoDialog.getLblKcal(), KCAL);
        localizeLabel(infoDialog.getLblDescription(), DESCRIPTION);
    }
    
}
