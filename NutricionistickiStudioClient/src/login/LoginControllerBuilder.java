/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import builder.Controller;
import builder.ControllerBuilder;
import builder.Localizer;
import builder.RequestBuilder;
import builder.ResponseHandler;
import builder.Validator;
import javax.swing.JPanel;

/**
 *
 * @author maja
 */
public class LoginControllerBuilder extends ControllerBuilder {

    @Override
    protected JPanel createPanel() {
        return new LoginPanel();
    }

    @Override
    protected Localizer createLocalizer() {
        return new LoginLocalizer();
    }

    @Override
    protected Validator createValidator() {
        return new LoginValidator();
    }

    @Override
    protected ResponseHandler createResponseHandler() {
        return new LoginResponseHandler();
    }

    @Override
    protected RequestBuilder createRequestBuilder() {
        return new LoginRequestBuilder();
    }

    @Override
    public Controller createController() {
        return new LoginController();
    }
    
}
