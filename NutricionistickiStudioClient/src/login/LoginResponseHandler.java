/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import builder.Controller;
import builder.ControllerBuilder;
import builder.ResponseHandler;
import home.HomeControllerBuilder;
import model.User;
import shared.GlobalData;
import shared.Util;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class LoginResponseHandler extends ResponseHandler {

    @Override
    public void handleResponse(TransferObject response) {
        switch (response.getOperation()) {
            case LOGIN_SUCCESSFUL:
                loginSuccessful(response);
                break;
            case LOGIN_UNSUCCESSFUL:
                showErrorMessage(response);
                break;
        }
    }

    private void loginSuccessful(TransferObject response) {
        Util.showMessageDialog(LoginLocalizer.LOGIN_SUCCESS);
        GlobalData.getInstance().put(GlobalData.USER, response.get(User.USER_KEY));
        ControllerBuilder homeControllerBuilder = new HomeControllerBuilder();
        Controller homeController = homeControllerBuilder.build();
        controller.goToPanel(homeController);
    }

}
