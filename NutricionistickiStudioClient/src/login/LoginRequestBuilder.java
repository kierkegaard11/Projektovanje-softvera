/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import builder.RequestBuilder;
import model.User;
import transfer.Operation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class LoginRequestBuilder extends RequestBuilder {
    
    public TransferObject buildLoginRequest() {
        LoginPanel panel = (LoginPanel) controller.getPanel();
        String username = panel.getTxtUsername().getText().trim();
        String password = new String(panel.getTxtPassword().getPassword()).trim();
        
        return createRequest(Operation.LOGIN)
                .put(User.COL_USERNAME, username)
                .put(User.COL_PASSWORD, password)
                .build();
    }
    
}
