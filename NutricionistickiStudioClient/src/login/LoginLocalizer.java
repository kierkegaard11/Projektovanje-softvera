/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import builder.Localizer;

/**
 *
 * @author maja
 */
public class LoginLocalizer extends Localizer {
    
    public static final String BREADCRUMB = "panel.login.breadcrumb";
    public static final String REGISTER_SUCCESS = "operation.register.success";
    public static final String LOGIN_SUCCESS = "operation.login.success";
    public static final String VALIDATION_USERNAME_EMPTY = "panel.login.validation.username.empty";
    public static final String VALIDATION_PASSWORD_EMPTY = "panel.login.validation.password.empty";
    private static final String USERNAME = "panel.login.username";
    private static final String PASSWORD = "panel.login.password";
    private static final String LOGIN = "panel.login.login";
    private static final String NOT_REGISTERED_YET = "panel.login.notRegisteredYet";
    private static final String REGISTER = "panel.login.register";
    private static final String CONFIGURATION = "panel.login.configuration";

    @Override
    public void localizationChanged() {
        LoginPanel panel = (LoginPanel) super.panel;
        localizeLabelMessageKey(panel.getLblUsername(), USERNAME);
        localizeLabelMessageKey(panel.getLblPassword(), PASSWORD);
        localizeButtonMessageKey(panel.getBtnLogin(), LOGIN);
        localizeLabelMessageKey(panel.getLblNotRegisteredYet(), NOT_REGISTERED_YET);
        localizeButtonMessageKey(panel.getBtnRegister(), REGISTER);
        localizeButtonMessageKey(panel.getBtnConfiguration(), CONFIGURATION);
        localizeLabelValidationWithMessageKey(panel.getLblUsernameValidation(), VALIDATION_USERNAME_EMPTY);
        localizeLabelValidationWithMessageKey(panel.getLblPasswordValidation(), VALIDATION_PASSWORD_EMPTY);
    }

    @Override
    public void initialize() {
    }
    
}
