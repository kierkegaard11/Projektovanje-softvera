/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import builder.Controller;
import builder.ControllerBuilder;
import configuration.ConfigurationControllerBuilder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import register.RegisterControllerBuilder;

/**
 *
 * @author maja
 */
public class LoginController extends Controller {

    @Override
    public String getBreadcrumbTextKey() {
        return LoginLocalizer.BREADCRUMB;
    }

    @Override
    protected void clearValidationMessages() {
        LoginPanel panel = (LoginPanel) super.panel;
        clearValidationMessage(panel.getLblUsernameValidation());
        clearValidationMessage(panel.getLblPasswordValidation());
    }

    @Override
    protected void setupImages() {
    }

    @Override
    public void abstractInitialize() {
        addBtnLoginListener();
        addBtnRegisterListener();
        addBtnConfigurationListener();
    }

    private void addBtnLoginListener() {
        LoginPanel panel = (LoginPanel) super.panel;
        panel.getBtnLogin().addActionListener((ActionEvent e) -> {
            if (validateForm() && server.connect()) {
                LoginRequestBuilder requestBuilder = (LoginRequestBuilder) super.requestBuilder;
                sendRequestAndHandleResponse(requestBuilder.buildLoginRequest());
            }
        });
    }

    private void addBtnRegisterListener() {
        LoginPanel panel = (LoginPanel) super.panel;
        panel.getBtnRegister().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControllerBuilder registerControllerBuilder = new RegisterControllerBuilder();
                Controller registerController = registerControllerBuilder.build();
                goToPanel(registerController);
            }
        });
    }

    private void addBtnConfigurationListener() {
        LoginPanel panel = (LoginPanel) super.panel;
        panel.getBtnConfiguration().addActionListener((ActionEvent e) -> {
            ControllerBuilder configurationControllerBuilder = new ConfigurationControllerBuilder();
            Controller configurationController = configurationControllerBuilder.build();
            goToPanel(configurationController);
        });
    }

    private boolean validateForm() {
        LoginValidator validator = (LoginValidator) super.validator;
        boolean usernameValid = validator.validateUsername();
        boolean passwordValid = validator.validatePassword();
        return usernameValid && passwordValid;
    }
    
}
