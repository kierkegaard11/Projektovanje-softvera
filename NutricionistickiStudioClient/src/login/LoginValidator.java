/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import builder.Validator;
import javax.swing.JLabel;

/**
 *
 * @author maja
 */
public class LoginValidator extends Validator {

    public boolean validateUsername() {
        LoginPanel panel = (LoginPanel) super.panel;
        String username = panel.getTxtUsername().getText().trim();
        JLabel lblUsernameValidation = panel.getLblUsernameValidation();
        String message = localizationLoader.getString(LoginLocalizer.VALIDATION_USERNAME_EMPTY);
        return validateTextIsEmpty(username, lblUsernameValidation, message);
    }
    
    public boolean validatePassword() {
        LoginPanel panel = (LoginPanel) super.panel;
        String text = new String(panel.getTxtPassword().getPassword()).trim();
        JLabel lblValidation = panel.getLblPasswordValidation();
        String message = localizationLoader.getString(LoginLocalizer.VALIDATION_PASSWORD_EMPTY);
        return validateTextIsEmpty(text, lblValidation, message);
    }
    
}
