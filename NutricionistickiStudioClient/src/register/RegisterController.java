/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package register;

import builder.Controller;
import java.awt.event.ActionEvent;
import server.Server;

/**
 *
 * @author maja
 */
public class RegisterController extends Controller {

    @Override
    public String getBreadcrumbTextKey() {
        return RegisterLocalizer.BREADCRUMB;
    }

    @Override
    protected void clearValidationMessages() {
        RegisterPanel panel = (RegisterPanel) super.panel;
        clearValidationMessage(panel.getLblFirstNameValidation());
        clearValidationMessage(panel.getLblLastNameValidation());
        clearValidationMessage(panel.getLblUsernameValidation());
        clearValidationMessage(panel.getLblPasswordValidation());
    }

    @Override
    protected void setupImages() {
    }

    @Override
    public void abstractInitialize() {
        addBtnRegisterListener();
    }

    private void addBtnRegisterListener() {
        RegisterPanel panel = (RegisterPanel) super.panel;
        panel.getBtnRegister().addActionListener((ActionEvent e) -> {
            if (validateForm() && Server.getInstance().connect()) {
                RegisterRequestBuilder requestBuilder = (RegisterRequestBuilder) super.requestBuilder;
                sendRequestAndHandleResponse(requestBuilder.buildRegisterRequest());
            }
        });
    }

    private boolean validateForm() {
        RegisterValidator validator = (RegisterValidator) super.validator;
        boolean firstNameValid = validator.validateFirstName();
        boolean lastNameValid = validator.validateLastName();
        boolean usernameValid = validator.validateUsername();
        boolean passwordValid = validator.validatePassword();
        return firstNameValid && lastNameValid && usernameValid && passwordValid;
    }

}
