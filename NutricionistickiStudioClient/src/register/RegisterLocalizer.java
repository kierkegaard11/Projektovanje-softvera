/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package register;

import builder.Localizer;

/**
 *
 * @author maja
 */
public class RegisterLocalizer extends Localizer {

    public static final String BREADCRUMB = "panel.register.breadcrumb";
    public static final String REGISTER_SUCCESS = "operation.register.success";
    public static final String VALIDATION_FIRST_NAME_EMPTY = "panel.register.validation.lastName.empty";
    public static final String VALIDATION_LAST_NAME_EMPTY = "panel.register.validation.lastName.empty";
    public static final String VALIDATION_USERNAME_LENGTH = "panel.register.validation.username.length";
    public static final String VALIDATION_PASSWORD_LENGTH = "panel.register.validation.password.length";
    private static final String FIRST_NAME = "panel.register.firstName";
    private static final String LAST_NAME = "panel.register.lastName";
    private static final String USERNAME = "panel.register.username";
    private static final String PASSWORD = "panel.register.password";
    private static final String REGISTER = "panel.register.register";

    @Override
    public void localizationChanged() {
        RegisterPanel panel = (RegisterPanel) super.panel;
        localizeLabelMessageKey(panel.getLblFirstName(), FIRST_NAME);
        localizeLabelMessageKey(panel.getLblLastName(), LAST_NAME);
        localizeLabelMessageKey(panel.getLblUsername(), USERNAME);
        localizeLabelMessageKey(panel.getLblPassword(), PASSWORD);
        localizeButtonMessageKey(panel.getBtnRegister(), REGISTER);
        localizeLabelValidationWithMessageKey(panel.getLblFirstNameValidation(), VALIDATION_FIRST_NAME_EMPTY);
        localizeLabelValidationWithMessageKey(panel.getLblLastNameValidation(), VALIDATION_LAST_NAME_EMPTY);
        String usernameValidationMessage = String.format(localizationLoader.getString(VALIDATION_USERNAME_LENGTH), RegisterValidator.USERNAME_MIN_LENGTH);
        localizeLabelValidationWithMessage(panel.getLblUsernameValidation(), usernameValidationMessage);
        String passwordValidationMessage = String.format(localizationLoader.getString(VALIDATION_PASSWORD_LENGTH), RegisterValidator.PASSWORD_MIN_LENGTH);
        localizeLabelValidationWithMessage(panel.getLblUsernameValidation(), passwordValidationMessage);
    }

    @Override
    public void initialize() {
    }

}
