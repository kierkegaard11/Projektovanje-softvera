/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package register;

import builder.RequestBuilder;
import model.User;
import transfer.Operation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class RegisterRequestBuilder extends RequestBuilder {
    
    public TransferObject buildRegisterRequest() {
        RegisterPanel panel = (RegisterPanel) controller.getPanel();
        String firstName = panel.getTxtFirstName().getText().trim();
        String lastName = panel.getTxtLastName().getText().trim();
        String username = panel.getTxtUsername().getText().trim();
        String password = new String(panel.getTxtPassword().getPassword()).trim();
        
        return createRequest(Operation.REGISTER)
                .put(User.COL_FIRSTNAME, firstName)
                .put(User.COL_LASTNAME, lastName)
                .put(User.COL_USERNAME, username)
                .put(User.COL_PASSWORD, password)
                .build();
    }
    
}
