/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package register;

import builder.Validator;
import javax.swing.JLabel;

/**
 *
 * @author maja
 */
public class RegisterValidator extends Validator {

    public static int USERNAME_MIN_LENGTH = 5;
    public static int PASSWORD_MIN_LENGTH = 5;
    
    public boolean validateFirstName() {
        RegisterPanel panel = (RegisterPanel) super.panel;
        String firstName = panel.getTxtFirstName().getText().trim();
        JLabel lblFirstNameValidation = panel.getLblFirstNameValidation();
        String message = localizationLoader.getString(RegisterLocalizer.VALIDATION_FIRST_NAME_EMPTY);
        return validateTextIsEmpty(firstName, lblFirstNameValidation, message);
    }
    
    public boolean validateLastName() {
        RegisterPanel panel = (RegisterPanel) super.panel;
        String lastName = panel.getTxtLastName().getText().trim();
        JLabel lblLastNameValidation = panel.getLblLastNameValidation();
        String message = localizationLoader.getString(RegisterLocalizer.VALIDATION_LAST_NAME_EMPTY);
        return validateTextIsEmpty(lastName, lblLastNameValidation, message);
    }
    
    public boolean validateUsername() {
        RegisterPanel panel = (RegisterPanel) super.panel;
        String username = panel.getTxtUsername().getText().trim();
        JLabel lblUsernameValidation = panel.getLblUsernameValidation();
        String message = String.format(localizationLoader.getString(RegisterLocalizer.VALIDATION_USERNAME_LENGTH), USERNAME_MIN_LENGTH);
        return validateTextLength(username, lblUsernameValidation, message, USERNAME_MIN_LENGTH);
    }
    
    public boolean validatePassword() {
        RegisterPanel panel = (RegisterPanel) super.panel;
        String password = new String(panel.getTxtPassword().getPassword()).trim();
        JLabel lblPasswordValidation = panel.getLblPasswordValidation();
        String message = String.format(localizationLoader.getString(RegisterLocalizer.VALIDATION_PASSWORD_LENGTH), PASSWORD_MIN_LENGTH);
        return validateTextLength(password, lblPasswordValidation, message, PASSWORD_MIN_LENGTH);
    }
    
}
