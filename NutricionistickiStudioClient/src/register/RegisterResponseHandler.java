/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package register;

import builder.ResponseHandler;
import shared.Util;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class RegisterResponseHandler extends ResponseHandler {

    @Override
    public void handleResponse(TransferObject response) {
        switch (response.getOperation()) {
            case REGISTRATION_SUCCESSFUL:
                Util.showMessageDialog(RegisterLocalizer.REGISTER_SUCCESS);
                break;
            case REGISTRATION_UNSUCCESSFUL:
                showErrorMessage(response);
                break;
        }
    }
}
