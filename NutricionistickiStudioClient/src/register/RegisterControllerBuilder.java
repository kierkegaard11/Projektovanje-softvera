/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package register;

import builder.Controller;
import builder.ControllerBuilder;
import builder.Localizer;
import builder.RequestBuilder;
import builder.ResponseHandler;
import builder.Validator;
import javax.swing.JPanel;

/**
 *
 * @author maja
 */
public class RegisterControllerBuilder extends ControllerBuilder {

    @Override
    protected JPanel createPanel() {
        return new RegisterPanel();
    }

    @Override
    protected Localizer createLocalizer() {
        return new RegisterLocalizer();
    }

    @Override
    protected Validator createValidator() {
        return new RegisterValidator();
    }

    @Override
    protected ResponseHandler createResponseHandler() {
        return new RegisterResponseHandler();
    }

    @Override
    protected RequestBuilder createRequestBuilder() {
        return new RegisterRequestBuilder();
    }

    @Override
    protected Controller createController() {
        return new RegisterController();
    }
    
}
