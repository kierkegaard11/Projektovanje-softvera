/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

import main.HeaderController;
import main.HeaderPanel;
import main.MainController;
import main.MainForm;

/**
 *
 * @author maja
 */
public class Factory {
    
    public static HeaderController createHeaderController() {
        HeaderPanel headerPanel = new HeaderPanel();
        return new HeaderController(headerPanel);
    }
    
    public static MainController createMainController(HeaderController headerController) {
        MainForm mainForm = new MainForm();
        return new MainController(headerController, mainForm);
    }
    
}
