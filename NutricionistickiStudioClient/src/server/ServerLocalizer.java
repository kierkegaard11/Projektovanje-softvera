/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

/**
 *
 * @author maja
 */
public class ServerLocalizer {
 
    public static final String CONNECT_ERROR = "server.connection.connectError";
    public static final String CONNECT_SUCCESS = "server.connection.connectSuccess";
    public static final String SEND_DATA_ERROR = "server.connection.sendDataError";
    public static final String READ_DATA_ERROR = "server.connection.readDataError";
    
}
