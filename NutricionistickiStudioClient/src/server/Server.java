/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import shared.ConfigurationProperties;
import shared.Util;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public final class Server {

    private static Server instance;

    private ServerConnection serverConnection;
    private ConfigurationProperties configurationProperties;

    private Server() {
        serverConnection = new ServerConnection();
        configurationProperties = ConfigurationProperties.getInstance();
    }

    public static Server getInstance() {
        if (instance == null) {
            instance = new Server();
        }
        return instance;
    }

    public boolean connect() {
        if (!serverConnection.connected()) {
            try {
                String host = configurationProperties.getProperty(ConfigurationProperties.SERVER_HOST);
                int port = Integer.parseInt(configurationProperties.getProperty(ConfigurationProperties.SERVER_PORT));
                serverConnection.connect(host, port);
                Util.showMessageDialog(ServerLocalizer.CONNECT_SUCCESS);
            } catch (IOException ex) {
                Logger.getLogger(ServerConnection.class.getName()).log(Level.SEVERE, null, ex);
                Util.showMessageDialog(ServerLocalizer.CONNECT_ERROR);
                return false;
            }
        }
        return true;
    }

    public boolean send(TransferObject request) {
        if (serverConnection.connected()) {
            try {
                serverConnection.send(request);
                return true;
            } catch (IOException ex) {
                Logger.getLogger(ServerConnection.class.getName()).log(Level.SEVERE, null, ex);
                disconnect();
                Util.showMessageDialog(ServerLocalizer.SEND_DATA_ERROR);
            }
        } else {
            Util.showMessageDialog(ServerLocalizer.CONNECT_ERROR);
        }
        return false;
    }

    public TransferObject read() {
        if (serverConnection.connected()) {
            try {
                return serverConnection.read();
            } catch (IOException ex) {
                Logger.getLogger(ServerConnection.class.getName()).log(Level.SEVERE, null, ex);
                disconnect();
                Util.showMessageDialog(ServerLocalizer.READ_DATA_ERROR);
            }
        } else {
            Util.showMessageDialog(ServerLocalizer.CONNECT_ERROR);
        }
        return null;
    }

    public void disconnect() {
        serverConnection.disconnect();
    }

}
