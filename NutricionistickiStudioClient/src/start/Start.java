/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package start;

import builder.ControllerBuilder;
import factory.Factory;
import localization.LocalizationLoader;
import login.LoginControllerBuilder;
import main.HeaderController;
import main.MainController;

/**
 *
 * @author maja
 */
public class Start {
    
    public static void main(String[] args) {
        
        LocalizationLoader.setFilePath("./src/locales");
        
        HeaderController headerController = Factory.createHeaderController();
        headerController.initialize();
        
        MainController mainController = Factory.createMainController(headerController);
        mainController.initialize();
        
        ControllerBuilder loginControllerBuilder = new LoginControllerBuilder();
        mainController.setPanelController(loginControllerBuilder.build());
        
        mainController.showForm();
        
    }
    
}
