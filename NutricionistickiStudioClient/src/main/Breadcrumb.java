/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import builder.Controller;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import localization.LocalizationLoader;
import localization.LocalizationObserver;
import localization.LocalizationSubject;

/**
 *
 * @author maja
 */
public class Breadcrumb implements LocalizationObserver {

    public static int WIDTH = 130;

    private Controller panelController;
    private JButton button;
    
    public Breadcrumb(Controller panelController) {
        this.panelController = panelController;
        initializeButton();
        addButtonMouseListener();
        LocalizationSubject.getInstance().subscribe(this);
    }

    private void initializeButton() {
        button = new JButton();
        button.setBackground(Color.white);
        button.setBorder(new LineBorder(Color.black));
    }

    private void addButtonMouseListener() {
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                breadcrumbClicked();
            }
        });
    }

    private void breadcrumbClicked() {
        BreadcrumbSubject.getInstance().breadcrumbClicked(this);
    }

    @Override
    public void localizationChanged() {
        String breadcrumbTextKey = panelController.getBreadcrumbTextKey();
        String buttonText = LocalizationLoader.getInstance().getString(breadcrumbTextKey);
        button.setText(buttonText);
    }

    public Controller getPanelController() {
        return panelController;
    }

    public JButton getButton() {
        return button;
    }

}
