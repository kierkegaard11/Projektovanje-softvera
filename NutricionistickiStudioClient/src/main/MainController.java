/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import builder.Controller;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import localization.LocalizationLoader;
import localization.LocalizationObserver;
import localization.LocalizationSubject;
import org.netbeans.lib.awtextra.AbsoluteConstraints;

/**
 *
 * @author maja
 */
public class MainController implements BreadcrumbObserver, LocalizationObserver {
    
    private static final int FORM_WIDTH = 1024;
    private static final int FORM_HEIGHT = 798;
    private static final int PANEL_BREADCRUMBS_HEIGHT = 30;
    private static final String BACKGROUND_IMAGE_PATH = "/images/background.jpg";
    private static final String TITLE = "form.main.title";
    
    private HeaderController panelHeaderController;
    private MainForm formMain;
    private JLabel backgroundImage;
    private Controller currentPanelController;
    
    public MainController(HeaderController panelHeaderController, MainForm formMain) {
        this.panelHeaderController = panelHeaderController;
        this.formMain = formMain;
        BreadcrumbSubject.getInstance().subscribe(this);
        LocalizationSubject.getInstance().subscribe(this);
    }

    public void initialize() {
        initializeBackgroundImage();
        setPanelHeader(panelHeaderController.getPanelHeader());
    }
    
    private void initializeBackgroundImage() {
        backgroundImage = new JLabel();
        backgroundImage.setIcon(new ImageIcon(getClass().getResource(BACKGROUND_IMAGE_PATH)));
        backgroundImage.setBounds(0, 0, FORM_WIDTH, getPanelHeight());
    }
    
    public void showForm() {
        formMain.setLocationRelativeTo(null);
        formMain.setVisible(true);
    }

    public void setPanelHeader(JPanel panelHeader) {
        formMain.getContentPane().add(panelHeader, new AbsoluteConstraints(0, 0, FORM_WIDTH, PANEL_BREADCRUMBS_HEIGHT));
        formMain.pack();
    }
    
    public void setPanelController(Controller panelController) {
        panelController.setFormMainController(this);
        setCurrentPanelController(panelController);
        addCurrentPanelToForm();
        addNewBreadcrumb(panelController);
        panelHeaderController.loadLocalization();
    }
    
    private void addNewBreadcrumb(Controller panelController) {
        panelHeaderController.addNewBreadcrumb(panelController);
    }
    
    private void setCurrentPanelController(Controller panelController) {
        if (currentPanelController != null) {
            formMain.getContentPane().remove(currentPanelController.getPanel());
        }
        currentPanelController = panelController;
        currentPanelController.getPanel().add(backgroundImage);
    }
    
    private void addCurrentPanelToForm() {
        formMain.getContentPane().add(currentPanelController.getPanel(), new AbsoluteConstraints(0, PANEL_BREADCRUMBS_HEIGHT, FORM_WIDTH, getPanelHeight()));
        formMain.pack();
        formMain.repaint();
    }
    
    private int getPanelHeight() {
        return FORM_HEIGHT - PANEL_BREADCRUMBS_HEIGHT;
    }

    @Override
    public void breadcrumbClicked(Breadcrumb clickedBreadcrumb) {
        if (clickedBreadcrumb.getPanelController() != currentPanelController) {
            setPanelController(clickedBreadcrumb.getPanelController());
        }
    }

    @Override
    public void localizationChanged() {
        formMain.setTitle(LocalizationLoader.getInstance().getString(TITLE));
    }
    
}
