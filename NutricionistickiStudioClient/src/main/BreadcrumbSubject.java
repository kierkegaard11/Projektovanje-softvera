/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public class BreadcrumbSubject {
    
    private static BreadcrumbSubject instance;
    
    private List<BreadcrumbObserver> observers;
    
    private BreadcrumbSubject() {
        observers = new ArrayList<>();
    }
    
    public static BreadcrumbSubject getInstance() {
        if (instance == null) {
            instance = new BreadcrumbSubject();
        }
        return instance;
    }
    
    public void subscribe(BreadcrumbObserver observer) {
        observers.add(observer);
    }
    
    public void unsubscribe(BreadcrumbObserver observer) {
        observers.remove(observer);
    }
    
    public void breadcrumbClicked(Breadcrumb clickedBreadcrumb) {
        for (BreadcrumbObserver observer : observers) {
            observer.breadcrumbClicked(clickedBreadcrumb);
        }
    }
    
}
