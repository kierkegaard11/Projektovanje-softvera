/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import builder.Controller;
import home.HomeController;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import localization.Locale;
import localization.LocalizationLoader;
import login.LoginController;
import org.netbeans.lib.awtextra.AbsoluteConstraints;

/**
 *
 * @author maja
 */
public class HeaderController implements BreadcrumbObserver {

    private HeaderPanel panelHeader;
    private List<Breadcrumb> breadcrumbs;

    public HeaderController(HeaderPanel panelHeader) {
        this.panelHeader = panelHeader;
        this.breadcrumbs = new ArrayList<>();
        BreadcrumbSubject.getInstance().subscribe(this);
    }

    @Override
    public void breadcrumbClicked(Breadcrumb clickedBreadcrumb) {
        removeLeadingBredcrumbs(clickedBreadcrumb);
    }

    public void initialize() {
        JComboBox cmbLocales = panelHeader.getCmbLocales();
        cmbLocales.addItem(Locale.EN);
        cmbLocales.addItem(Locale.SR);
        addCmbLocalesListener(cmbLocales);
    }

    private void addCmbLocalesListener(JComboBox cmbLocales) {
        cmbLocales.addActionListener((ActionEvent e) -> {
            loadLocalization();
        });
    }

    public void loadLocalization() {
        Locale selectedLocale = (Locale) panelHeader.getCmbLocales().getSelectedItem();
        LocalizationLoader.getInstance().loadLocalization(selectedLocale);
    }

    public JPanel getPanelHeader() {
        return panelHeader;
    }

    public void addNewBreadcrumb(Controller panelController) {
        if (!breadcrumbExists(panelController)) {
            Breadcrumb breadcrumb = new Breadcrumb(panelController);
            addBreadcrumb(breadcrumb);
        }
    }

    private boolean breadcrumbExists(Controller panelController) {
        for (Breadcrumb breadcrumb : breadcrumbs) {
            if (breadcrumb.getPanelController().getPanel().equals(panelController.getPanel())) {
                return true;
            }
        }
        return false;
    }

    private void addBreadcrumb(Breadcrumb breadcrumb) {
        if (breadcrumb.getPanelController() instanceof HomeController ||
                breadcrumb.getPanelController() instanceof LoginController) {
            resetBreadcrumbs();
        }
        AbsoluteConstraints constraints = getBreadcrumbConstraints();
        panelHeader.getPanelBreadcrumbs().add(breadcrumb.getButton(), constraints);
        breadcrumbs.add(breadcrumb);
    }

    private AbsoluteConstraints getBreadcrumbConstraints() {
        int x = breadcrumbs.size() * Breadcrumb.WIDTH;
        int y = 0;
        int width = Breadcrumb.WIDTH;
        int height = panelHeader.getHeight();
        return new AbsoluteConstraints(x, y, width, height);
    }

    private void removeLeadingBredcrumbs(Breadcrumb clickedBreadcrumb) {
        for (int i = breadcrumbs.size() - 1; i >= 0; i--) {
            Breadcrumb breadcrumb = breadcrumbs.get(i);
            if (!breadcrumb.equals(clickedBreadcrumb)) {
                panelHeader.getPanelBreadcrumbs().remove(breadcrumbs.get(i).getButton());
                breadcrumbs.remove(i);
            } else {
                break;
            }
        }
    }

    private void resetBreadcrumbs() {
        panelHeader.getPanelBreadcrumbs().removeAll();
        breadcrumbs = new ArrayList<>();
    }

}
