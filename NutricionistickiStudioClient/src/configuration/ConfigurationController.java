/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration;

import builder.Controller;
import configuration.keyValue.KeyValueController;
import configuration.keyValue.KeyValueIsEmptyValidator;
import configuration.keyValue.KeyValueIsNumberValidator;
import configuration.keyValue.KeyValuePanel;
import configuration.keyValue.KeyValueValidator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import shared.ConfigurationProperties;
import shared.Util;

/**
 *
 * @author maja
 */
public class ConfigurationController extends Controller {
    
    private List<KeyValueController> keyValueControllers;

    @Override
    public String getBreadcrumbTextKey() {
        return ConfigurationLocalizer.BREADCRUMB;
    }

    @Override
    protected void clearValidationMessages() {
        if (keyValueControllers != null) {
            for (KeyValueController keyValueController : keyValueControllers) {
                keyValueController.clearValidationMessage();
            }
        }
    }

    @Override
    protected void setupImages() {
    }

    @Override
    protected void abstractInitialize() {
        setupKeyValuePanels();
        loadConfigurationProperties();
        addBtnSaveListener();
    }

    private void setupKeyValuePanels() {
        keyValueControllers = new ArrayList<>();
        setupSpecificKeyValuePanels();
        ((ConfigurationValidator) validator).setKeyValueControllers(keyValueControllers);
        ((ConfigurationLocalizer) localizer).setKeyValueControllers(keyValueControllers);
        clearValidationMessages();
    }

    private void loadConfigurationProperties() {
        for (KeyValueController keyValueController : keyValueControllers) {
            String propertyKey = keyValueController.getPropertyKey();
            String propertyValue = configurationProperties.getProperty(propertyKey);
            keyValueController.setPropertyValue(propertyValue);
        }
    }

    private void addBtnSaveListener() {
        ConfigurationPanel panel = (ConfigurationPanel) super.panel;
        panel.getBtnSave().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                saveConfiguration();
            }
        });
    }

    private void saveConfiguration() {
        if (((ConfigurationValidator) validator).validateForm()) {
            for (KeyValueController keyValueController : keyValueControllers) {
                configurationProperties.setProperty(keyValueController.getPropertyKey(), keyValueController.getPropertyValue());
            }
            configurationProperties.storeProperties();
            Util.showMessageDialog(ConfigurationLocalizer.CONFIGURATION_SAVED);
        }
    }

    private void setupSpecificKeyValuePanels() {
        ConfigurationPanel panel = (ConfigurationPanel) super.panel;
        setupKeyValue(panel.getServerHostPanel(), ConfigurationProperties.SERVER_HOST, new KeyValueIsEmptyValidator());
        setupKeyValue(panel.getServerPortPanel(), ConfigurationProperties.SERVER_PORT, new KeyValueIsNumberValidator());
    }
    
    private void setupKeyValue(KeyValuePanel keyValuePanel, String propertyKey, KeyValueValidator validator) {
        KeyValueController keyValueController = new KeyValueController(propertyKey, keyValuePanel, validator);
        keyValueControllers.add(keyValueController);
    }

}
