/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration;

import builder.Validator;
import configuration.keyValue.KeyValueController;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public class ConfigurationValidator extends Validator {
    
    private List<KeyValueController> keyValueControllers;

    public void setKeyValueControllers(List<KeyValueController> keyValueControllers) {
        this.keyValueControllers = keyValueControllers;
    }
    
    public boolean validateForm() {
        List<Boolean> validList = new ArrayList<>();
        for (KeyValueController keyValueController : keyValueControllers) {
            validList.add(keyValueController.validate());
        }
        boolean everyPropertyValid = true;
        for (Boolean valid : validList) {
            everyPropertyValid = everyPropertyValid && valid;
        }
        return everyPropertyValid;
    }
    
}
