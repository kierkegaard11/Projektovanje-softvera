/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration.keyValue;

import localization.LocalizationLoader;

/**
 *
 * @author maja
 */
public abstract class KeyValueValidator {

    private static final String VALIDATION_MESSAGE_FORMAT = "panel.configuration.validation.%s";
    
    protected KeyValuePanel panel;
    private LocalizationLoader localizationLoader;
    private String propertyKey;

    public KeyValueValidator() {
        localizationLoader = LocalizationLoader.getInstance();
    }
    
    public boolean validate() {
        if (validateAbstract()) {
            panel.getLblValidation().setText("");
            return true;
        }
        panel.getLblValidation().setText(getValidationMessage());
        return false;
    }
    
    protected abstract boolean validateAbstract();
    
    protected abstract String getValidatorType();

    private String getValidationMessage() {
        String validationKey = String.format(VALIDATION_MESSAGE_FORMAT, getValidatorType());
        String validationMessage = String.format(localizationLoader.getString(validationKey), propertyKey);
        return validationMessage;
    }

    public void setPanel(KeyValuePanel panel) {
        this.panel = panel;
    }

    public void setPropertyKey(String propertyKey) {
        this.propertyKey = propertyKey;
    }
    
}
