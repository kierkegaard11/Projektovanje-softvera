/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration.keyValue;

/**
 *
 * @author maja
 */
public class KeyValueController {
    
    private String propertyKey;
    private KeyValuePanel panel;
    private KeyValueValidator validator;

    public KeyValueController(String propertyKey, KeyValuePanel panel, KeyValueValidator validator) {
        this.propertyKey = propertyKey;
        this.panel = panel;
        this.validator = validator;
        initialize();
    }

    private void initialize() {
        panel.getLblPropertyKey().setText(propertyKey);
        validator.setPropertyKey(propertyKey);
        validator.setPanel(panel);
    }
    
    public void clearValidationMessage() {
        panel.getLblValidation().setText("");
    }

    public boolean validate() {
        return validator.validate();
    }
    
    public String getPropertyKey() {
        return propertyKey;
    }
    
    public KeyValuePanel getPanel() {
        return panel;
    }
    
    public String getPropertyValue() {
        return panel.getTxtPropertyValue().getText().trim();
    }

    public void setPropertyValue(String propertyValue) {
        panel.getTxtPropertyValue().setText(propertyValue);
    }

    public String getValidatorType() {
        return validator.getValidatorType();
    }
    
}
