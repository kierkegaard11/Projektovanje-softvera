/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration.keyValue;

/**
 *
 * @author maja
 */
public class KeyValueIsNumberValidator extends KeyValueValidator {

    private static final String TYPE = "isNumber";

    @Override
    protected boolean validateAbstract() {
        try {
            Integer.parseInt(panel.getTxtPropertyValue().getText().trim());
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    @Override
    protected String getValidatorType() {
        return TYPE;
    }
    
}
