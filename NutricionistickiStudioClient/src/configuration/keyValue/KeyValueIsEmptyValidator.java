/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration.keyValue;

/**
 *
 * @author maja
 */
public class KeyValueIsEmptyValidator extends KeyValueValidator {

    private static final String TYPE = "isEmpty";

    @Override
    protected boolean validateAbstract() {
        return !panel.getTxtPropertyValue().getText().trim().isEmpty();
    }

    @Override
    protected String getValidatorType() {
        return TYPE;
    }
    
}
