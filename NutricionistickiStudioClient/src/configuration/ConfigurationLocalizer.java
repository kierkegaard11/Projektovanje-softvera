/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration;

import builder.Localizer;
import configuration.keyValue.KeyValueController;
import configuration.keyValue.KeyValuePanel;
import java.util.List;
import javax.swing.JLabel;

/**
 *
 * @author maja
 */
public class ConfigurationLocalizer extends Localizer {

    public static final String BREADCRUMB = "panel.configuration.breadcrumb";
    public static final String CONFIGURATION_SAVED = "form.configuration.configurationSaved";
    private static final String MESSAGE_FORMAT = "panel.configuration.validation.%s";
    private static final String SAVE = "panel.configuration.save";

    private List<KeyValueController> keyValueControllers;

    public void setKeyValueControllers(List<KeyValueController> keyValueControllers) {
        this.keyValueControllers = keyValueControllers;
    }

    @Override
    public void localizationChanged() {
        for (KeyValueController keyValueController : keyValueControllers) {
            localizeKeyValue(keyValueController);
        }

        ConfigurationPanel panel = (ConfigurationPanel) super.panel;
        localizeButtonMessageKey(panel.getBtnSave(), SAVE);
    }

    private void localizeKeyValue(KeyValueController keyValueController) {
        KeyValuePanel keyValuePanel = keyValueController.getPanel();
        JLabel lblValidation = keyValuePanel.getLblValidation();
        String propertyKey = keyValueController.getPropertyKey();
        String messageKey = String.format(MESSAGE_FORMAT, keyValueController.getValidatorType());
        String message = String.format(localizationLoader.getString(messageKey), propertyKey);
        localizeLabelValidationWithMessage(lblValidation, message);
    }

    @Override
    public void initialize() {
    }

}
