/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration;

import builder.Controller;
import builder.ControllerBuilder;
import builder.Localizer;
import builder.RequestBuilder;
import builder.ResponseHandler;
import builder.Validator;
import javax.swing.JPanel;

/**
 *
 * @author maja
 */
public class ConfigurationControllerBuilder extends ControllerBuilder {

    @Override
    protected JPanel createPanel() {
        return new ConfigurationPanel();
    }

    @Override
    protected Localizer createLocalizer() {
        return new ConfigurationLocalizer();
    }

    @Override
    protected Validator createValidator() {
        return new ConfigurationValidator();
    }

    @Override
    protected ResponseHandler createResponseHandler() {
        return new ConfigurationResponseHandler();
    }

    @Override
    protected RequestBuilder createRequestBuilder() {
        return new ConfigurationRequestBuilder();
    }

    @Override
    protected Controller createController() {
        return new ConfigurationController();
    }
    
}
