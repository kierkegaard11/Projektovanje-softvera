/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home;

import builder.Controller;
import builder.ControllerBuilder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import medicalRecords.MedicalRecordsControllerBuilder;
import shared.ImageController;

/**
 *
 * @author maja
 */
public class HomeController extends Controller {

    @Override
    public String getBreadcrumbTextKey() {
        return HomeLocalizer.BREADCRUMB;
    }

    @Override
    protected void clearValidationMessages() {
    }

    @Override
    protected void setupImages() {
        HomePanel panel = (HomePanel) super.panel;
        imageController.setupImage(panel.getLblDishesImage(), ImageController.DISHES_IMAGE, ImageController.DISHES_HOVER_IMAGE);
        imageController.setupImage(panel.getLblMenusImage(), ImageController.MENUS_IMAGE, ImageController.MENUS_HOVER_IMAGE);
        imageController.setupImage(panel.getLblMedicalRecordsImage(), ImageController.MEDICAL_RECORDS_IMAGE, ImageController.MEDICAL_RECORDS_HOVER_IMAGE);
        addImageOnClickListeners();
    }

    @Override
    public void abstractInitialize() {
        addBtnLogoutListener();
    }

    private void addBtnLogoutListener() {
        HomePanel panel = (HomePanel) super.panel;
        panel.getBtnLogout().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logout();
            }
        });
    }

    private void addImageOnClickListeners() {
        addDishesImageOnClickListener();
        addMenusImageOnClickListener();
        addMedicalRecordsImageOnClickListener();
    }

    private void addDishesImageOnClickListener() {
        HomeRequestBuilder requestBuilder = (HomeRequestBuilder) super.requestBuilder;
        ((HomePanel) super.panel).getLblDishesImage().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                sendRequestAndHandleResponse(requestBuilder.buildFindIngredientRequest());
            }
        });
    }

    private void addMenusImageOnClickListener() {
        HomeRequestBuilder requestBuilder = (HomeRequestBuilder) super.requestBuilder;
        ((HomePanel) super.panel).getLblMenusImage().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                sendRequestAndHandleResponse(requestBuilder.buildFindMenuTypeRequest());
            }
        });
    }

    private void addMedicalRecordsImageOnClickListener() {
        ((HomePanel) super.panel).getLblMedicalRecordsImage().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ControllerBuilder medicalRecordsControllerBuilder = new MedicalRecordsControllerBuilder();
                Controller medicalRecordsController = medicalRecordsControllerBuilder.build();
                goToPanel(medicalRecordsController);
            }
        });
    }

}
