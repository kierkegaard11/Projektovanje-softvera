/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home;

import builder.Controller;
import builder.ControllerBuilder;
import builder.ResponseHandler;
import dishes.DishesControllerBuilder;
import java.util.List;
import menus.MenusControllerBuilder;
import model.Ingredient;
import model.MenuType;
import model.persistence.Persistence;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class HomeResponseHandler extends ResponseHandler {

    @Override
    public void handleResponse(TransferObject response) {
        switch (response.getOperation()) {
            case FIND_INGREDIENT_SUCCESS:
                handleFindIngredientSuccess(response);
                break;
            case FIND_INGREDIENT_ERROR:
                showErrorMessage(response);
                break;
            case FIND_MENU_TYPE_SUCCESS:
                handleFindMenuTypeSuccess(response);
                break;
            case FIND_MENU_TYPE_ERROR:
                showErrorMessage(response);
                break;
        }
    }

    private void handleFindIngredientSuccess(TransferObject response) {
        List<Persistence> ingredients = (List<Persistence>) response.get(Ingredient.INGREDIENT_KEY);
        ControllerBuilder dishesControllerBuilder = new DishesControllerBuilder(ingredients);
        Controller dishesController = dishesControllerBuilder.build();
        controller.goToPanel(dishesController);
    }

    private void handleFindMenuTypeSuccess(TransferObject response) {
        List<Persistence> menuTypes = (List<Persistence>) response.get(MenuType.MENU_TYPE_KEY);
        ControllerBuilder menusControllerBuilder = new MenusControllerBuilder(menuTypes);
        Controller menusController = menusControllerBuilder.build();
        controller.goToPanel(menusController);
    }

}
