/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home;

import builder.Localizer;
import model.User;
import shared.GlobalData;

/**
 *
 * @author maja
 */
public class HomeLocalizer extends Localizer {

    public static final String BREADCRUMB = "panel.home.breadcrumb";
    private static final String LOGOUT = "panel.home.logout";
    private static final String GREETINGS = "panel.home.greetings";
    private static final String DISHES = "panel.home.dishes";
    private static final String MENUS = "panel.home.menus";
    private static final String MEDICAL_RECORDS = "panel.home.medicalRecords";

    @Override
    public void localizationChanged() {
        HomePanel panel = (HomePanel) super.panel;
        localizeButtonMessageKey(panel.getBtnLogout(), LOGOUT);
        localizeLblGreetings(panel);
        localizeLabelMessageKey(panel.getLblDishes(), DISHES);
        localizeLabelMessageKey(panel.getLblMenus(), MENUS);
        localizeLabelMessageKey(panel.getLblMedicalRecords(), MEDICAL_RECORDS);
    }
    
    private void localizeLblGreetings(HomePanel panel) {
        User user = (User) GlobalData.getInstance().get("user");
        String message = String.format(localizationLoader.getString(GREETINGS), user.getFirstName(), user.getLastName());
        localizeLabelMessage(panel.getLblGreetings(), message);
    }

    @Override
    public void initialize() {
    }
    
}
