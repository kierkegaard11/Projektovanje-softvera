/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home;

import builder.RequestBuilder;
import transfer.Operation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class HomeRequestBuilder extends RequestBuilder {

    public TransferObject buildFindIngredientRequest() {
        return createRequest(Operation.FIND_INGREDIENT)
                .build();
    }

    public TransferObject buildFindMenuTypeRequest() {
        return createRequest(Operation.FIND_MENU_TYPE)
                .build();
    }
    
}
