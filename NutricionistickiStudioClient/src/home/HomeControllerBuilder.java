/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home;

import builder.Controller;
import builder.ControllerBuilder;
import builder.Localizer;
import builder.RequestBuilder;
import builder.ResponseHandler;
import builder.Validator;
import javax.swing.JPanel;

/**
 *
 * @author maja
 */
public class HomeControllerBuilder extends ControllerBuilder {

    @Override
    protected JPanel createPanel() {
        return new HomePanel();
    }

    @Override
    protected Localizer createLocalizer() {
        return new HomeLocalizer();
    }

    @Override
    protected Validator createValidator() {
        return new HomeValidator();
    }

    @Override
    protected ResponseHandler createResponseHandler() {
        return new HomeResponseHandler();
    }

    @Override
    protected RequestBuilder createRequestBuilder() {
        return new HomeRequestBuilder();
    }

    @Override
    protected Controller createController() {
        return new HomeController();
    }
    
}
