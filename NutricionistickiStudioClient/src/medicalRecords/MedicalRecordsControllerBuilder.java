/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords;

import builder.Controller;
import builder.ControllerBuilder;
import builder.Localizer;
import builder.RequestBuilder;
import builder.ResponseHandler;
import builder.Validator;
import javax.swing.JPanel;

/**
 *
 * @author maja
 */
public class MedicalRecordsControllerBuilder extends ControllerBuilder {

    @Override
    protected JPanel createPanel() {
        return new MedicalRecordsPanel();
    }

    @Override
    protected Localizer createLocalizer() {
        return new MedicalRecordsLocalizer();
    }

    @Override
    protected Validator createValidator() {
        return new MedicalRecordsValidator();
    }

    @Override
    protected ResponseHandler createResponseHandler() {
        return new MedicalRecordsResponseHandler();
    }

    @Override
    protected RequestBuilder createRequestBuilder() {
        return new MedicalRecordsRequestBuilder();
    }

    @Override
    protected Controller createController() {
        return new MedicalRecordsController();
    }
    
}
