/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import shared.LocalizedCmbOption;

/**
 *
 * @author maja
 */
public class MedicalRecordsPanel extends javax.swing.JPanel {

    /**
     * Creates new form MedicalRecordPanel
     */
    public MedicalRecordsPanel() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblSearchCriteria = new javax.swing.JLabel();
        lblMedicalRecordId = new javax.swing.JLabel();
        txtMedicalRecordId = new javax.swing.JTextField();
        lblFirstName = new javax.swing.JLabel();
        txtFirstName = new javax.swing.JTextField();
        lblLastName = new javax.swing.JLabel();
        txtLastName = new javax.swing.JTextField();
        lblGender = new javax.swing.JLabel();
        cmbGender = new javax.swing.JComboBox<>();
        lblSearchResults = new javax.swing.JLabel();
        lblMedicalRecordIdValidation = new javax.swing.JLabel();
        lblMedicalRecordsImage = new javax.swing.JLabel();
        btnSearch = new shared.Button();
        scrollPaneMedicalRecords = new javax.swing.JScrollPane();
        tableMedicalRecords = new javax.swing.JTable();
        btnNew = new shared.Button();
        btnUpdate = new shared.Button();

        lblSearchCriteria.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lblSearchCriteria.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSearchCriteria.setText("Search criteria");

        lblMedicalRecordId.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lblMedicalRecordId.setText("Medical record id");

        txtMedicalRecordId.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N

        lblFirstName.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lblFirstName.setText("First name");

        txtFirstName.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N

        lblLastName.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lblLastName.setText("Last name");

        txtLastName.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N

        lblGender.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lblGender.setText("Gender");

        cmbGender.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N

        lblSearchResults.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lblSearchResults.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSearchResults.setText("Search results");

        lblMedicalRecordIdValidation.setForeground(java.awt.Color.red);
        lblMedicalRecordIdValidation.setText("Medical record id validation");

        lblMedicalRecordsImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/medicalRecords.png"))); // NOI18N

        tableMedicalRecords.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        scrollPaneMedicalRecords.setViewportView(tableMedicalRecords);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblSearchCriteria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblMedicalRecordId, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
                        .addComponent(txtMedicalRecordId)
                        .addComponent(lblFirstName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtFirstName)
                        .addComponent(lblLastName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtLastName)
                        .addComponent(lblGender, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cmbGender, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblMedicalRecordIdValidation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(lblMedicalRecordsImage, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblSearchResults, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(scrollPaneMedicalRecords, javax.swing.GroupLayout.DEFAULT_SIZE, 638, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblSearchCriteria)
                    .addComponent(lblSearchResults))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblMedicalRecordId)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMedicalRecordId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblMedicalRecordIdValidation)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblFirstName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblLastName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblGender)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblMedicalRecordsImage, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(scrollPaneMedicalRecords)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnNew, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnUpdate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private shared.Button btnNew;
    private shared.Button btnSearch;
    private shared.Button btnUpdate;
    private javax.swing.JComboBox<shared.LocalizedCmbOption> cmbGender;
    private javax.swing.JLabel lblFirstName;
    private javax.swing.JLabel lblGender;
    private javax.swing.JLabel lblLastName;
    private javax.swing.JLabel lblMedicalRecordId;
    private javax.swing.JLabel lblMedicalRecordIdValidation;
    private javax.swing.JLabel lblMedicalRecordsImage;
    private javax.swing.JLabel lblSearchCriteria;
    private javax.swing.JLabel lblSearchResults;
    private javax.swing.JScrollPane scrollPaneMedicalRecords;
    private javax.swing.JTable tableMedicalRecords;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtLastName;
    private javax.swing.JTextField txtMedicalRecordId;
    // End of variables declaration//GEN-END:variables

    public JButton getBtnNew() {
        return btnNew.getButton();
    }

    public JButton getBtnSearch() {
        return btnSearch.getButton();
    }

    public JButton getBtnUpdate() {
        return btnUpdate.getButton();
    }

    public JComboBox<LocalizedCmbOption> getCmbGender() {
        return cmbGender;
    }

    public JLabel getLblFirstName() {
        return lblFirstName;
    }

    public JLabel getLblGender() {
        return lblGender;
    }

    public JLabel getLblLastName() {
        return lblLastName;
    }

    public JLabel getLblMedicalRecordId() {
        return lblMedicalRecordId;
    }

    public JLabel getLblMedicalRecordIdValidation() {
        return lblMedicalRecordIdValidation;
    }

    public JLabel getLblMedicalRecordsImage() {
        return lblMedicalRecordsImage;
    }

    public JLabel getLblSearchCriteria() {
        return lblSearchCriteria;
    }

    public JLabel getLblSearchResults() {
        return lblSearchResults;
    }

    public JTable getTableMedicalRecords() {
        return tableMedicalRecords;
    }

    public JTextField getTxtFirstName() {
        return txtFirstName;
    }

    public JTextField getTxtLastName() {
        return txtLastName;
    }

    public JTextField getTxtMedicalRecordId() {
        return txtMedicalRecordId;
    }

    
}
