/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords;

import builder.Controller;
import builder.ControllerBuilder;
import builder.ResponseHandler;
import java.util.List;
import medicalRecords.medicalRecord.MedicalRecordControllerBuilder;
import model.MedicalRecord;
import model.persistence.Persistence;
import shared.Util;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class MedicalRecordsResponseHandler extends ResponseHandler {
    
    @Override
    public void handleResponse(TransferObject response) {
        switch (response.getOperation()) {
            case MEDICAL_RECORD_CREATE_SUCCESS:
                handleMedicalRecordCreateSuccess(response);
                break;
            case MEDICAL_RECORD_CREATE_ERROR:
                showErrorMessage(response);
                break;
            case MEDICAL_RECORD_FOUND:
                handleMedicalRecordFound(response);
                break;
            case MEDICAL_RECORD_NOT_FOUND:
                showErrorMessage(response);
                break;
        }
    }

    private void handleMedicalRecordCreateSuccess(TransferObject response) {
        MedicalRecord medicalRecord = (MedicalRecord) response.get(MedicalRecord.MEDICAL_RECORD_KEY);
        ControllerBuilder medicalRecordControllerBuilder = new MedicalRecordControllerBuilder(medicalRecord, (MedicalRecordsController) controller);
        Controller medicalRecordController = medicalRecordControllerBuilder.build();
        controller.goToPanel(medicalRecordController);
        Util.showMessageDialog(MedicalRecordsLocalizer.MEDICAL_RECORD_CREATE_SUCCESS_MESSAGE);
    }

    private void handleMedicalRecordFound(TransferObject response) {
        List<Persistence> medicalRecords = (List<Persistence>) response.get(MedicalRecord.MEDICAL_RECORD_KEY);
        if (medicalRecords.isEmpty()) {
            Util.showMessageDialog(MedicalRecordsLocalizer.MEDICAL_RECORD_NOT_FOUND_MESSAGE);
        } else {
            ((MedicalRecordsController) controller).setMedicalRecords(medicalRecords);
            Util.showMessageDialog(MedicalRecordsLocalizer.MEDICAL_RECORD_FOUND_MESSAGE);
        }
    }

}
