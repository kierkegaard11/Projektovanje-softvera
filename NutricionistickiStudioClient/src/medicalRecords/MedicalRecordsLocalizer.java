/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords;

import builder.Localizer;
import shared.Util;

/**
 *
 * @author maja
 */
public class MedicalRecordsLocalizer extends Localizer {

    public static final String BREADCRUMB = "panel.medicalRecords.breadcrumb";
    public static final String VALIDATION_MEDICAL_RECORD_ID_IS_NUMBER = "panel.medicalRecords.validation.medicalRecordId.isNumber";
    public static final String MEDICAL_RECORD_NOT_SELECTED = "panel.medicalRecords.medicalRecordNotSelected";
    public static final String MEDICAL_RECORD_CREATE_SUCCESS_MESSAGE = "operation.medicalRecordCreate.success";
    public static final String MEDICAL_RECORD_FOUND_MESSAGE = "operation.medicalRecordFind.success";
    public static final String MEDICAL_RECORD_NOT_FOUND_MESSAGE = "operation.medicalRecordFind.error";
    private static final String SEARCH_CRITERIA = "panel.medicalRecords.searchCriteria";
    private static final String MEDICAL_RECORD_ID = "panel.medicalRecords.medicalRecordId";
    private static final String FIRST_NAME = "panel.medicalRecords.firstName";
    private static final String LAST_NAME = "panel.medicalRecords.lastName";
    private static final String GENDER = "panel.medicalRecords.gender";
    private static final String SEARCH = "panel.medicalRecords.search";
    private static final String SEARCH_RESULTS = "panel.medicalRecords.searchResults";
    private static final String NEW = "panel.medicalRecords.new";
    private static final String UPDATE = "panel.medicalRecords.update";
    
    private MedicalRecordsTableModel medicalRecordsTableModel;
    private String[] medicalRecordTableColumnNames;

    @Override
    public void localizationChanged() {
        MedicalRecordsPanel panel = (MedicalRecordsPanel) super.panel;
        initializeMedicalRecordsTableColumnNames();
        localizeCmbGenders();
        localizeLabelMessageKey(panel.getLblSearchCriteria(), SEARCH_CRITERIA);
        localizeLabelMessageKey(panel.getLblMedicalRecordId(), MEDICAL_RECORD_ID);
        localizeLabelMessageKey(panel.getLblFirstName(), FIRST_NAME);
        localizeLabelMessageKey(panel.getLblLastName(), LAST_NAME);
        localizeLabelMessageKey(panel.getLblGender(), GENDER);
        localizeButtonMessageKey(panel.getBtnSearch(), SEARCH);
        localizeLabelMessageKey(panel.getLblSearchResults(), SEARCH_RESULTS);
        localizeButtonMessageKey(panel.getBtnNew(), NEW);
        localizeButtonMessageKey(panel.getBtnUpdate(), UPDATE);
    }
    
    public void setMedicalRecordsTableModel(MedicalRecordsTableModel medicalRecordsTableModel) {
        this.medicalRecordsTableModel = medicalRecordsTableModel;
    }

    private void initializeMedicalRecordsTableColumnNames() {
        medicalRecordTableColumnNames = new String[] {
            localizationLoader.getString(MEDICAL_RECORD_ID),
            localizationLoader.getString(FIRST_NAME),
            localizationLoader.getString(LAST_NAME),
            localizationLoader.getString(GENDER)
        };
        if (medicalRecordsTableModel != null) {
            medicalRecordsTableModel.setColumnNames(medicalRecordTableColumnNames);
        }
    }

    private void localizeCmbGenders() {
        Util.localizeCmb(((MedicalRecordsPanel) panel).getCmbGender());
    }


    public String[] getMedicalRecordsTableColumnNames() {
        return medicalRecordTableColumnNames;
    }

    @Override
    public void initialize() {
        initializeMedicalRecordsTableColumnNames();
        localizeCmbGenders();
    }
    
}
