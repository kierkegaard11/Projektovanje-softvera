/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import localization.LocalizationLoader;
import localization.LocalizationObserver;
import model.MedicalRecord;
import model.persistence.Persistence;

/**
 *
 * @author maja
 */
public class MedicalRecordsTableModel extends AbstractTableModel implements LocalizationObserver {
    
    private List<Persistence> medicalRecords;
    private String[] columnNames;
    private LocalizationLoader localizationLoader;
    
    public MedicalRecordsTableModel(String[] columnNames) {
        medicalRecords = new ArrayList<>();
        this.columnNames = columnNames;
        localizationLoader = LocalizationLoader.getInstance();
    }
    
    @Override
    public int getRowCount() {
        return medicalRecords.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        MedicalRecord medicalRecord = (MedicalRecord) medicalRecords.get(row);
        switch (col) {
            case 0:
                return medicalRecord.getMedicalRecordId();
            case 1:
                return medicalRecord.getFirstName();
            case 2:
                return medicalRecord.getLastName();
            case 3:
                return localizationLoader.getString(medicalRecord.getGender());
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
        fireTableStructureChanged();
    }

    public void setMedicalRecords(List<Persistence> medicalRecords) {
        this.medicalRecords = medicalRecords;
        fireTableDataChanged();
    }

    @Override
    public void localizationChanged() {
        fireTableDataChanged();
    }
    
    public MedicalRecord getMedicalRecordAtIndex(int index) {
        return (MedicalRecord) medicalRecords.get(index);
    }

    public void updateMedicalRecord(MedicalRecord updatedMedicalRecord) {
        for (int i = 0; i < medicalRecords.size(); i++) {
            if (medicalRecords.get(i).equals(updatedMedicalRecord)) {
                medicalRecords.set(i, updatedMedicalRecord);
                fireTableDataChanged();
                break;
            }
        }
    }
    
}
