/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords;

import builder.RequestBuilder;
import model.MedicalRecord;
import transfer.Operation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class MedicalRecordsRequestBuilder extends RequestBuilder {
    
    public TransferObject buildNewMedicalRecordRequest() {
        return createRequest(Operation.NEW_MEDICAL_RECORD)
                .build();
    }
    
    public TransferObject buildFindMedicalRecordRequest() {
        MedicalRecordsPanel panel = (MedicalRecordsPanel) controller.getPanel();
        return createRequest(Operation.FIND_MEDICAL_RECORD)
                .put(MedicalRecord.COL_MEDICAL_RECORD_ID, getInteger(panel.getTxtMedicalRecordId()))
                .put(MedicalRecord.COL_FIRST_NAME, getString(panel.getTxtFirstName()))
                .put(MedicalRecord.COL_LAST_NAME, getString(panel.getTxtLastName()))
                .put(MedicalRecord.COL_GENDER, getString(panel.getCmbGender()))
                .build();
    }
    
    public TransferObject buildFindAllMedicalRecordRequest() {
        return createRequest(Operation.FIND_MEDICAL_RECORD)
                .put(MedicalRecord.COL_MEDICAL_RECORD_ID, null)
                .put(MedicalRecord.COL_FIRST_NAME, null)
                .put(MedicalRecord.COL_LAST_NAME, null)
                .put(MedicalRecord.COL_GENDER, null)
                .build();
    }

}
