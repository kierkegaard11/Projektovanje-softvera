/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords;

import builder.Controller;
import builder.ControllerBuilder;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.JTable;
import medicalRecords.medicalRecord.MedicalRecordControllerBuilder;
import model.Gender;
import model.MedicalRecord;
import model.persistence.Persistence;
import shared.ImageController;
import shared.Util;

/**
 *
 * @author maja
 */
public class MedicalRecordsController extends Controller {

    @Override
    public String getBreadcrumbTextKey() {
        return MedicalRecordsLocalizer.BREADCRUMB;
    }

    @Override
    protected void clearValidationMessages() {
        MedicalRecordsPanel panel = (MedicalRecordsPanel) super.panel;
        clearValidationMessage(panel.getLblMedicalRecordIdValidation());
    }

    @Override
    protected void setupImages() {
        imageController.setupImage(((MedicalRecordsPanel) panel).getLblMedicalRecordsImage(), ImageController.MEDICAL_RECORDS_IMAGE, ImageController.MEDICAL_RECORDS_HOVER_IMAGE);
    }

    @Override
    protected void abstractInitialize() {
        initializeCmbGender();
        initializeTableMedicalRecords();
        addBtnNewActionListener();
        addBtnSearchListener();
        addBtnUpdateListener();
    }

    private void initializeTableMedicalRecords() {
        MedicalRecordsLocalizer localizer = (MedicalRecordsLocalizer) super.localizer;
        String[] columnNames = localizer.getMedicalRecordsTableColumnNames();
        MedicalRecordsTableModel medicalRecordsTableModel = new MedicalRecordsTableModel(columnNames);
        localizer.setMedicalRecordsTableModel(medicalRecordsTableModel);
        JTable tableMedicalRecords = ((MedicalRecordsPanel) panel).getTableMedicalRecords();
        tableMedicalRecords.getTableHeader().setReorderingAllowed(false);
        tableMedicalRecords.setModel(medicalRecordsTableModel);
    }

    private void addBtnNewActionListener() {
        ((MedicalRecordsPanel) panel).getBtnNew().addActionListener((ActionEvent e) -> {
            MedicalRecordsRequestBuilder requestBuilder = (MedicalRecordsRequestBuilder) super.requestBuilder;
            sendRequestAndHandleResponse(requestBuilder.buildNewMedicalRecordRequest());
        });
    }

    private void addBtnSearchListener() {
        ((MedicalRecordsPanel) panel).getBtnSearch().addActionListener((ActionEvent e) -> {
            MedicalRecordsRequestBuilder requestBuilder = (MedicalRecordsRequestBuilder) super.requestBuilder;
            sendRequestAndHandleResponse(requestBuilder.buildFindMedicalRecordRequest());
        });
    }

    public void setMedicalRecords(List<Persistence> medicalRecords) {
        MedicalRecordsPanel panel = (MedicalRecordsPanel) super.panel;
        MedicalRecordsTableModel tableModel = (MedicalRecordsTableModel) panel.getTableMedicalRecords().getModel();
        tableModel.setMedicalRecords(medicalRecords);
    }

    private void addBtnUpdateListener() {
        ((MedicalRecordsPanel) panel).getBtnUpdate().addActionListener((ActionEvent e) -> {
            goToMedicalRecordPanel();
        });
    }

    private void goToMedicalRecordPanel() {
        JTable medicalRecordsTable = ((MedicalRecordsPanel) super.panel).getTableMedicalRecords();
        int selectedIndex = medicalRecordsTable.getSelectedRow();
        if (selectedIndex != -1) {
            MedicalRecordsTableModel tableModel = (MedicalRecordsTableModel) medicalRecordsTable.getModel();
            MedicalRecord selectedMedicalRecord = tableModel.getMedicalRecordAtIndex(selectedIndex);
            ControllerBuilder medicalRecordControllerBuilder = new MedicalRecordControllerBuilder(selectedMedicalRecord, this);
            Controller medicalRecordController = medicalRecordControllerBuilder.build();
            goToPanel(medicalRecordController);
        } else {
            Util.showMessageDialog(MedicalRecordsLocalizer.MEDICAL_RECORD_NOT_SELECTED);
        }
    }

    private void initializeCmbGender() {
        Util.initializeLocalizedCmb(((MedicalRecordsPanel) panel).getCmbGender(),
                null,
                Gender.MALE,
                Gender.FEMALE);
    }

    public void updateMedicalRecordsTable(MedicalRecord updatedMedicalRecord) {
        MedicalRecordsPanel panel = (MedicalRecordsPanel) super.panel;
        MedicalRecordsTableModel tableModel = (MedicalRecordsTableModel) panel.getTableMedicalRecords().getModel();
        tableModel.updateMedicalRecord(updatedMedicalRecord);
    }

}
