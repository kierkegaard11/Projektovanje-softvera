/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords;

import builder.Validator;
import javax.swing.JLabel;

/**
 *
 * @author maja
 */
public class MedicalRecordsValidator extends Validator {
    
    public boolean validateMedicalRecordId() {
        MedicalRecordsPanel panel = (MedicalRecordsPanel) super.panel;
        String text = panel.getTxtMedicalRecordId().getText().trim();
        JLabel lblValidation = panel.getLblMedicalRecordIdValidation();
        String validationMessage = localizationLoader.getString(MedicalRecordsLocalizer.VALIDATION_MEDICAL_RECORD_ID_IS_NUMBER);
        return validateTextIsInteger(text, lblValidation, validationMessage);
    }
    
}
