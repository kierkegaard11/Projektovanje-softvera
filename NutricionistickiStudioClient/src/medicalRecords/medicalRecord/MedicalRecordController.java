/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords.medicalRecord;

import builder.Controller;
import java.awt.event.ActionEvent;
import java.util.Date;
import medicalRecords.MedicalRecordsController;
import model.Gender;
import model.MedicalRecord;
import shared.LocalizedCmbOption;
import shared.Util;

/**
 *
 * @author maja
 */
public class MedicalRecordController extends Controller {

    private MedicalRecord medicalRecord;
    private MedicalRecordsController medicalRecordsController;

    public MedicalRecordController(MedicalRecord medicalRecord, MedicalRecordsController medicalRecordsController) {
        this.medicalRecord = medicalRecord;
        this.medicalRecordsController = medicalRecordsController;
    }

    @Override
    public String getBreadcrumbTextKey() {
        return MedicalRecordLocalizer.BREADCRUMB;
    }

    @Override
    protected void clearValidationMessages() {
        MedicalRecordPanel panel = (MedicalRecordPanel) super.panel;
        clearValidationMessage(panel.getLblFirstNameValidation());
        clearValidationMessage(panel.getLblLastNameValidation());
        clearValidationMessage(panel.getLblDateOfBirthValidation());
        clearValidationMessage(panel.getLblMobilePhoneValidation());
        clearValidationMessage(panel.getLblEmailValidation());
    }

    @Override
    protected void setupImages() {
    }

    @Override
    protected void abstractInitialize() {
        initializeCmbGender();
        populateMedicalRecordPanel();
        addBtnSaveListener();
    }

    private void initializeCmbGender() {
        Util.initializeLocalizedCmb(((MedicalRecordPanel) panel).getCmbGender(),
                Gender.MALE,
                Gender.FEMALE);
    }

    private void populateMedicalRecordPanel() {
        MedicalRecordPanel panel = (MedicalRecordPanel) super.panel;
        panel.getTxtMedicalRecordId().setText(Integer.toString(medicalRecord.getMedicalRecordId()));
        panel.getTxtFirstName().setText(medicalRecord.getFirstName());
        panel.getTxtLastName().setText(medicalRecord.getLastName());
        panel.getCmbGender().setSelectedItem(new LocalizedCmbOption(medicalRecord.getGender()));
        panel.getTxtDateOfBirth().setText(Util.getDateString(medicalRecord.getDateOfBirth()));
        panel.getTxtMobilePhone().setText(medicalRecord.getMobilePhone());
        panel.getTxtEmail().setText(medicalRecord.getEmail());
        panel.getTxtNote().setText(medicalRecord.getNote());
    }

    private void addBtnSaveListener() {
        MedicalRecordPanel panel = (MedicalRecordPanel) super.panel;
        panel.getBtnSave().addActionListener((ActionEvent ae) -> {
            if (validateForm()) {
                populateMedicalRecordFromForm();
                MedicalRecordRequestBuilder requestBuilder = (MedicalRecordRequestBuilder) super.requestBuilder;
                sendRequestAndHandleResponse(requestBuilder.buildUpdateMedicalRecordRequest());
            }
        });
    }

    private boolean validateForm() {
        MedicalRecordValidator validator = (MedicalRecordValidator) super.validator;
        boolean firstNameValid = validator.validateFirstName();
        boolean lastNameValid = validator.validateLastName();
        boolean dateOfBirthValid = validator.validateDateOfBirth();
        boolean emailValid = validator.validateEmail();
        boolean mobilePhoneValid = validator.validateMobilePhone();
        return firstNameValid &&
                lastNameValid &&
                dateOfBirthValid &&
                emailValid &&
                mobilePhoneValid;
    }

    private void populateMedicalRecordFromForm() {
        MedicalRecordPanel panel = (MedicalRecordPanel) super.panel;
        Integer medicalRecordId = Integer.parseInt(panel.getTxtMedicalRecordId().getText().trim());
        String firstName = panel.getTxtFirstName().getText().trim();
        String lastName = panel.getTxtLastName().getText().trim();
        Date dateOfBirth = Util.getDate(panel.getTxtDateOfBirth().getText().trim());
        String email = panel.getTxtEmail().getText().trim();
        String mobilePhone = panel.getTxtMobilePhone().getText().trim();
        String gender = ((LocalizedCmbOption) panel.getCmbGender().getSelectedItem()).getKey();
        String note = panel.getTxtNote().getText().trim();
        medicalRecord = new MedicalRecord(medicalRecordId, firstName, lastName, dateOfBirth, gender, mobilePhone, email, note);
    }

    public MedicalRecord getMedicalRecord() {
        return medicalRecord;
    }

    public void updateMedicalRecordsTable(MedicalRecord updatedMedicalRecord) {
        medicalRecordsController.updateMedicalRecordsTable(updatedMedicalRecord);
    }
    
}
