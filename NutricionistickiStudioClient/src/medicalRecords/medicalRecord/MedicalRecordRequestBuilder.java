/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords.medicalRecord;

import builder.RequestBuilder;
import model.MedicalRecord;
import transfer.Operation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class MedicalRecordRequestBuilder extends RequestBuilder {

    TransferObject buildUpdateMedicalRecordRequest() {
        return createRequest(Operation.UPDATE_MEDICAL_RECORD)
                .put(MedicalRecord.MEDICAL_RECORD_KEY, ((MedicalRecordController) controller).getMedicalRecord())
                .build();
    }
    
}
