/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords.medicalRecord;

import builder.Controller;
import builder.ControllerBuilder;
import builder.Localizer;
import builder.RequestBuilder;
import builder.ResponseHandler;
import builder.Validator;
import javax.swing.JPanel;
import medicalRecords.MedicalRecordsController;
import model.MedicalRecord;

/**
 *
 * @author maja
 */
public class MedicalRecordControllerBuilder extends ControllerBuilder {

    private MedicalRecord medicalRecord;
    private MedicalRecordsController medicalRecordsController;

    public MedicalRecordControllerBuilder(MedicalRecord medicalRecord, MedicalRecordsController medicalRecordsController) {
        this.medicalRecord = medicalRecord;
        this.medicalRecordsController = medicalRecordsController;
    }

    @Override
    protected JPanel createPanel() {
        return new MedicalRecordPanel();
    }

    @Override
    protected Localizer createLocalizer() {
        return new MedicalRecordLocalizer();
    }

    @Override
    protected Validator createValidator() {
        return new MedicalRecordValidator();
    }

    @Override
    protected ResponseHandler createResponseHandler() {
        return new MedicalRecordResponseHandler();
    }

    @Override
    protected RequestBuilder createRequestBuilder() {
        return new MedicalRecordRequestBuilder();
    }

    @Override
    protected Controller createController() {
        return new MedicalRecordController(medicalRecord, medicalRecordsController);
    }
    
}
