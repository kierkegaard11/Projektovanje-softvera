/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords.medicalRecord;

import builder.Localizer;
import shared.Util;

/**
 *
 * @author maja
 */
public class MedicalRecordLocalizer extends Localizer {
    
    public static final String BREADCRUMB = "panel.medicalRecord.breadcrumb";
    public static final String VALIDATION_FIRST_NAME_IS_EMPTY = "panel.medicalRecord.validation.firstName.isEmpty";
    public static final String VALIDATION_LAST_NAME_IS_EMPTY = "panel.medicalRecord.validation.lastName.isEmpty";
    public static final String VALIDATION_DATE_OF_BIRTH_INVALID = "panel.medicalRecord.validation.dateOfBirth.invalid";
    public static final String VALIDATION_MOBILE_PHONE_IS_EMPTY = "panel.medicalRecord.validation.mobilePhone.isEmpty";
    public static final String VALIDATION_EMAIL_INVALID = "panel.medicalRecord.validation.email.invalid";
    public static final String MEDICAL_RECORD_UPDATE_SUCCESS_MESSAGE = "operation.medicalRecordUpdate.success";
    private static final String FIRST_NAME = "panel.medicalRecord.firstName";
    private static final String LAST_NAME = "panel.medicalRecord.lastName";
    private static final String GENDER = "panel.medicalRecord.gender";
    private static final String DATE_OF_BIRTH = "panel.medicalRecord.dateOfBirth";
    private static final String MOBILE_PHONE = "panel.medicalRecord.mobilePhone";
    private static final String EMAIL = "panel.medicalRecord.email";
    private static final String NOTE = "panel.medicalRecord.note";
    private static final String SAVE = "panel.medicalRecord.save";

    @Override
    public void localizationChanged() {
        MedicalRecordPanel panel = (MedicalRecordPanel) super.panel;
        localizeCmbGenders();
        localizeLabelMessageKey(panel.getLblFirstName(), FIRST_NAME);
        localizeLabelValidationWithMessageKey(panel.getLblFirstNameValidation(), VALIDATION_FIRST_NAME_IS_EMPTY);
        localizeLabelMessageKey(panel.getLblLastName(), LAST_NAME);
        localizeLabelValidationWithMessageKey(panel.getLblLastNameValidation(), VALIDATION_LAST_NAME_IS_EMPTY);
        localizeLabelMessageKey(panel.getLblGender(), GENDER);
        localizeLabelMessageKey(panel.getLblDateOfBirth(), DATE_OF_BIRTH);
        localizeLabelValidationWithMessageKey(panel.getLblDateOfBirthValidation(), VALIDATION_DATE_OF_BIRTH_INVALID);
        localizeLabelMessageKey(panel.getLblMobilePhone(), MOBILE_PHONE);
        localizeLabelValidationWithMessageKey(panel.getLblMobilePhoneValidation(), VALIDATION_MOBILE_PHONE_IS_EMPTY);
        localizeLabelMessageKey(panel.getLblEmail(), EMAIL);
        localizeLabelValidationWithMessageKey(panel.getLblEmailValidation(), VALIDATION_EMAIL_INVALID);
        localizeLabelMessageKey(panel.getLblNote(), NOTE);
        localizeButtonMessageKey(panel.getBtnSave(), SAVE);
    }
    
    private void localizeCmbGenders() {
        Util.localizeCmb(((MedicalRecordPanel) panel).getCmbGender());
    }

    @Override
    public void initialize() {
        localizeCmbGenders();
    }
    
}
