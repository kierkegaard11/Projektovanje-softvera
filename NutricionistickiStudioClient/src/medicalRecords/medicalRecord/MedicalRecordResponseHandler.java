/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords.medicalRecord;

import builder.ResponseHandler;
import model.MedicalRecord;
import shared.Util;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public class MedicalRecordResponseHandler extends ResponseHandler {
    
    @Override
    public void handleResponse(TransferObject response) {
        switch (response.getOperation()) {
            case MEDICAL_RECORD_UPDATE_SUCCESS:
                handleMedicalRecordUpdateSuccess(response);
                break;
            case MEDICAL_RECORD_UPDATE_ERROR:
                showErrorMessage(response);
                break;
        }
    }

    private void handleMedicalRecordUpdateSuccess(TransferObject response) {
        MedicalRecord updatedMedicalRecord = (MedicalRecord) response.get(MedicalRecord.MEDICAL_RECORD_KEY);
        ((MedicalRecordController) controller).updateMedicalRecordsTable(updatedMedicalRecord);
        Util.showMessageDialog(MedicalRecordLocalizer.MEDICAL_RECORD_UPDATE_SUCCESS_MESSAGE);
    }

}
