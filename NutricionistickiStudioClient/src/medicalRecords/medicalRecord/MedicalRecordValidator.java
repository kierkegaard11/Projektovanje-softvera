/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicalRecords.medicalRecord;

import builder.Validator;
import javax.swing.JLabel;

/**
 *
 * @author maja
 */
public class MedicalRecordValidator extends Validator {
    
    public boolean validateFirstName() {
        MedicalRecordPanel panel = (MedicalRecordPanel) super.panel;
        String text = panel.getTxtFirstName().getText().trim();
        JLabel lblValidation = panel.getLblFirstNameValidation();
        String message = localizationLoader.getString(MedicalRecordLocalizer.VALIDATION_FIRST_NAME_IS_EMPTY);
        return validateTextIsEmpty(text, lblValidation, message);
    }
    
    public boolean validateLastName() {
        MedicalRecordPanel panel = (MedicalRecordPanel) super.panel;
        String text = panel.getTxtLastName().getText().trim();
        JLabel lblValidation = panel.getLblLastNameValidation();
        String message = localizationLoader.getString(MedicalRecordLocalizer.VALIDATION_LAST_NAME_IS_EMPTY);
        return validateTextIsEmpty(text, lblValidation, message);
    }
    
    public boolean validateDateOfBirth() {
        MedicalRecordPanel panel = (MedicalRecordPanel) super.panel;
        String text = panel.getTxtDateOfBirth().getText().trim();
        JLabel lblValidation = panel.getLblDateOfBirthValidation();
        String message = localizationLoader.getString(MedicalRecordLocalizer.VALIDATION_DATE_OF_BIRTH_INVALID);
        return validateTextIsValidDate(text, lblValidation, message);
    }

    public boolean validateMobilePhone() {
        MedicalRecordPanel panel = (MedicalRecordPanel) super.panel;
        String text = panel.getTxtMobilePhone().getText().trim();
        JLabel lblValidation = panel.getLblMobilePhoneValidation();
        String message = localizationLoader.getString(MedicalRecordLocalizer.VALIDATION_MOBILE_PHONE_IS_EMPTY);
        return validateTextIsEmpty(text, lblValidation, message);
    }
    
    public boolean validateEmail() {
        MedicalRecordPanel panel = (MedicalRecordPanel) super.panel;
        String text = panel.getTxtEmail().getText().trim();
        JLabel lblValidation = panel.getLblEmailValidation();
        String message = localizationLoader.getString(MedicalRecordLocalizer.VALIDATION_EMAIL_INVALID);
        return validateTextIsEmpty(text, lblValidation, message);
    }
    
}
