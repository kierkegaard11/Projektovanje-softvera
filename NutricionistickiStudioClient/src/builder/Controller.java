/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import javax.swing.JLabel;
import javax.swing.JPanel;
import login.LoginControllerBuilder;
import main.MainController;
import server.Server;
import shared.ConfigurationProperties;
import shared.ImageController;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public abstract class Controller {

    private MainController formMainController;
    protected JPanel panel;
    protected Localizer localizer;
    protected Validator validator;
    protected ConfigurationProperties configurationProperties;
    protected ImageController imageController;
    protected ResponseHandler responseHandler;
    protected RequestBuilder requestBuilder;
    protected Server server;

    public Controller() {
        configurationProperties = ConfigurationProperties.getInstance();
        imageController = ImageController.getInstance();
        server = Server.getInstance();
    }

    public void goToPanel(Controller panelController) {
        formMainController.setPanelController(panelController);
    }

    public void initialize() {
        clearValidationMessages();
        setupImages();
        abstractInitialize();
    }

    protected void clearValidationMessage(JLabel lblValidation) {
        lblValidation.setText("");
    }

    protected void sendRequestAndHandleResponse(TransferObject request) {
        if (server.send(request)) {
            TransferObject response = server.read();
            if (response != null) {
                responseHandler.handleResponse(response);
            } else {
                logout();
            }
        } else {
            logout();
        }
    }
    
    protected void logout() {
        server.disconnect();
        ControllerBuilder loginControllerBuilder = new LoginControllerBuilder();
        Controller loginController = loginControllerBuilder.build();
        goToPanel(loginController);
    }

    public abstract String getBreadcrumbTextKey();

    protected abstract void clearValidationMessages();

    protected abstract void setupImages();

    protected abstract void abstractInitialize();
    

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public void setLocalizer(Localizer localizer) {
        this.localizer = localizer;
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    public void setResponseHandler(ResponseHandler responseHandler) {
        this.responseHandler = responseHandler;
    }

    public void setRequestBuilder(RequestBuilder requestBuilder) {
        this.requestBuilder = requestBuilder;
    }
    
    public void setFormMainController(MainController formMainController) {
        this.formMainController = formMainController;
    }

    public JPanel getPanel() {
        return panel;
    }
    
}
