/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import model.persistence.Persistence;
import shared.LocalizedCmbOption;
import transfer.Operation;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public abstract class RequestBuilder {
    
    protected Controller controller;
    private TransferObject request;

    public RequestBuilder createRequest(Operation operation) {
        request = new TransferObject();
        request.setOperation(operation);
        return this;
    }
    
    public RequestBuilder put(String key, Object value) {
        request.put(key, value);
        return this;
    }
    
    public TransferObject build() {
        return request;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }
    
    protected String getString(JTextField txtField) {
        String text = txtField.getText().trim();
        if (text != null && !text.isEmpty()) {
            return text;
        }
        return null;
    }
    
    protected Integer getInteger(JTextField txtField) {
        String text = txtField.getText().trim();
        if (text != null) {
            try {
                return Integer.parseInt(text);
            } catch (NumberFormatException ex) {
            }
        }
        return null;
    }
    
    protected String getString(JComboBox localizedCmb) {
        LocalizedCmbOption wrapper = (LocalizedCmbOption) localizedCmb.getSelectedItem();
        if (wrapper != null) {
            return wrapper.getKey();
        }
        return null;
    }
    
    protected Persistence getPersistence(JComboBox cmb) {
        return (Persistence) cmb.getSelectedItem();
    }
    
}
