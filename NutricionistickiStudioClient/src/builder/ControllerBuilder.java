/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import javax.swing.JPanel;

/**
 *
 * @author maja
 */
public abstract class ControllerBuilder {
    
    protected JPanel panel;
    protected Localizer localizer;
    protected Validator validator;
    protected ResponseHandler responseHandler;
    protected RequestBuilder requestBuilder;
    protected Controller controller;
    
    public Controller build() {
        buildPanel();
        buildLocalizer();
        buildValidator();
        buildResponseHandler();
        buildRequestBuilder();
        buildController();
        return controller;
    }

    private void buildPanel() {
        panel = createPanel();
    }
    
    private void buildLocalizer() {
        localizer = createLocalizer();
        localizer.setPanel(panel);
        localizer.initialize();
    }

    private void buildValidator() {
        validator = createValidator();
        validator.setPanel(panel);
    }

    private void buildResponseHandler() {
        responseHandler = createResponseHandler();
    }

    private void buildRequestBuilder() {
        requestBuilder = createRequestBuilder();
    }
    
    private void buildController() {
        controller = createController();
        controller.setPanel(panel);
        controller.setLocalizer(localizer);
        controller.setValidator(validator);
        controller.setResponseHandler(responseHandler);
        responseHandler.setController(controller);
        controller.setRequestBuilder(requestBuilder);
        requestBuilder.setController(controller);
        controller.initialize();
    }

    protected abstract JPanel createPanel();
    protected abstract Localizer createLocalizer();
    protected abstract Validator createValidator();
    protected abstract ResponseHandler createResponseHandler();
    protected abstract RequestBuilder createRequestBuilder();
    protected abstract Controller createController();
    
}
