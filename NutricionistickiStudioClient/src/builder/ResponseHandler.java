/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import shared.Util;
import transfer.TransferObject;

/**
 *
 * @author maja
 */
public abstract class ResponseHandler {
    
    protected Controller controller;

    public void setController(Controller controller) {
        this.controller = controller;
    }
    
    public abstract void handleResponse(TransferObject response);
    
    protected void showErrorMessage(TransferObject response) {
        Util.showMessageDialog((String) response.get(TransferObject.MESSAGE));
    }
    
}
