/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import localization.LocalizationLoader;
import localization.LocalizationObserver;
import localization.LocalizationSubject;

/**
 *
 * @author maja
 */
public abstract class Localizer implements LocalizationObserver {

    protected JPanel panel;
    protected LocalizationLoader localizationLoader;

    public Localizer() {
        localizationLoader = LocalizationLoader.getInstance();
        LocalizationSubject.getInstance().subscribe(this);
    }

    protected void localizeLabelMessageKey(JLabel label, String messageKey) {
        label.setText(localizationLoader.getString(messageKey));
    }

    protected void localizeLabelMessage(JLabel label, String message) {
        label.setText(message);
    }

    protected void localizeButtonMessageKey(JButton button, String messageKey) {
        button.setText(localizationLoader.getString(messageKey));
    }

    protected void localizeLabelValidationWithMessageKey(JLabel labelValidation, String messageKey) {
        if (shouldLocalizeLabelValidation(labelValidation)) {
            labelValidation.setText(localizationLoader.getString(messageKey));
        }
    }

    protected void localizeLabelValidationWithMessage(JLabel labelValidation, String message) {
        if (shouldLocalizeLabelValidation(labelValidation)) {
            labelValidation.setText(message);
        }
    }

    private boolean shouldLocalizeLabelValidation(JLabel labelValidation) {
        return !labelValidation.getText().trim().isEmpty();
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }
    
    public abstract void initialize();
}
