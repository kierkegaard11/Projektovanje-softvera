/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import javax.swing.JLabel;
import javax.swing.JPanel;
import localization.LocalizationLoader;
import shared.Util;

/**
 *
 * @author maja
 */
public abstract class Validator {
    
    protected JPanel panel;
    protected LocalizationLoader localizationLoader;
    
    public Validator() {
        localizationLoader = LocalizationLoader.getInstance();
    }
    
    public boolean validateTextIsEmpty(String text, JLabel lblValidation, String validationMessage) {
        if (!text.isEmpty()) {
            clearLblValidation(lblValidation);
            return true;
        }
        fillLblValidation(lblValidation, validationMessage);
        return false;
    }
    
    public boolean validateTextLength(String text, JLabel lblValidation, String validationMessage, int minLength) {
        if (text.length() >= minLength) {
            clearLblValidation(lblValidation);
            return true;
        }
        fillLblValidation(lblValidation, validationMessage);
        return false;
    }
    
    public boolean validateTextIsInteger(String text, JLabel lblValidation, String validationMessage) {
        try {
            Integer.parseInt(text);
            clearLblValidation(lblValidation);
        } catch (NumberFormatException ex) {
            fillLblValidation(lblValidation, validationMessage);
            return false;
        }
        return true;
    }
    
    public boolean validateTextIsDouble(String text, JLabel lblValidation, String validationMessage) {
        try {
            Double.parseDouble(text);
            clearLblValidation(lblValidation);
        } catch (NumberFormatException ex) {
            fillLblValidation(lblValidation, validationMessage);
            return false;
        }
        return true;
    }
    
    public boolean validateTextIsValidDate(String text, JLabel lblValidation, String validationMessage) {
        if (Util.isDateValid(text)) {
            clearLblValidation(lblValidation);
            return true;
        }
        fillLblValidation(lblValidation, validationMessage);
        return false;
    }
    
    public boolean validateTextIsValidEmail(String text, JLabel lblValidation, String validationMessage) {
        if (Util.isEmailValid(text)) {
            clearLblValidation(lblValidation);
            return true;
        }
        fillLblValidation(lblValidation, validationMessage);
        return false;
    }
    
    protected void clearLblValidation(JLabel lblValidation) {
        lblValidation.setText("");
    }

    protected void fillLblValidation(JLabel lblValidation, String validationMessage) {
        lblValidation.setText(validationMessage);
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }
    
}
